import { Component } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  FormGroup,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { RegisterRequest } from '../shared/model/register-request';
import { MessageService } from 'primeng/api';
import { AuthService } from '../services/auth.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css'],
  providers: [MessageService],
})
export class RegistrationComponent {
  registerRequest!: RegisterRequest;

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  passwordFormControl = new FormControl('', [
    Validators.required,
    Validators.minLength(8),
  ]);
  nameFormControl = new FormControl('', [
    Validators.required,
    Validators.pattern('[A-ZŠĆČĐŽ][a-zšđčćžA-ZŠĆČĐŽ]+( [a-zšđčćžA-ZŠĆČĐŽ]+)*'),
  ]);
  surnameFormControl = new FormControl('', [
    Validators.required,
    Validators.pattern('[A-ZŠĆČĐŽ][a-zšđčćžA-ZŠĆČĐŽ]+( [a-zšđčćžA-ZŠĆČĐŽ]+)*'),
  ]);
  phoneFormControl = new FormControl('', [
    Validators.required,
    Validators.pattern('[+][0-9]{6,13}'),
    Validators.minLength(6),
    Validators.maxLength(13),
  ]);
  usernameFormControl = new FormControl('', [
    Validators.required,
    Validators.pattern('^(?=[a-zA-Z0-9._]{2,20}$)(?!.*[_.]{2})[^_.].*[^_.]$'),
  ]);
  password2FormControl = new FormControl('', [
    Validators.required,
    this.createPasswordMatchingValidator(this.passwordFormControl),
  ]);
  vehicleNumberFormControl = new FormControl('', [Validators.required]);
  yearsExperienceFormControl = new FormControl('', [Validators.required]);

  registerFormGroup: FormGroup;
  matcher = new ErrorStateMatcher();
  errorMessage!: string;

  constructor(
    private readonly authSerive: AuthService,
    private readonly messageService: MessageService,
    private readonly fb: FormBuilder,
    private readonly router: Router
  ) {
    this.registerFormGroup = fb.group({
      email: this.emailFormControl,
      password: this.passwordFormControl,
      firstName: this.nameFormControl,
      lastName: this.surnameFormControl,
      phoneNumber: this.phoneFormControl,
      username: this.usernameFormControl,
      password2: this.password2FormControl,
      vehicleNumber: this.vehicleNumberFormControl,
      yearsExperience: this.yearsExperienceFormControl,
    });
  }

  submit(): void {
    if (!this.registerFormGroup.valid) return;
    this.registerRequest = this.registerFormGroup.value;
    this.authSerive.register(this.registerRequest).subscribe({
      next: (res) => {
        this.messageService.add({
          key: 'registration-message',
          severity: 'success',
          summary: 'Registration successfull',
          detail: 'Please verify your email',
        });
        setTimeout(() => {
          this.router.navigate(['login']);
        }, 3000);
      },
      error: (err: HttpErrorResponse) => {
        if (err.error.message.includes('(email)'))
          this.errorMessage = 'Email already exists.';
        else if (err.error.message.includes('(username)'))
          this.errorMessage = 'Username already exists.';
        this.messageService.add({
          key: 'registration-message',
          severity: 'error',
          summary: 'Registration failed',
          detail: this.errorMessage,
        });
        this.errorMessage = '';
      },
    });
  }

  createPasswordMatchingValidator(
    passwordFormControl: FormControl
  ): ValidatorFn {
    {
      return (control: AbstractControl): Record<string, string> | null =>
        control.value === passwordFormControl.value
          ? null
          : { wrongValue: control.value };
    }
  }
}
