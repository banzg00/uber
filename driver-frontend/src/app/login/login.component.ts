import { Component } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { ErrorStateMatcher } from '@angular/material/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent {
  passwordFormControl = new FormControl('', [Validators.required]);
  usernameFormControl = new FormControl('', [Validators.required]);

  loginForm: FormGroup;
  matcher = new ErrorStateMatcher();
  error: string | null;
  loggedIn: boolean;

  constructor(
    private readonly authService: AuthService,
    private readonly fb: FormBuilder,
    private readonly router: Router
  ) {
    this.loginForm = fb.group({
      username: this.usernameFormControl,
      password: this.passwordFormControl,
    });
  }

  login(): void {
    if (!this.loginForm.valid) {
      return;
    }
    this.error = '';
    this.authService.login(this.loginForm.value).subscribe({
      next: (res: any) => {
        this.authService.setUser(res.jwtToken);
        this.router.navigate(['/home']);
      },
      error: (err: any) => {
        if (err.error.message) {
          this.error = err.error.message;
        } else {
          this.error = 'Something went wrong';
        }
      },
    });
  }
}
