import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  message: string | null;

  constructor(private readonly authService: AuthService) {}

  ngOnInit(): void {
    this.message = 'Welcome ' + this.authService.getUsername() + '!';
  }
}
