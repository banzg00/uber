export interface RegisterRequest {
  username: string | null;
  firstName: string | null;
  lastName: string | null;
  email: string | null;
  password: string | null;
  phoneNumber: string | null;
  vehicleNumber: string | null;
  yearsExperience: number | null;
}
