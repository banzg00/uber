import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Observable } from 'rxjs';
import { RegisterRequest } from '../shared/model/register-request';
@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private readonly API_PATH =
    environment.api_path + 'iam-service/api/uber/auth';
  constructor(
    private readonly httpClient: HttpClient,
    private readonly router: Router
  ) {}

  public login(loginData: JSON): Observable<string> {
    return this.httpClient.post<string>(this.API_PATH + '/login', loginData);
  }

  public setUser(token: string): void {
    localStorage.setItem('token', token);
  }

  public getUser(): string | null {
    return localStorage.getItem('token');
  }

  checkIfSomeoneIsLoggedIn(): boolean {
    if (this.getUser() !== null) {
      return true;
    }
    return false;
  }

  getAuthHeader(): HttpHeaders {
    console.log(localStorage.getItem('token'));
    if (localStorage.getItem('token')) {
      return new HttpHeaders({
        Authorization: 'Bearer ' + localStorage.getItem('token'),
      });
    } else {
      return new HttpHeaders();
    }
  }

  public getUsername(): string {
    const jwt: JwtHelperService = new JwtHelperService();
    const user = this.getUser();
    if (user == null) {
      return '';
    }
    const decodedToken = jwt.decodeToken(user);
    return decodedToken.username;
  }

  public logout(): void {
    localStorage.removeItem('token');
    this.router.navigate(['login']);
  }

  public register(registerRequest: RegisterRequest): Observable<any> {
    return this.httpClient.post<any>(
      this.API_PATH + '/register/driver',
      registerRequest
    );
  }
}
