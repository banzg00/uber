import { Component } from '@angular/core';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-protected-layout',
  templateUrl: './protected-layout.component.html',
  styleUrls: ['./protected-layout.component.css'],
})
export class ProtectedLayoutComponent {
  constructor(private readonly authService: AuthService) {}
  logout(): void {
    this.authService.logout();
  }
}
