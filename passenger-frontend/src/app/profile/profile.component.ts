import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MessageService } from 'primeng/api';
import { AuthService } from '../services/auth.service';
import { UserService } from '../services/user.service';
import { User } from '../shared/model/user';
import { HttpErrorResponse } from '@angular/common/http';
import { BuyCreditsDialogComponent } from '../buy-credits-dialog/buy-credits-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { CreditBalance } from '../shared/model/credit-balance';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  providers: [MessageService],
})
export class ProfileComponent implements OnInit {
  profilePhoto =
    'https://icon-library.com/images/no-image-icon/no-image-icon-0.jpg';
  username!: string;
  credits: number = 0;
  provider!: string;

  form: FormGroup = new FormGroup({
    email: new FormControl({ value: '', disabled: true }, [
      Validators.required,
      Validators.email,
    ]),
    firstName: new FormControl('', [
      Validators.required,
      Validators.pattern(
        '[A-ZŠĆČĐŽ][a-zšđčćžA-ZŠĆČĐŽ]+( [a-zšđčćžA-ZŠĆČĐŽ]+)*'
      ),
      Validators.minLength(3),
      Validators.maxLength(20),
    ]),
    lastName: new FormControl('', [
      Validators.required,
      Validators.pattern(
        '[A-ZŠĆČĐŽ][a-zšđčćžA-ZŠĆČĐŽ]+( [a-zšđčćžA-ZŠĆČĐŽ]+)*'
      ),
      Validators.minLength(3),
      Validators.maxLength(20),
    ]),
    phoneNumber: new FormControl('', [
      Validators.required,
      Validators.pattern('[+][0-9]{6,13}'),
      Validators.minLength(6),
      Validators.maxLength(13),
    ]),
    username: new FormControl({ value: '', disabled: true }, [
      Validators.required,
    ]),
  });

  constructor(
    private readonly authSerive: AuthService,
    private readonly userService: UserService,
    private readonly messageService: MessageService,
    public creditsDialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.userService.getUser().subscribe({
      next: (res: User) => {
        this.form.setValue({
          email: res.email,
          firstName: res.firstName,
          lastName: res.lastName,
          phoneNumber: res.phoneNumber,
          username: res.username,
        });
        this.username = res.username;
        this.credits = res.credits;
      },
    });
  }

  openPasswordDialog() {}

  updateUser() {
    if (!this.form.valid) {
      return;
    }
    this.userService.updateUser(this.form).subscribe({
      next: (res) => {
        console.log(res);
        if (res === null || res === undefined) {
          return;
        }
        this.messageService.add({
          key: 'update-profile-message',
          severity: 'success',
          summary: 'Profile updated successfully',
        });
      },
      error: (err: HttpErrorResponse) => {
        this.messageService.add({
          key: 'update-profile-message',
          severity: 'error',
          summary: 'Profile not updated',
          detail:
            'There was an error occured when updating your profile. Please try again later.',
        });
      },
    });
  }

  selectPhoto(event: any): void {
    this.messageService.add({
      key: 'update-profile-message',
      severity: 'info',
      summary: 'Changing photo is not possible yet',
      detail: 'Profile photo could not be currently changed. Try again later.',
    });
  }

  openCreditsDialog() {
    const dialogRef = this.creditsDialog.open(BuyCreditsDialogComponent);
    dialogRef.afterClosed().subscribe((res: CreditBalance) => {
      if (res?.balance === null || res?.balance === undefined) {
        return;
      }
      if (res.successful) {
        this.messageService.add({
          key: 'buy-credits-message',
          severity: 'success',
          summary: 'Credits successfully pruchased',
        });
        this.credits = res.balance;
      } else {
        this.messageService.add({
          key: 'buy-credits-message',
          severity: 'error',
          summary: 'Credits not purchased',
          detail: 'There was an error with credits purchase',
        });
      }
    });
  }
}
