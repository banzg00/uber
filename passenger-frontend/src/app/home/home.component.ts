import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';
import { Subscription, debounceTime, distinctUntilChanged } from 'rxjs';
import { TripService } from '../services/trip.service';
import { AddressDTO } from '../shared/model/address-dto';
import * as L from 'leaflet';
import { icon, latLng, Map, MapOptions, marker, tileLayer } from 'leaflet';
import 'leaflet-routing-machine';
import { MapPoint } from '../shared/model/map-point';
import {
  COLORS,
  DEFAULT_LATITUDE,
  DEFAULT_LONGITUDE,
  SRB_LATIN_TO_ENG_LATIN_MAP,
} from '../shared/constants/constants';
import { MessageService } from 'primeng/api';
import { HttpErrorResponse } from '@angular/common/http';
import { RouteDTO } from '../shared/model/route-dto';
import { RouteData } from '../shared/model/route-data';
import { Trip } from '../shared/model/trip';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [MessageService],
})
export class HomeComponent implements OnInit {
  srcAddresses!: AddressDTO[];
  destAddresses!: AddressDTO[];

  searchForm!: FormGroup;
  srcSearchSubscription!: Subscription;
  destSearchSubscription!: Subscription;

  source = new FormControl('', [Validators.required]);
  destination = new FormControl('', [Validators.required]);

  selectedAddressIndexSrc = -1;
  selectedAddressIndexDest = -1;

  map!: Map;
  mapPointStations: MapPoint[] = [];
  options!: MapOptions;
  lastLayer: L.Layer[] = [];
  foundRoutes: RouteData[] = [];
  polylines: L.Polyline[] = [];
  selectedColor = COLORS[0];

  newTrip!: Trip;

  constructor(
    private fb: FormBuilder,
    private tripService: TripService,
    private messageService: MessageService,
    private authService: AuthService
  ) {}

  ngOnInit() {
    this.searchForm = this.fb.group({
      source: this.source,
      destination: this.destination,
    });
    this.initializeMapOptions();
  }

  srcSubscribe() {
    this.srcSearchSubscription?.unsubscribe();
    this.srcSearchSubscription = this.source.valueChanges
      .pipe(debounceTime(800), distinctUntilChanged())
      .subscribe(() => {
        this.findSrcAddress(this.searchForm.value.source);
      });
  }

  destSubscribe() {
    this.destSearchSubscription?.unsubscribe();
    this.destSearchSubscription = this.destination.valueChanges
      .pipe(debounceTime(800), distinctUntilChanged())
      .subscribe(() => {
        this.findDestAddress(this.searchForm.value.destination);
      });
  }

  findSrcAddress(address: string) {
    if (address.length < 3) return;
    this.tripService.addressLookup(address).subscribe((results) => {
      this.srcAddresses = results;
    });
  }

  findDestAddress(address: string) {
    if (address.length < 3) return;
    this.tripService.addressLookup(address).subscribe((results) => {
      this.destAddresses = results;
    });
  }

  convertToLatin(address: string): string {
    let latinText = '';
    for (let i = 0; i < address.length; i++) {
      const char = address[i];
      const latinChar = SRB_LATIN_TO_ENG_LATIN_MAP[char] || char; // Use mapping or keep the original character
      latinText += latinChar;
    }
    return latinText;
  }

  selectAddress(srcField: boolean, address: AddressDTO): void {
    this.filterAddressChanged();
    this.pinAddress(address, srcField);
    if (srcField) {
      this.srcSearchSubscription.unsubscribe();
      this.srcAddresses = [];
      this.source.setValue(address.display_name);
      return;
    }
    this.destSearchSubscription.unsubscribe();
    this.destAddresses = [];
    this.destination.setValue(address.display_name);
  }

  private filterAddressChanged() {
    this.mapPointStations = this.mapPointStations.filter((point) => {
      return (
        point.name === this.source.value ||
        point.name === this.destination.value
      );
    });
  }

  clearField(src: boolean) {
    if (src) {
      this.mapPointStations = this.mapPointStations.filter((point) => {
        return point.name !== this.source.value;
      });
      this.source.setValue('');
      this.srcAddresses = [];
      this.createMarker();
      return;
    }
    this.mapPointStations = this.mapPointStations.filter((point) => {
      return point.name !== this.destination.value;
    });
    this.destination.setValue('');
    this.destAddresses = [];
    this.createMarker();
  }

  orderTrip() {
    let route = this.foundRoutes[0].color === this.selectedColor ? this.foundRoutes[0] : this.foundRoutes[1]
    this.tripService
      .orderTrip(this.getDtoFromRoute(route))
      .subscribe({
        next: (res: Trip) => {
          this.messageService.add({
            key: 'order-trip-message',
            severity: 'success',
            summary: 'Trip successfully ordered',
            detail: 'Please wait until some of our drivers accept it.',
          });
          this.tripService.sendNewTrip(res);
        },
        error: () => {
          this.messageService.add({
            key: 'order-trip-message',
            severity: 'error',
            summary: 'Trip not ordered',
            detail:
              'There is a problem with application. Please try again later.',
          });
        },
      });
  }

  getDtoFromRoute(route: RouteData): RouteDTO {
    return {
      totalTime: route.totalTime,
      totalDistance: route.totalDistance,
      price: route.price,
      coordinates: this.mapPointStations,
      passengerUsername: this.authService.getUsername(),
    };
  }

  findRoutes() {
    if (!this.searchForm.valid) return;

    this.tripService
      .getRoutes(
        `${this.mapPointStations[0].lng},${this.mapPointStations[0].lat}`,
        `${this.mapPointStations[1].lng},${this.mapPointStations[1].lat}`
      )
      .subscribe({
        next: (res: RouteDTO[]) => {
          for (const r of res) {
            this.foundRoutes.push(
              new RouteData(
                this.trnsformMapCoosToLatLng(r.coordinates),
                parseFloat((r.totalTime / 60).toFixed(1)),
                parseFloat(r.totalDistance.toFixed(2)),
                r.price
              )
            );
          }
          this.drawRoutes();
        },
        error: (err: HttpErrorResponse) => {
          this.messageService.add({
            key: 'order-trip-message',
            severity: 'error',
            summary: 'No routes found',
            detail: 'There are no routes found for selected addresses.',
          });
        },
      });
  }

  trnsformMapCoosToLatLng(coordinates: MapPoint[]): L.LatLng[] {
    let latLng: L.LatLng[] = [];
    for (let coos of coordinates) {
      latLng.push(new L.LatLng(coos.lat, coos.lng));
    }
    return latLng;
  }

  private drawRoutes(): void {
    for (let i = 0; i < this.foundRoutes.length; i++) {
      this.foundRoutes[i].color = COLORS[i];
      let polyline = L.polyline(this.foundRoutes[i].coordinates, {
        color: this.foundRoutes[i].color,
      }).addTo(this.map);
      this.polylines.push(polyline);
      polyline.on('click', (e) => {
        this.selectedColor = e.target.options.color;
        this.bindTooltip();
      });
      this.bindTooltip();
    }
  }

  private bindTooltip(): void {
    for (let i = 0; i < this.polylines.length; i++) {
      this.polylines[i].closeTooltip();
      let tooltipText;
      if (this.foundRoutes[i].color === this.selectedColor) {
        tooltipText = `<div style="background-color: rgb(0, 154, 164);
                                   color: #fff;
                                   padding: 5px;
                                   border: 1px solid black;">
                      Distance: ${this.foundRoutes[i].totalDistance} km<br>
                      Time: ${this.foundRoutes[i].totalTime} min<br>
                      Price: ${this.foundRoutes[i].price} din
                      </div>`;
      } else {
        tooltipText = `Distance: ${this.foundRoutes[i].totalDistance} km<br>
                       Time: ${this.foundRoutes[i].totalTime} min<br>
                       Price: ${this.foundRoutes[i].price} din`;
      }
      
      this.polylines[i].bindTooltip(tooltipText, {
        permanent: true,
      }).openTooltip(); 
    }
  }

  // Dropdown menu settings
  onKeyDown(srcField: boolean, event: KeyboardEvent) {
    if (event.key === 'ArrowUp') {
      event.preventDefault(); // Prevent scrolling the page with arrow keys
      this.selectPreviousAddress(srcField);
    } else if (event.key === 'ArrowDown') {
      event.preventDefault(); // Prevent scrolling the page with arrow keys
      this.selectNextAddress(srcField);
    } else if (event.key === 'Enter') {
      event.preventDefault();
      if (srcField) {
        console.log(this.srcAddresses);
        console.log(this.selectedAddressIndexSrc);
        this.selectAddress(
          srcField,
          this.srcAddresses[this.selectedAddressIndexSrc]
        );
        return;
      }
      this.selectAddress(
        srcField,
        this.destAddresses[this.selectedAddressIndexDest]
      );
    }
  }

  selectNextAddress(srcField: boolean) {
    if (srcField) {
      if (this.selectedAddressIndexSrc < this.srcAddresses.length - 1) {
        this.selectedAddressIndexSrc++;
      }
      return;
    }
    if (this.selectedAddressIndexDest < this.destAddresses.length - 1) {
      this.selectedAddressIndexDest++;
    }
  }

  selectPreviousAddress(srcField: boolean) {
    if (srcField) {
      if (this.selectedAddressIndexSrc > 0) {
        this.selectedAddressIndexSrc--;
      }
      return;
    }
    if (this.selectedAddressIndexDest > 0) {
      this.selectedAddressIndexDest--;
    }
  }

  // Map settings

  initializeMap(map: Map): void {
    this.map = map;
    this.createMarker();
  }

  private initializeMapOptions(): void {
    this.options = {
      zoom: 13,
      layers: [
        tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
          maxZoom: 18,
          attribution: 'OSM',
        }),
      ],
    };
    this.lastLayer.push(
      tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 18,
        attribution: 'OSM',
      })
    );
  }

  private getDefaultIcon() {
    return icon({
      iconSize: [37, 47],
      iconAnchor: [18, 47],
      iconUrl: 'assets/marker-icon.png',
    });
  }

  private createMarker(): void {
    this.clearMap();
    this.map.setView([DEFAULT_LATITUDE, DEFAULT_LONGITUDE], this.map.getZoom());

    const mapIcon = this.getDefaultIcon();
    for (const station of this.mapPointStations) {
      const coordinates2 = latLng([
        station.lat ? station.lat : 45.0,
        station.lng ? station.lng : 19.2331,
      ]);

      this.lastLayer.push(
        marker(coordinates2).setIcon(mapIcon).addTo(this.map)
      );
      this.map.setView(coordinates2, this.map.getZoom());
    }
  }

  clearMap(): void {
    const numOfLayers = this.lastLayer.length;
    for (let i = 1; i < numOfLayers; i++) {
      if (this.map.hasLayer(this.lastLayer[i])) {
        this.map.removeLayer(this.lastLayer[i]);
      }
    }
    this.polylines.forEach((polyline) => {
      this.map.removeLayer(polyline);
    });
    this.polylines = [];
    this.foundRoutes = [];
  }

  pinAddress(address: AddressDTO, src: boolean): void {
    if (!this.existInMapPointStations(address.display_name)) {
      this.addMapPoint(address, src);
      this.createMarker();
    } else {
      this.messageService.add({
        key: 'map-point-message',
        severity: 'warning',
        summary: 'Station already chosen',
        detail: 'Selected station is already part of path.',
        life: 5000,
      });
    }
  }

  existInMapPointStations(name: string): boolean {
    for (const mapPoint of this.mapPointStations) {
      if (mapPoint.name === name) {
        return true;
      }
    }
    return false;
  }

  private addMapPoint(address: AddressDTO, src: boolean): void {
    if (src) {
      this.mapPointStations.unshift(
        new MapPoint(address.display_name, address.lat, address.lon)
      );
    } else {
      this.mapPointStations.push(
        new MapPoint(address.display_name, address.lat, address.lon)
      );
    }
  }
}
