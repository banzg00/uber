import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { AppComponent } from './app.component';
import { PublicLayoutComponent } from './public-layout/public-layout.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { ToastModule } from 'primeng/toast';
import { FormsModule } from '@angular/forms';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ProtectedLayoutComponent } from './protected-layout/protected-layout.component';
import { HomeComponent } from './home/home.component';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { ProfileComponent } from './profile/profile.component';
import { BuyCreditsDialogComponent } from './buy-credits-dialog/buy-credits-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { ActiveTripsComponent } from './active-trips/active-trips.component';
import { TripDetailsDialogComponent } from './trip-details-dialog/trip-details-dialog.component';
import { CancellationReasonDialogComponent } from './cancellation-reason-dialog/cancellation-reason-dialog.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatTooltipModule } from '@angular/material/tooltip';

@NgModule({
  declarations: [
    AppComponent,
    PublicLayoutComponent,
    RegisterComponent,
    LoginComponent,
    PageNotFoundComponent,
    ProtectedLayoutComponent,
    HomeComponent,
    ProfileComponent,
    BuyCreditsDialogComponent,
    ActiveTripsComponent,
    TripDetailsDialogComponent,
    CancellationReasonDialogComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule,
    MatFormFieldModule,
    MatCardModule,
    MatInputModule,
    ReactiveFormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    HttpClientModule,
    ToastModule,
    MatIconModule,
    FormsModule,
    LeafletModule,
    MatDialogModule,
    DragDropModule,
    MatTooltipModule
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
