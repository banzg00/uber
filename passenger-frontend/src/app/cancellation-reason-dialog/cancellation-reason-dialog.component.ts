import { Component, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { TripService } from '../services/trip.service';
import { Trip } from '../shared/model/trip';

@Component({
  selector: 'app-cancellation-reason-dialog',
  templateUrl: './cancellation-reason-dialog.component.html',
  styleUrls: ['./cancellation-reason-dialog.component.css'],
})
export class CancellationReasonDialogComponent {
  form = new FormGroup({
    reason: new FormControl('', [Validators.required]),
  });

  constructor(
    @Inject(MAT_DIALOG_DATA) public trip: Trip,
    public dialogRef: MatDialogRef<CancellationReasonDialogComponent>,
    private readonly tripService: TripService
  ) {}

  cancelTrip(): void {
    if (!this.form.valid) {
      return;
    }

    this.tripService
      .cancelTrip({
        tripId: this.trip.id,
        cancellationDescription: this.form.value.reason,
      })
      .subscribe({
        next: (res) => {
          this.dialogRef.close({ successful: true });
        },
        error: () => {
          this.dialogRef.close({ successful: false });
        },
      });
  }
}
