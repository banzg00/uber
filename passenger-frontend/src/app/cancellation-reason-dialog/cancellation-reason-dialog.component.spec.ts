import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CancellationReasonDialogComponent } from './cancellation-reason-dialog.component';

describe('CancellationReasonDialogComponent', () => {
  let component: CancellationReasonDialogComponent;
  let fixture: ComponentFixture<CancellationReasonDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CancellationReasonDialogComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(CancellationReasonDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
