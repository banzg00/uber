import { Component, Inject } from '@angular/core';
import { Trip } from '../shared/model/trip';
import {
  MAT_DIALOG_DATA,
  MatDialog,
  MatDialogRef,
} from '@angular/material/dialog';
import { CancellationReasonDialogComponent } from '../cancellation-reason-dialog/cancellation-reason-dialog.component';

@Component({
  selector: 'app-trip-details-dialog',
  templateUrl: './trip-details-dialog.component.html',
  styleUrls: ['./trip-details-dialog.component.css'],
})
export class TripDetailsDialogComponent {
  constructor(
    @Inject(MAT_DIALOG_DATA) public trip: Trip,
    public cancellationDialog: MatDialog,
    private thisDialogRef: MatDialogRef<TripDetailsDialogComponent>
  ) {}

  cancelTrip() {
    const dialogRef = this.cancellationDialog.open(
      CancellationReasonDialogComponent,
      {
        data: this.trip,
      }
    );
    dialogRef.afterClosed().subscribe((res: any) => {
      if (res?.successful === null || res?.successful === undefined) {
        return;
      }
      this.thisDialogRef.close(res);
    });
  }
}
