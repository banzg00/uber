import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../shared/model/user';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  API_IAM_PATH_SERVER = 'http://192.168.30.144:8080/iam-service/api/uber/user';
  API_IAM_PATH_LOCAL = 'http://localhost:8080/iam-service/api/uber/user';

  constructor(private http: HttpClient, private authService: AuthService) {}

  getUser(): Observable<User> {
    return this.http.get<User>(
      `${
        this.API_IAM_PATH_LOCAL
      }/passenger/get/${this.authService.getUsername()}`,
      {
        headers: this.authService.getAuthHeader(),
      }
    );
  }

  updateUser(form: FormGroup<any>) {
    const dto = this.createUpdateDTO(form);
    return this.http.put<any>(
      `${this.API_IAM_PATH_LOCAL}/passenger/update`,
      dto,
      {
        headers: this.authService.getAuthHeader(),
      }
    );
  }

  private createUpdateDTO(data: FormGroup) {
    return {
      email: data.getRawValue().email,
      firstName: data.value.firstName,
      lastName: data.value.lastName,
      phoneNumber: data.value.phoneNumber,
      username: data.getRawValue().username,
    };
  }
}
