import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root',
})
export class PaymentService {
  API_PAYMENT_PATH_SERVER =
    'http://192.168.30.144:8080/payment-service/api/uber/transaction';
  API_PAYMENT_PATH_LOCAL =
    'http://localhost:8080/payment-service/api/uber/transaction';

  constructor(private http: HttpClient, private authService: AuthService) {}

  buyCredits(amount: string) {
    const dto = this.createCreditsDTO(amount);
    return this.http.post<any>(
      `${this.API_PAYMENT_PATH_LOCAL}/credit/payIn`,
      dto,
      {
        headers: this.authService.getAuthHeader(),
      }
    );
  }

  private createCreditsDTO(amount: string) {
    return {
      username: this.authService.getUsername(),
      amount,
    };
  }
}
