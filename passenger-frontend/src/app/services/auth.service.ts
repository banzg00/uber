import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RegisterRequest } from '../shared/model/register-request';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  API_PATH_SERVER = 'http://192.168.30.144:8080/iam-service/api/uber/auth';
  API_PATH_LOCAL = 'http://localhost:8080/iam-service/api/uber/auth';

  constructor(private http: HttpClient) {}

  register(dto: RegisterRequest): Observable<any> {
    return this.http.post<any>(
      `${this.API_PATH_SERVER}/register/passenger`,
      dto
    );
  }

  login(loginData: JSON): Observable<string> {
    return this.http.post<string>(this.API_PATH_LOCAL + '/login', loginData);
  }

  setToken(token: string): void {
    localStorage.setItem('token', token);
  }

  getToken(): string | null {
    return localStorage.getItem('token');
  }

  removeToken(): void {
    localStorage.removeItem('token');
  }

  checkIfSomeoneIsLoggedIn(): boolean {
    if (this.getToken() !== null) {
      return true;
    }
    return false;
  }

  getAuthHeader(): HttpHeaders {
    if (localStorage.getItem('token')) {
      return new HttpHeaders({
        Authorization: 'Bearer ' + localStorage.getItem('token'),
        responseType: 'text',
      });
    } else {
      return new HttpHeaders();
    }
  }

  getUsername(): string {
    const jwt: JwtHelperService = new JwtHelperService();
    const user = this.getToken();
    if (user === null) {
      return '';
    }
    const decodedToken = jwt.decodeToken(user);
    return decodedToken.username;
  }

  logout() {
    return this.http.put<any>(
      this.API_PATH_LOCAL + '/logout',
      { token: this.getToken() },
      {
        headers: this.getAuthHeader(),
      }
    );
  }
}
