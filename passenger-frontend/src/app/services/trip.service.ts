import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { AddressDTO } from '../shared/model/address-dto';
import { AuthService } from './auth.service';
import { RouteDTO } from '../shared/model/route-dto';
import { Trip } from '../shared/model/trip';

@Injectable({
  providedIn: 'root',
})
export class TripService {
  API_PATH_SERVER = 'http://192.168.30.144:8080/trip-service';
  API_PATH_LOCAL = 'http://localhost:8080/trip-service';
  
  private newTripSubject = new Subject<Trip>();
  newTrip$ = this.newTripSubject.asObservable();

  private tripStatusUpdatedSubject = new Subject<number>();
  tripStatusUpdated$ = this.tripStatusUpdatedSubject.asObservable();
  
  
  constructor(private http: HttpClient, private authService: AuthService) {}

  sendNewTrip(trip: Trip) {
    this.newTripSubject.next(trip);
  }

  tripStatusUpdated(tripId: number) {
    this.tripStatusUpdatedSubject.next(tripId);
  }

  addressLookup(address: string): Observable<AddressDTO[]> {
    return this.http.get<AddressDTO[]>(
      `${this.API_PATH_LOCAL}/get/address?address=${address}`,
      {
        headers: this.authService.getAuthHeader(),
      }
    );
  }

  getRoutes(from: string, to: string): Observable<RouteDTO[]> {
    return this.http.get<RouteDTO[]>(
      `${this.API_PATH_LOCAL}/get/routes?from=${from}&to=${to}`,
      {
        headers: this.authService.getAuthHeader(),
      }
    );
  }

  orderTrip(route: RouteDTO): Observable<any> {
    return this.http.post<any>(`${this.API_PATH_LOCAL}/create`, route, {
      headers: this.authService.getAuthHeader(),
    });
  }

  getActiveTrips(): Observable<Trip[]> {
    return this.http.get<Trip[]>(
      `${
        this.API_PATH_LOCAL
      }/trip/active/get/${this.authService.getUsername()}`,
      {
        headers: this.authService.getAuthHeader(),
      }
    );
  }

  cancelTrip(tripCancellation: any): Observable<any> {
    return this.http.put<any>(
      `${this.API_PATH_LOCAL}/cancel/passenger`,
      tripCancellation,
      {
        headers: this.authService.getAuthHeader(),
      }
    );
  }
}
