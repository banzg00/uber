import { Component, OnDestroy, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import { MessageService } from 'primeng/api';
import { Notification } from '../shared/model/notification';
import { TripService } from '../services/trip.service';

@Component({
  selector: 'app-protected-layout',
  templateUrl: './protected-layout.component.html',
  styleUrls: ['./protected-layout.component.css'],
  providers: [MessageService],
})
export class ProtectedLayoutComponent implements OnInit, OnDestroy {
  webSocketEndPoint: string = 'http://localhost:8084/notification-service/ws';
  topic: string = '/topic/messages/passengers';
  stompClient: any;

  constructor(
    private authService: AuthService,
    private router: Router,
    private messageService: MessageService,
    private tripService: TripService
  ) {}

  ngOnInit(): void {
    this._connect();
  }
  ngOnDestroy(): void {
    this._disconnect();
  }

  _connect() {
    console.log('Initialize WebSocket Connection');
    let ws = new SockJS(this.webSocketEndPoint);
    this.stompClient = Stomp.over(ws);
    const _this = this;
    _this.stompClient.connect(
      {},
      function (frame: any) {
        _this.stompClient.subscribe(
          `${_this.topic}/${_this.authService.getUsername()}`,
          function (sdkEvent: any) {
            _this.onMessageReceived(sdkEvent);
          }
        );
        //_this.stompClient.reconnect_delay = 2000;
      },
      this.errorCallBack
    );
  }

  _disconnect() {
    if (this.stompClient !== null) {
      this.stompClient.disconnect();
    }
    console.log('Disconnected');
  }

  // on error, schedule a reconnection attempt
  errorCallBack(error: any) {
    console.log('errorCallBack -> ' + error);
    setTimeout(() => {
      this._connect();
    }, 5000);
  }

  onMessageReceived(message: any) {
    const json = JSON.parse(message.body);
    const notification: Notification = {
      tripId: json.tripId,
      username: json.username,
      content: json.content,
    };
    this.tripService.tripStatusUpdated(notification.tripId);
    this.messageService.add({
      key: 'notification',
      severity: 'info',
      summary: 'Trip notification',
      detail: notification.content,
    });
  }

  logout(): void {
    this.authService.logout().subscribe({
      next: () => {
        this.authService.removeToken();
        this.router.navigate(['/login']);
      },
      error: (err: HttpErrorResponse) => {
        console.log(err.error);
      },
    });
  }
}
