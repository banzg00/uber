export interface User {
  email: string;
  firstName: string;
  lastName: string;
  phoneNumber: string;
  username: string;
  credits: number;
}
