import { MapPoint } from './map-point';

export interface RouteDTO {
  coordinates: MapPoint[];
  totalTime: number;
  totalDistance: number;
  price: number;
  passengerUsername: string | null;
}
