export class RouteData {
  color = '';

  public constructor(
    public coordinates: L.LatLng[],
    public totalTime: number,
    public totalDistance: number,
    public price: number
  ) {}
}
