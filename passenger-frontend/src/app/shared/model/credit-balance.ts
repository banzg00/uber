export interface CreditBalance {
  successful: boolean | null;
  balance: number | null;
}
