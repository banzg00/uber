export interface Trip {
  id: number;
  startDate: Date | null;
  endDate: Date | null;
  routeId: number;
  tripStatus: string;
  passengerUsername: string;
  driverUsername: string | null;
  price: number;
  tripCancellationId: number | null;
  distance: number;
  source: string;
  destination: string;
}
