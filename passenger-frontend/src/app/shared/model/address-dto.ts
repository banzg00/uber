export interface AddressDTO {
  lat: number;
  lon: number;
  display_name: string;
}
