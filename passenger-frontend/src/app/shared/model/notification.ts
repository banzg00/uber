export interface Notification {
  tripId: number;
  username: string | null;
  content: string;
}
