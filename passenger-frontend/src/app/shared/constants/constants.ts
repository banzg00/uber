export const DEFAULT_LATITUDE = 45.24545;
export const DEFAULT_LONGITUDE = 19.83223;
export const DEFAULT_NAME = 'Novi Sad';
export const COLORS = ['red', 'blue'];
export const SRB_LATIN_TO_ENG_LATIN_MAP: { [key: string]: string } = {
  š: 's',
  đ: 'dj',
  č: 'c',
  ć: 'c',
  ž: 'z',
  Š: 'S',
  Đ: 'Dj',
  Č: 'C',
  Ć: 'C',
  Ž: 'Z',
};
