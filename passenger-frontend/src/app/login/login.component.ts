import { Component } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent {
  passwordFormControl = new FormControl('', [Validators.required]);
  usernameFormControl = new FormControl('', [Validators.required]);

  loginForm: FormGroup;
  matcher = new ErrorStateMatcher();
  error?: string;
  loggedIn?: boolean;

  constructor(
    private readonly authService: AuthService,
    private readonly fb: FormBuilder,
    private readonly router: Router
  ) {
    this.loginForm = fb.group({
      username: this.usernameFormControl,
      password: this.passwordFormControl,
    });
  }

  login(): void {
    if (!this.loginForm.valid) {
      return;
    }
    this.error = '';
    this.authService.login(this.loginForm.value).subscribe({
      next: (res: any) => {
        this.authService.setToken(res.jwtToken);
        this.router.navigate(['/home']);
      },
      error: (err: HttpErrorResponse) => {
        if (err.error.message) {
          this.error = err.error.message;
        } else {
          this.error = 'Something went wrong';
        }
      },
    });
  }
}
