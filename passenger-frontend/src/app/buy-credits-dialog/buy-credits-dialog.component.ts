import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { PaymentService } from '../services/payment.service';

@Component({
  selector: 'app-buy-credits-dialog',
  templateUrl: './buy-credits-dialog.component.html',
  styleUrls: ['./buy-credits-dialog.component.css'],
})
export class BuyCreditsDialogComponent {
  form = new FormGroup({
    credits: new FormControl('', [
      Validators.required,
      Validators.pattern('[1-9]+[0-9]*'),
    ]),
  });

  constructor(
    public dialogRef: MatDialogRef<BuyCreditsDialogComponent>,
    private readonly paymentService: PaymentService
  ) {}

  buyCredits(): void {
    if (!this.form.valid) {
      return;
    }

    this.paymentService.buyCredits(this.form.value.credits!).subscribe({
      next: (res) => {
        this.dialogRef.close({ successful: true, balance: res.balance });
      },
      error: () => {
        this.dialogRef.close({ successful: false, balance: null });
      },
    });
  }
}
