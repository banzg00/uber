import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyCreditsDialogComponent } from './buy-credits-dialog.component';

describe('BuyCreditsDialogComponent', () => {
  let component: BuyCreditsDialogComponent;
  let fixture: ComponentFixture<BuyCreditsDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BuyCreditsDialogComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BuyCreditsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
