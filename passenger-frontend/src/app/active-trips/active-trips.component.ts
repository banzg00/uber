import { Component, Input, OnInit } from '@angular/core';
import { TripService } from '../services/trip.service';
import { Trip } from '../shared/model/trip';
import { HttpErrorResponse } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { TripDetailsDialogComponent } from '../trip-details-dialog/trip-details-dialog.component';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-active-trips',
  templateUrl: './active-trips.component.html',
  styleUrls: ['./active-trips.component.css'],
  providers: [MessageService],
})
export class ActiveTripsComponent implements OnInit {
  activeTrips!: Trip[];
  @Input() newTrip!: Trip;

  constructor(
    private readonly tripService: TripService,
    public tripDialog: MatDialog,
    private readonly messageService: MessageService
  ) {}

  ngOnInit(): void {
    this.tripService.newTrip$.subscribe((newTrip) => {
      this.activeTrips.push(newTrip);
    });

    this.tripService.tripStatusUpdated$.subscribe((tripId) => {
      this.getActiveTrips();
    })

    this.getActiveTrips();
  }

  getActiveTrips() {
    this.tripService.getActiveTrips().subscribe({
      next: (res: Trip[]) => {
        this.activeTrips = res;
      },
      error: (err: HttpErrorResponse) => {
        console.log(err.error);
      },
    });
  }

  showDetails(trip: Trip) {
    const dialogRef = this.tripDialog.open(TripDetailsDialogComponent, {
      data: trip,
    });
    dialogRef.afterClosed().subscribe((res: any) => {
      if (res?.successful === null || res?.successful === undefined) {
        return;
      }
      if (res.successful) {
        this.messageService.add({
          key: 'cancel-trip-message',
          severity: 'success',
          summary: 'Trip cancelled successfully',
        });
        this.activeTrips = this.activeTrips.filter((t) => t.id !== trip.id);
      } else {
        this.messageService.add({
          key: 'cancel-trip-message',
          severity: 'error',
          summary: 'Trip not canceled',
          detail: 'There was an error with trip cancelling',
        });
      }
    });
  }
}
