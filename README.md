# Uber Application

This app is an Uber remake. It is not a copy, it is just my implementation of some kind of service like Uber. It is implemented using microservice architecture with 4 services: iam, trip, notification and payment. That architecture is implemented in Spring boot 2 and comunication between microservices is done using FeignClient. Frontend is implemented in Angular 15 and separated in two parts: Passenger app and Driver app so they can be used separately. Other technologies that are used on backend are:
 - Consul
 - RabbitMQ
 - Posgres
 - Retry (as a service recovery mechanism)
 - Logstash, Elasicsearch and Kibana
 - Prometheus and Grafana

*Note: Driver frontend is not implemented in this version of application

This application is only for studying purposes!