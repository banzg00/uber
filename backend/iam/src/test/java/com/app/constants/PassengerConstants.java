package com.app.constants;

public class PassengerConstants {

  public static final String PASSENGER_BASE_URL = "/api/uber/passenger/";
  public static final String GET_ALL_URL = "all";
  public static final String PASSENGER_USERNAME = "marko";
  public static final String PASSENGER_PASSWORD = "pass123";
  public static final String PASSENGER_NAME = "Marko";
  public static final String PASSENGER_FIRST_NAME_1 = "Marko";
  public static final String PASSENGER_LAST_NAME_1 = "Markovic";
  public static final String PASSENGER_USERNAME_1 = "marko123";
  public static final String PASSENGER_FIRST_NAME_2 = "Jovana";
  public static final String PASSENGER_LAST_NAME_2 = "Jovanovic";
  public static final String PASSENGER_USERNAME_2 = "jovanaa";
  public static final String PASSENGER_FIRST_NAME_3 = "Petar";
  public static final String PASSENGER_LAST_NAME_3 = "Petrovic";
  public static final String PASSENGER_USERNAME_3 = "petar";
  public static final int PASSENGER_LIST_SIZE = 3;
  public static final String INVALID_PASSENGER_USERNAME = "invalid";
  public static final double AMOUNT_1000 = 1000.00;
  public static final double AMOUNT_0 = 0.0;
  public static final double LAT = 22.44;
  public static final double LNG = 23.56;
  public static final double PRICE = 300.00;
  public static final double TOTAL_DISTANCE = 200;
  public static final int TOTAL_TIME = 200;

}
