package com.app.constants;

public class DriverConstants {

  public static final String DRIVER_BASE_URL = "/api/uber/driver/";
  public static final String GET_ALL_URL = "all";
  public static final String GET_DRIVER_PROFILE_CORRECT = "get/petar";
  public static final String GET_DRIVER_PROFILE_INCORRECT = "get/mathias";
  public static final String DRIVER_USERNAME = "petar";
  public static final String DRIVER_PASSWORD = "pass999";
  public static final String DRIVER_NAME = "Petar";
  public static final String DRIVER_FIRST_NAME_1 = "Marko";
  public static final String DRIVER_LAST_NAME_1 = "Markovic";
  public static final String DRIVER_USERNAME_1 = "marko123";
  public static final String DRIVER_FIRST_NAME_2 = "Jovana";
  public static final String DRIVER_LAST_NAME_2 = "Jovanovic";
  public static final String DRIVER_USERNAME_2 = "jovanaa";
  public static final String DRIVER_FIRST_NAME_3 = "Petar";
  public static final String DRIVER_LAST_NAME_3 = "Petrovic";
  public static final String DRIVER_USERNAME_3 = "petar";
  public static final int DRIVER_LIST_SIZE = 3;
  public static final String INVALID_DRIVER_USERNAME = "invalid";
}
