package com.app.constants;

import net.bytebuddy.utility.RandomString;

public class UserConstants {

  public final static String USER_PASSENGER_FIRST_NAME_1 = "Ana";
  public final static String USER_PASSENGER_LAST_NAME_1 = "Johnson";
  public final static String USER_PASSENGER_PASSWORD_1 = "$2a$08$cD.7ZFQ8ZaEUUoxKPDggzOEcvyPMf4sFKaEIbS8q28N1j6dUVJ/Wq"; // pass 123
  public final static String USER_PASSENGER_PASSWORD_1_RAW = "pass123";
  public final static String USER_PASSENGER_USERNAME_1 = "ana";
  public final static String USER_PASSENGER_PHONE_NUMBER_1 = "+381600000";
  public final static Long USER_PASSENGER_ID_1 = 1L;
  public final static String USER_PASSENGER_FIRST_NAME_2 = "David";
  public final static String USER_PASSENGER_LAST_NAME_2 = "Smith";
  public final static String USER_PASSENGER_PASSWORD_2 = "$2a$10$3WNXqri8F3T.lMKtnyYLfO03EEWVm6U6NtzvKdhnxYOeIazU6CB4a"; // jovana321
  public final static String USER_PASSENGER_USERNAME_2 = "david";
  public final static Long USER_DRIVER_ID = 3L;
  public final static String USER_DRIVER_FIRST_NAME = "John";
  public final static String USER_DRIVER_LAST_NAME = "Jackson";
  public final static String USER_DRIVER_PASSWORD = "$2a$10$UySSuVMtP1DdRfXqnNBzyOpQJfykPzmQ2jGyV.Obw1/BIoo1UfLRe"; // hello987
  public final static String USER_DRIVER_USERNAME = "john";
  public final static String USER_DRIVER_USERNAME_1 = "slavoljub";
  public final static boolean USER_ENABLED = false;
  public final static String USER_VERIFICATION_CODE = RandomString.make(64);
  public final static String VERIFICATION_RESPONSE = "Registration successfully verified";
  public final static String FROM_MAIL = "{$spring.mail.username}";
  public final static String USER_ROLE_NAME_PASSENGER = "ROLE_PASSENGER";
  public final static String USER_ROLE_NAME_DRIVER = "ROLE_DRIVER";
  public final static Long PAYMENT_ACCOUNT_ID = 1L;
}
