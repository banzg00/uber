package com.app.constants;

public class AuthenticationConstants {

  public static final String BASE_URL = "/api/uber/auth/";
  public static final String LOGIN_URL = "login";
  public static final String DRIVER_REGISTRATION_URL = "register/driver";
  public static final String PASSENGER_REGISTRATION_URL = "register/passenger";
  public static final String VERIFICATION_URL = "verify?code=";
  public static final String PASSENGER_USERNAME = "marko";
  public static final String PASSENGER_PASSWORD = "pass123";
  public static final String DRIVER_USERNAME = "petar";
  public static final String DRIVER_PASSWORD = "pass999";
  public static final String INCORRECT_USERNAME = "asdasdasd";
  public static final String INCORRECT_PASSWORD = "asdasdasdasd";
  public final static String USER_REGISTRATION_PASSENGER_FIRST_NAME = "David";
  public final static String USER_REGISTRATION_PASSENGER_LAST_NAME = "Johnson";
  public final static String USER_REGISTRATION_PASSENGER_PASSWORD_RAW = "pass123";
  public final static String USER_REGISTRATION_PASSENGER_USERNAME = "david";
  public final static String USER_REGISTRATION_PASSENGER_EMAIL = "david@ex.com";
  public final static String USER_REGISTRATION_PASSENGER_PHONE_NUMBER = "+3816000010";
  public final static String VERIFICATION_CODE = "fUlwIIyEWswz9M1kGHzAFdqa6A4O6W14XKCvfCqxrOe6BEN18rrunqJvraTLTU5C";
  public final static String VERIFICATION_RESPONSE = "Registration successfully verified";
  public final static String USER_REGISTRATION_DRIVER_FIRST_NAME = "David";
  public final static String USER_REGISTRATION_DRIVER_LAST_NAME = "Johnson";
  public final static String USER_REGISTRATION_DRIVER_PASSWORD_RAW = "pass123";
  public final static String USER_REGISTRATION_DRIVER_USERNAME = "david123";
  public final static String USER_REGISTRATION_DRIVER_PHONE_NUMBER = "+3816000010";
  public static final String USER_REGISTRATION_DRIVER_EMAIL = "slavoljub@index.com";
  public static final String USER_DRIVER_PHONE_NUMBER = "+474823834";
  public static final String DRIVER_VEHICLE_NUMBER = "83458834";
  public static final int DRIVER_YEARS_EXP = 13;

}
