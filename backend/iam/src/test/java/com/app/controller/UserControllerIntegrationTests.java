package com.app.controller;

import static com.app.constants.DriverConstants.DRIVER_BASE_URL;
import static com.app.constants.DriverConstants.DRIVER_PASSWORD;
import static com.app.constants.DriverConstants.DRIVER_USERNAME;
import static com.app.constants.DriverConstants.GET_DRIVER_PROFILE_CORRECT;
import static com.app.constants.DriverConstants.GET_DRIVER_PROFILE_INCORRECT;
import static com.app.constants.PassengerConstants.PASSENGER_PASSWORD;
import static com.app.constants.PassengerConstants.PASSENGER_USERNAME;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import com.app.constants.AuthenticationConstants;
import com.app.dto.DriverDTO;
import com.app.dto.LogInDTO;
import com.app.dto.LogInResponseDTO;
import java.util.Objects;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:application.properties")
public class UserControllerIntegrationTests {

  @Autowired
  private TestRestTemplate restTemplate;

  private String driverAccessToken;

  private String passengerAccessToken;

  @Before
  public void loginDriver() {
    String loginUrl =
        AuthenticationConstants.BASE_URL + AuthenticationConstants.LOGIN_URL;
    ResponseEntity<LogInResponseDTO> responseEntity = restTemplate.postForEntity(loginUrl,
        LogInDTO.builder().username(DRIVER_USERNAME).password(DRIVER_PASSWORD).build(),
        LogInResponseDTO.class);

    driverAccessToken = "Bearer " + Objects.requireNonNull(responseEntity.getBody()).getJwtToken();
  }

  @Before
  public void loginPassenger() {
    String loginUrl =
        AuthenticationConstants.BASE_URL + AuthenticationConstants.LOGIN_URL;
    ResponseEntity<LogInResponseDTO> responseEntity = restTemplate.postForEntity(loginUrl,
        LogInDTO.builder().username(PASSENGER_USERNAME).password(PASSENGER_PASSWORD).build(),
        LogInResponseDTO.class);

    passengerAccessToken = "Bearer " + Objects.requireNonNull(responseEntity.getBody()).getJwtToken();
  }

  @Test
  public void testGetDriverProfile_correct() {
    HttpHeaders headers = new HttpHeaders();
    headers.add(HttpHeaders.AUTHORIZATION, passengerAccessToken);
    headers.setContentType(MediaType.APPLICATION_JSON);
    HttpEntity<Object> request = new HttpEntity<>(headers);

    ResponseEntity<DriverDTO> responseEntity = restTemplate.exchange(
        DRIVER_BASE_URL + GET_DRIVER_PROFILE_CORRECT,
        HttpMethod.GET, request, new ParameterizedTypeReference<>() {});
    DriverDTO driver = responseEntity.getBody();

    assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    assertNotNull(driver);
    assertEquals(DRIVER_USERNAME, driver.getUsername());
  }

  @Test
  public void testGetDriverProfile_incorrect() {
    HttpHeaders headers = new HttpHeaders();
    headers.add(HttpHeaders.AUTHORIZATION, passengerAccessToken);
    headers.setContentType(MediaType.APPLICATION_JSON);
    HttpEntity<Object> request = new HttpEntity<>(headers);

    ResponseEntity<DriverDTO> responseEntity = restTemplate.exchange(
        DRIVER_BASE_URL + GET_DRIVER_PROFILE_INCORRECT,
        HttpMethod.GET, request, new ParameterizedTypeReference<>() {
        });
    DriverDTO driver = responseEntity.getBody();

    assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
    assertNotNull(driver);
    assertNull(driver.getUsername());
  }

}
