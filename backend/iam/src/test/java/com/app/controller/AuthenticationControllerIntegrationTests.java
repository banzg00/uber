package com.app.controller;

import static com.app.constants.AuthenticationConstants.BASE_URL;
import static com.app.constants.AuthenticationConstants.DRIVER_PASSWORD;
import static com.app.constants.AuthenticationConstants.DRIVER_REGISTRATION_URL;
import static com.app.constants.AuthenticationConstants.DRIVER_USERNAME;
import static com.app.constants.AuthenticationConstants.DRIVER_VEHICLE_NUMBER;
import static com.app.constants.AuthenticationConstants.DRIVER_YEARS_EXP;
import static com.app.constants.AuthenticationConstants.INCORRECT_PASSWORD;
import static com.app.constants.AuthenticationConstants.INCORRECT_USERNAME;
import static com.app.constants.AuthenticationConstants.LOGIN_URL;
import static com.app.constants.AuthenticationConstants.PASSENGER_PASSWORD;
import static com.app.constants.AuthenticationConstants.PASSENGER_REGISTRATION_URL;
import static com.app.constants.AuthenticationConstants.PASSENGER_USERNAME;
import static com.app.constants.AuthenticationConstants.USER_REGISTRATION_DRIVER_EMAIL;
import static com.app.constants.AuthenticationConstants.USER_REGISTRATION_DRIVER_FIRST_NAME;
import static com.app.constants.AuthenticationConstants.USER_REGISTRATION_DRIVER_LAST_NAME;
import static com.app.constants.AuthenticationConstants.USER_REGISTRATION_DRIVER_PASSWORD_RAW;
import static com.app.constants.AuthenticationConstants.USER_REGISTRATION_DRIVER_PHONE_NUMBER;
import static com.app.constants.AuthenticationConstants.USER_REGISTRATION_DRIVER_USERNAME;
import static com.app.constants.AuthenticationConstants.USER_REGISTRATION_PASSENGER_EMAIL;
import static com.app.constants.AuthenticationConstants.USER_REGISTRATION_PASSENGER_FIRST_NAME;
import static com.app.constants.AuthenticationConstants.USER_REGISTRATION_PASSENGER_LAST_NAME;
import static com.app.constants.AuthenticationConstants.USER_REGISTRATION_PASSENGER_PASSWORD_RAW;
import static com.app.constants.AuthenticationConstants.USER_REGISTRATION_PASSENGER_PHONE_NUMBER;
import static com.app.constants.AuthenticationConstants.USER_REGISTRATION_PASSENGER_USERNAME;
import static com.app.constants.AuthenticationConstants.VERIFICATION_CODE;
import static com.app.constants.AuthenticationConstants.VERIFICATION_RESPONSE;
import static com.app.constants.AuthenticationConstants.VERIFICATION_URL;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import com.app.dto.DriverRegistrationDTO;
import com.app.dto.LogInDTO;
import com.app.dto.LogInResponseDTO;
import com.app.dto.PassengerRegistrationDTO;
import com.app.dto.RegistrationResponseDTO;
import java.util.Objects;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:application.properties")
public class AuthenticationControllerIntegrationTests {

  @Autowired
  private TestRestTemplate restTemplate;

  @Test
  public void testLogIn_correct() {
    ResponseEntity<LogInResponseDTO> passenger = restTemplate.postForEntity(BASE_URL + LOGIN_URL,
        LogInDTO.builder().username(PASSENGER_USERNAME).password(PASSENGER_PASSWORD).build(),
        LogInResponseDTO.class);

    ResponseEntity<LogInResponseDTO> driver = restTemplate.postForEntity(BASE_URL + LOGIN_URL,
        LogInDTO.builder().username(DRIVER_USERNAME).password(DRIVER_PASSWORD).build(),
        LogInResponseDTO.class);

    assertEquals(HttpStatus.OK, passenger.getStatusCode());
    assertNotNull(Objects.requireNonNull(passenger.getBody()).getJwtToken());
    assertEquals(PASSENGER_USERNAME, passenger.getBody().getUsername());

    assertEquals(HttpStatus.OK, driver.getStatusCode());
    assertNotNull(Objects.requireNonNull(driver.getBody()).getJwtToken());
    assertEquals(DRIVER_USERNAME, driver.getBody().getUsername());
  }

  @Test
  public void testLogIn_usernameIncorrect() {
    ResponseEntity<LogInResponseDTO> passenger = restTemplate.postForEntity(BASE_URL + LOGIN_URL,
        LogInDTO.builder().username(INCORRECT_USERNAME).password(PASSENGER_PASSWORD).build(),
        LogInResponseDTO.class);

    ResponseEntity<LogInResponseDTO> driver = restTemplate.postForEntity(BASE_URL + LOGIN_URL,
        LogInDTO.builder().username(INCORRECT_USERNAME).password(DRIVER_PASSWORD).build(),
        LogInResponseDTO.class);

    assertEquals(HttpStatus.NOT_FOUND, passenger.getStatusCode());
    assertNull(Objects.requireNonNull(passenger.getBody()).getJwtToken());

    assertEquals(HttpStatus.NOT_FOUND, driver.getStatusCode());
    assertNull(Objects.requireNonNull(driver.getBody()).getJwtToken());
  }

  @Test
  public void testLogIn_passwordIncorrect() {
    ResponseEntity<LogInResponseDTO> passenger = restTemplate.postForEntity(BASE_URL + LOGIN_URL,
        LogInDTO.builder().username(PASSENGER_USERNAME).password(INCORRECT_PASSWORD).build(),
        LogInResponseDTO.class);

    ResponseEntity<LogInResponseDTO> driver = restTemplate.postForEntity(BASE_URL + LOGIN_URL,
        LogInDTO.builder().username(DRIVER_USERNAME).password(INCORRECT_PASSWORD).build(),
        LogInResponseDTO.class);

    assertEquals(HttpStatus.BAD_REQUEST, passenger.getStatusCode());
    assertNull(Objects.requireNonNull(passenger.getBody()).getJwtToken());

    assertEquals(HttpStatus.BAD_REQUEST, driver.getStatusCode());
    assertNull(Objects.requireNonNull(driver.getBody()).getJwtToken());
  }

  @Test
  public void testRegisterDriver() {
    DriverRegistrationDTO driverRegistrationDTO = DriverRegistrationDTO.builder()
        .username(USER_REGISTRATION_DRIVER_USERNAME)
        .password(USER_REGISTRATION_DRIVER_PASSWORD_RAW)
        .email(USER_REGISTRATION_DRIVER_EMAIL)
        .firstName(USER_REGISTRATION_DRIVER_FIRST_NAME)
        .lastName(USER_REGISTRATION_DRIVER_LAST_NAME)
        .phoneNumber(USER_REGISTRATION_DRIVER_PHONE_NUMBER)
        .vehicleNumber(DRIVER_VEHICLE_NUMBER)
        .yearsExperience(DRIVER_YEARS_EXP)
        .build();

    ResponseEntity<RegistrationResponseDTO> registrationResponseDTO = restTemplate.postForEntity(
        BASE_URL + DRIVER_REGISTRATION_URL, driverRegistrationDTO, RegistrationResponseDTO.class);

    assertEquals(HttpStatus.OK, registrationResponseDTO.getStatusCode());
    assertNotNull(registrationResponseDTO);
    assertEquals(Objects.requireNonNull(registrationResponseDTO.getBody()).getUsername(),
        USER_REGISTRATION_DRIVER_USERNAME);
  }

  @Test
  public void testRegisterDriverWithExistingUsername() {
    DriverRegistrationDTO driverRegistrationDTO = DriverRegistrationDTO.builder()
        .username(DRIVER_USERNAME)
        .password(USER_REGISTRATION_DRIVER_PASSWORD_RAW)
        .email(USER_REGISTRATION_DRIVER_EMAIL)
        .firstName(USER_REGISTRATION_DRIVER_FIRST_NAME)
        .lastName(USER_REGISTRATION_DRIVER_LAST_NAME)
        .phoneNumber(USER_REGISTRATION_DRIVER_PHONE_NUMBER)
        .vehicleNumber(DRIVER_VEHICLE_NUMBER)
        .yearsExperience(DRIVER_YEARS_EXP)
        .build();

    ResponseEntity<RegistrationResponseDTO> registrationResponseDTO = restTemplate.postForEntity(
        BASE_URL + DRIVER_REGISTRATION_URL, driverRegistrationDTO, RegistrationResponseDTO.class);

    assertEquals(HttpStatus.BAD_REQUEST, registrationResponseDTO.getStatusCode());
    assertNull(Objects.requireNonNull(registrationResponseDTO.getBody()).getUsername());
  }

  @Test
  public void testRegisterPassenger() {
    PassengerRegistrationDTO passengerRegistrationDTO = PassengerRegistrationDTO.builder()
        .username(USER_REGISTRATION_PASSENGER_USERNAME)
        .password(USER_REGISTRATION_PASSENGER_PASSWORD_RAW)
        .email(USER_REGISTRATION_PASSENGER_EMAIL)
        .firstName(USER_REGISTRATION_PASSENGER_FIRST_NAME)
        .lastName(USER_REGISTRATION_PASSENGER_LAST_NAME)
        .phoneNumber(USER_REGISTRATION_PASSENGER_PHONE_NUMBER)
        .build();

    ResponseEntity<RegistrationResponseDTO> registrationResponseDTO = restTemplate.postForEntity(
        BASE_URL + PASSENGER_REGISTRATION_URL, passengerRegistrationDTO,
        RegistrationResponseDTO.class);

    assertEquals(HttpStatus.OK, registrationResponseDTO.getStatusCode());
    assertNotNull(registrationResponseDTO);
    assertEquals(Objects.requireNonNull(registrationResponseDTO.getBody()).getUsername(),
        USER_REGISTRATION_PASSENGER_USERNAME);
  }

  @Test
  public void testRegisterPassengerWithExistingUsername() {
    PassengerRegistrationDTO passengerRegistrationDTO = PassengerRegistrationDTO.builder()
        .username(PASSENGER_USERNAME)
        .password(USER_REGISTRATION_PASSENGER_PASSWORD_RAW)
        .email(USER_REGISTRATION_PASSENGER_EMAIL)
        .firstName(USER_REGISTRATION_PASSENGER_FIRST_NAME)
        .lastName(USER_REGISTRATION_PASSENGER_LAST_NAME)
        .phoneNumber(USER_REGISTRATION_PASSENGER_PHONE_NUMBER)
        .build();

    ResponseEntity<RegistrationResponseDTO> registrationResponseDTO = restTemplate.postForEntity(
        BASE_URL + PASSENGER_REGISTRATION_URL, passengerRegistrationDTO,
        RegistrationResponseDTO.class);

    assertEquals(HttpStatus.BAD_REQUEST, registrationResponseDTO.getStatusCode());
    assertNull(Objects.requireNonNull(registrationResponseDTO.getBody()).getUsername());
  }

  @Test
  public void testVerifyRegistration() {
    ResponseEntity<String> retVal = restTemplate.getForEntity(
        BASE_URL + VERIFICATION_URL + VERIFICATION_CODE, String.class);

    assertEquals(HttpStatus.OK, retVal.getStatusCode());
    assertEquals(retVal.getBody(), VERIFICATION_RESPONSE);
  }
}
