package com.app.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.app.constants.PassengerConstants;
import com.app.exceptions.UserNotFoundException;
import com.app.model.Passenger;
import com.app.repository.IPassengerRepository;
import com.app.service.impl.PassengerService;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class PassengerServiceUnitTest {

  @Mock
  private IPassengerRepository passengerRepository;

  @InjectMocks
  private PassengerService passengerService;

  Passenger PASSENGER1;
  Passenger PASSENGER2;
  Passenger PASSENGER3;
  List<Passenger> PASSENGERS = new ArrayList<>();

  @BeforeEach
  public void init() {
    PASSENGER1 = Passenger.builder().firstName(PassengerConstants.PASSENGER_FIRST_NAME_1)
        .lastName(PassengerConstants.PASSENGER_LAST_NAME_1).username(
            PassengerConstants.PASSENGER_USERNAME_1).build();
    PASSENGER2 = Passenger.builder().firstName(PassengerConstants.PASSENGER_FIRST_NAME_2)
        .lastName(PassengerConstants.PASSENGER_LAST_NAME_2).username(
            PassengerConstants.PASSENGER_USERNAME_2).build();
    PASSENGER3 = Passenger.builder().firstName(PassengerConstants.PASSENGER_FIRST_NAME_3)
        .lastName(PassengerConstants.PASSENGER_LAST_NAME_3).username(
            PassengerConstants.PASSENGER_USERNAME_3).build();
    PASSENGERS.add(PASSENGER1);
    PASSENGERS.add(PASSENGER2);
    PASSENGERS.add(PASSENGER3);
  }

  @Test
  public void testGetAll() {
    when(passengerRepository.findAll()).thenReturn(PASSENGERS);

    List<Passenger> foundPassengers = passengerService.getAll();

    assertThat(foundPassengers).isEqualTo(PASSENGERS);
    assertThat(foundPassengers).hasSize(PassengerConstants.PASSENGER_LIST_SIZE);

    verify(passengerRepository, times(1)).findAll();
  }

  @Test
  public void testGetByUsername() {
    when(passengerRepository.findByUsername(PassengerConstants.PASSENGER_USERNAME_1)).thenReturn(Optional.of(PASSENGER1));

    Passenger foundPassenger = passengerService.getByUsername(
        PassengerConstants.PASSENGER_USERNAME_1);

    assertThat(foundPassenger).isNotNull();
    assertThat(foundPassenger.getUsername()).isEqualTo(PassengerConstants.PASSENGER_USERNAME_1);
    assertThat(foundPassenger).isEqualTo(PASSENGER1);

    verify(passengerRepository, times(1)).findByUsername(PassengerConstants.PASSENGER_USERNAME_1);
  }

  @Test
  public void testGetByInvalidUsername() {
    when(passengerRepository.findByUsername(PassengerConstants.PASSENGER_USERNAME)).thenReturn(
        Optional.of(PASSENGER3));

    assertThrows(UserNotFoundException.class, () -> {
      passengerService.getByUsername(PassengerConstants.INVALID_PASSENGER_USERNAME);
    });

    verify(passengerRepository, times(1)).findByUsername(
        PassengerConstants.INVALID_PASSENGER_USERNAME);
  }
}
