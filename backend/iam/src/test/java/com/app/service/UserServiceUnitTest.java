package com.app.service;

import static com.app.constants.AuthenticationConstants.DRIVER_VEHICLE_NUMBER;
import static com.app.constants.AuthenticationConstants.DRIVER_YEARS_EXP;
import static com.app.constants.AuthenticationConstants.USER_DRIVER_PHONE_NUMBER;
import static com.app.constants.AuthenticationConstants.USER_REGISTRATION_DRIVER_EMAIL;
import static com.app.constants.AuthenticationConstants.USER_REGISTRATION_PASSENGER_EMAIL;
import static com.app.constants.UserConstants.PAYMENT_ACCOUNT_ID;
import static com.app.constants.UserConstants.USER_DRIVER_FIRST_NAME;
import static com.app.constants.UserConstants.USER_DRIVER_LAST_NAME;
import static com.app.constants.UserConstants.USER_DRIVER_PASSWORD;
import static com.app.constants.UserConstants.USER_DRIVER_USERNAME;
import static com.app.constants.UserConstants.USER_DRIVER_USERNAME_1;
import static com.app.constants.UserConstants.USER_ENABLED;
import static com.app.constants.UserConstants.USER_PASSENGER_FIRST_NAME_1;
import static com.app.constants.UserConstants.USER_PASSENGER_FIRST_NAME_2;
import static com.app.constants.UserConstants.USER_PASSENGER_LAST_NAME_1;
import static com.app.constants.UserConstants.USER_PASSENGER_LAST_NAME_2;
import static com.app.constants.UserConstants.USER_PASSENGER_PASSWORD_1;
import static com.app.constants.UserConstants.USER_PASSENGER_PASSWORD_1_RAW;
import static com.app.constants.UserConstants.USER_PASSENGER_PASSWORD_2;
import static com.app.constants.UserConstants.USER_PASSENGER_PHONE_NUMBER_1;
import static com.app.constants.UserConstants.USER_PASSENGER_USERNAME_1;
import static com.app.constants.UserConstants.USER_PASSENGER_USERNAME_2;
import static com.app.constants.UserConstants.USER_ROLE_NAME_DRIVER;
import static com.app.constants.UserConstants.USER_ROLE_NAME_PASSENGER;
import static com.app.constants.UserConstants.USER_VERIFICATION_CODE;
import static com.app.constants.UserConstants.VERIFICATION_RESPONSE;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.app.client.PaymentServiceClient;
import com.app.client.TripServiceClient;
import com.app.dto.DriverRegistrationDTO;
import com.app.dto.PassengerRegistrationDTO;
import com.app.model.Driver;
import com.app.model.Passenger;
import com.app.model.User;
import com.app.model.UserRole;
import com.app.repository.IUserRepository;
import com.app.service.impl.UserService;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootTest
public class UserServiceUnitTest {

  @Mock
  private IUserRepository userRepository;

  @Mock
  private IUserRoleService roleService;

  @Mock
  private IMailService mailService;

  @Mock
  private TripServiceClient tripServiceClient;

  @Mock
  private PaymentServiceClient paymentServiceClient;

  @Mock
  private PasswordEncoder passwordEncoder;

  @InjectMocks
  private UserService userService;

  Passenger PASSENGER1;
  Passenger PASSENGER2;
  Driver DRIVER1;
  Driver DRIVER2;
  List<UserDetails> USERS = new ArrayList<>();
  UserRole USER_ROLE_1;
  UserRole USER_ROLE_2;

  @BeforeEach
  public void init() {
    USER_ROLE_1 = UserRole.builder().name(USER_ROLE_NAME_PASSENGER).build();
    USER_ROLE_2 = UserRole.builder().name(USER_ROLE_NAME_DRIVER).build();
    PASSENGER1 = Passenger.builder().username(USER_PASSENGER_USERNAME_1)
        .password(USER_PASSENGER_PASSWORD_1).firstName(USER_PASSENGER_FIRST_NAME_1)
        .email(USER_REGISTRATION_PASSENGER_EMAIL)
        .lastName(USER_PASSENGER_LAST_NAME_1).phoneNumber(USER_PASSENGER_PHONE_NUMBER_1)
        .userRole(USER_ROLE_1)
        .enabled(USER_ENABLED).verificationCode(USER_VERIFICATION_CODE).build();
    PASSENGER2 = Passenger.builder().firstName(USER_PASSENGER_FIRST_NAME_2)
        .lastName(USER_PASSENGER_LAST_NAME_2).username(USER_PASSENGER_USERNAME_2)
        .password(USER_PASSENGER_PASSWORD_2).build();
    DRIVER1 = Driver.builder().firstName(USER_DRIVER_FIRST_NAME).lastName(USER_DRIVER_LAST_NAME)
        .username(USER_DRIVER_USERNAME).password(USER_DRIVER_PASSWORD)
        .userRole(USER_ROLE_2).build();
    DRIVER2 = Driver.builder().username(USER_DRIVER_USERNAME_1)
        .password(USER_DRIVER_PASSWORD)
        .email(USER_REGISTRATION_DRIVER_EMAIL)
        .firstName(USER_DRIVER_FIRST_NAME).lastName(USER_DRIVER_LAST_NAME)
        .phoneNumber(USER_DRIVER_PHONE_NUMBER)
        .vehicleNumber(DRIVER_VEHICLE_NUMBER)
        .yearsExperience(DRIVER_YEARS_EXP)
        .build();
    USERS.add(PASSENGER1);
    USERS.add(PASSENGER2);
    USERS.add(DRIVER1);
    USERS.add(DRIVER2);
  }

  @SneakyThrows
  @Test
  public void testLoadUserByUsername() {
    when(userRepository.findByUsername(USER_PASSENGER_USERNAME_1)).thenReturn(
        Optional.of(PASSENGER1));
    when(userRepository.findByUsername(USER_DRIVER_USERNAME)).thenReturn(Optional.of(DRIVER1));

    UserDetails foundPassenger = userService.loadUserByUsername(USER_PASSENGER_USERNAME_1);
    UserDetails foundDriver = userService.loadUserByUsername(USER_DRIVER_USERNAME);

    assertThat(foundPassenger).isNotNull();
    assertThat(foundPassenger.getUsername()).isEqualTo(USER_PASSENGER_USERNAME_1);
    assertThat(foundDriver).isNotNull();
    assertThat(foundDriver.getUsername()).isEqualTo(USER_DRIVER_USERNAME);

    verify(userRepository, times(1)).findByUsername(USER_PASSENGER_USERNAME_1);
    verify(userRepository, times(1)).findByUsername(USER_DRIVER_USERNAME);
  }

  @SneakyThrows
  @Test
  public void testRegisterPassenger() {
    PassengerRegistrationDTO passengerRegistrationDTO = PassengerRegistrationDTO.builder()
        .username(USER_PASSENGER_USERNAME_1)
        .password(USER_PASSENGER_PASSWORD_1_RAW)
        .email(USER_REGISTRATION_PASSENGER_EMAIL)
        .firstName(USER_PASSENGER_FIRST_NAME_1).lastName(USER_PASSENGER_LAST_NAME_1)
        .phoneNumber(USER_PASSENGER_PHONE_NUMBER_1)
        .build();

    when(userRepository.save(any())).thenReturn(PASSENGER1);
    when(roleService.findByName(USER_ROLE_NAME_PASSENGER)).thenReturn(USER_ROLE_1);
    when(paymentServiceClient.createPaymentAccount()).thenReturn(PAYMENT_ACCOUNT_ID);

    User user = userService.registerPassenger(passengerRegistrationDTO);

    assertThat(user).isNotNull();
    assertThat(user.getUsername()).isEqualTo(USER_PASSENGER_USERNAME_1);

    verify(userRepository, times(2)).save(any());
    verify(roleService, times(1)).findByName(USER_ROLE_NAME_PASSENGER);
    verify(paymentServiceClient, times(1)).createPaymentAccount();
  }

  @SneakyThrows
  @Test
  public void testRegisterDriver() {
    DriverRegistrationDTO driverRegistrationDTO = DriverRegistrationDTO.builder()
        .username(USER_DRIVER_USERNAME_1)
        .password(USER_DRIVER_PASSWORD)
        .email(USER_REGISTRATION_DRIVER_EMAIL)
        .firstName(USER_DRIVER_FIRST_NAME).lastName(USER_DRIVER_LAST_NAME)
        .phoneNumber(USER_DRIVER_PHONE_NUMBER)
        .vehicleNumber(DRIVER_VEHICLE_NUMBER)
        .yearsExperience(DRIVER_YEARS_EXP)
        .build();

    when(userRepository.save(any())).thenReturn(DRIVER2);
    when(roleService.findByName(USER_ROLE_NAME_DRIVER)).thenReturn(USER_ROLE_2);
    when(paymentServiceClient.createPaymentAccount()).thenReturn(PAYMENT_ACCOUNT_ID);

    User user = userService.registerDriver(driverRegistrationDTO);

    assertThat(user).isNotNull();
    assertThat(user.getUsername()).isEqualTo(USER_DRIVER_USERNAME_1);

    verify(userRepository, times(2)).save(any());
    verify(roleService, times(1)).findByName(USER_ROLE_NAME_DRIVER);
    verify(paymentServiceClient, times(1)).createPaymentAccount();
  }

  @SneakyThrows
  @Test
  public void testVerifyRegistration() {
    when(userRepository.findByVerificationCode(USER_VERIFICATION_CODE)).thenReturn(
        Optional.of(PASSENGER1));
    when(userRepository.save(any())).thenReturn(PASSENGER1);

    String retVal = userService.verifyRegistration(USER_VERIFICATION_CODE);

    assertThat(retVal).isEqualTo(VERIFICATION_RESPONSE);

    verify(userRepository, times(1)).save(any());
    verify(userRepository, times(1)).findByVerificationCode(USER_VERIFICATION_CODE);
  }

}
