package com.app.service;

import static com.app.constants.AuthenticationConstants.USER_REGISTRATION_PASSENGER_EMAIL;
import static com.app.constants.UserConstants.FROM_MAIL;
import static com.app.constants.UserConstants.USER_PASSENGER_FIRST_NAME_1;
import static com.app.constants.UserConstants.USER_PASSENGER_LAST_NAME_1;
import static com.app.constants.UserConstants.USER_PASSENGER_PASSWORD_1;
import static com.app.constants.UserConstants.USER_PASSENGER_PASSWORD_1_RAW;
import static com.app.constants.UserConstants.USER_PASSENGER_PHONE_NUMBER_1;
import static com.app.constants.UserConstants.USER_PASSENGER_USERNAME_1;
import static com.app.constants.UserConstants.USER_ROLE_NAME_PASSENGER;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.app.config.MailConfig;
import com.app.dto.PassengerRegistrationDTO;
import com.app.model.Passenger;
import com.app.model.User;
import com.app.model.UserRole;
import com.app.service.impl.MailService;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.javamail.JavaMailSender;

@SpringBootTest
public class MailServiceUnitTests {

  @Mock
  private JavaMailSender mailSender;

  @Mock
  private MailConfig mailConfig;

  @InjectMocks
  private MailService mailService;

  User USER;
  UserRole USER_ROLE;

  @BeforeEach
  public void init() {
    USER_ROLE = UserRole.builder().name(USER_ROLE_NAME_PASSENGER).build();
    USER = Passenger.builder().username(USER_PASSENGER_USERNAME_1)
        .password(USER_PASSENGER_PASSWORD_1).firstName(USER_PASSENGER_FIRST_NAME_1)
        .email(USER_REGISTRATION_PASSENGER_EMAIL)
        .lastName(USER_PASSENGER_LAST_NAME_1).phoneNumber(USER_PASSENGER_PHONE_NUMBER_1)
        .userRole(USER_ROLE).build();
  }

  @SneakyThrows
  @Test
  public void testRegisterPassenger() {
    MimeMessage mimeMessage = new MimeMessage((Session) null);
    when(mailSender.createMimeMessage()).thenReturn(mimeMessage);
    when(mailConfig.getUsername()).thenReturn(FROM_MAIL);

    mailService.sendVerificationEmail(USER);

    verify(mailSender, times(1)).createMimeMessage();
    verify(mailSender, times(1)).send(mimeMessage);
    verify(mailConfig, times(1)).getUsername();
  }
}
