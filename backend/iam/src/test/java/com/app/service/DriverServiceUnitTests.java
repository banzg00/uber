package com.app.service;

import static com.app.constants.DriverConstants.DRIVER_FIRST_NAME_1;
import static com.app.constants.DriverConstants.DRIVER_FIRST_NAME_2;
import static com.app.constants.DriverConstants.DRIVER_FIRST_NAME_3;
import static com.app.constants.DriverConstants.DRIVER_LAST_NAME_1;
import static com.app.constants.DriverConstants.DRIVER_LAST_NAME_2;
import static com.app.constants.DriverConstants.DRIVER_LAST_NAME_3;
import static com.app.constants.DriverConstants.DRIVER_LIST_SIZE;
import static com.app.constants.DriverConstants.DRIVER_USERNAME;
import static com.app.constants.DriverConstants.DRIVER_USERNAME_1;
import static com.app.constants.DriverConstants.DRIVER_USERNAME_2;
import static com.app.constants.DriverConstants.DRIVER_USERNAME_3;
import static com.app.constants.DriverConstants.INVALID_DRIVER_USERNAME;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.app.exceptions.UserNotFoundException;
import com.app.model.Driver;
import com.app.repository.IDriverRepository;
import com.app.service.impl.DriverService;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class DriverServiceUnitTests {

  @Mock
  private IDriverRepository driverRepository;

  @InjectMocks
  private DriverService driverService;

  Driver DRIVER1;
  Driver DRIVER2;
  Driver DRIVER3;

  List<Driver> DRIVERS = new ArrayList<>();

  @BeforeEach
  public void init() {
    DRIVER1 = Driver.builder().firstName(DRIVER_FIRST_NAME_1).lastName(DRIVER_LAST_NAME_1)
        .username(DRIVER_USERNAME_1).build();
    DRIVER2 = Driver.builder().firstName(DRIVER_FIRST_NAME_2).lastName(DRIVER_LAST_NAME_2)
        .username(DRIVER_USERNAME_2).build();
    DRIVER3 = Driver.builder().firstName(DRIVER_FIRST_NAME_3).lastName(DRIVER_LAST_NAME_3)
        .username(DRIVER_USERNAME_3).build();
    DRIVERS.add(DRIVER1);
    DRIVERS.add(DRIVER2);
    DRIVERS.add(DRIVER3);
  }

  @SneakyThrows
  @Test
  public void testGetAll() {
    when(driverRepository.findAll()).thenReturn(DRIVERS);

    List<Driver> foundDrivers = driverService.getAll();

    assertThat(foundDrivers).isEqualTo(DRIVERS);
    assertThat(foundDrivers).hasSize(DRIVER_LIST_SIZE);

    verify(driverRepository, times(1)).findAll();
  }

  @SneakyThrows
  @Test
  public void testGetByUsername() {
    when(driverRepository.findByUsername(DRIVER_USERNAME)).thenReturn(Optional.of(DRIVER3));

    Driver foundDriver = driverService.getByUsername(DRIVER_USERNAME);

    assertThat(foundDriver).isNotNull();
    assertThat(foundDriver.getUsername()).isEqualTo(DRIVER_USERNAME);
    assertThat(foundDriver).isEqualTo(DRIVER3);

    verify(driverRepository, times(1)).findByUsername(DRIVER_USERNAME);
  }

  @SneakyThrows
  @Test
  public void testGetByInvalidUsername() {
    when(driverRepository.findByUsername(DRIVER_USERNAME)).thenReturn(Optional.of(DRIVER3));

    assertThrows(UserNotFoundException.class, () -> {
      driverService.getByUsername(INVALID_DRIVER_USERNAME);
    });

    verify(driverRepository, times(1)).findByUsername(INVALID_DRIVER_USERNAME);
  }

}
