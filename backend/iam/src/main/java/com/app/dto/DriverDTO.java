package com.app.dto;

import com.app.model.Driver;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder(toBuilder = true)
public class DriverDTO {

  private String username;
  private String firstName;
  private String lastName;
  private int yearsExperience;

  public DriverDTO(Driver driver) {
    this.username = driver.getUsername();
    this.firstName = driver.getFirstName();
    this.lastName = driver.getLastName();
    this.yearsExperience = driver.getYearsExperience();
  }
}
