package com.app.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class PassengerDTO {

  private Long id;

  @NotEmpty(message = "First name cannot be empty")
  private String firstName;

  @NotEmpty(message = "Last name cannot be empty")
  private String lastName;

  @Email(message = "Email must be in a specific email format")
  private String email;

  @NotEmpty(message = "Username cannot be empty")
  private String username;

  @NotEmpty(message = "Phone number cannot be empty")
  private String phoneNumber;

  private Double credits;

}
