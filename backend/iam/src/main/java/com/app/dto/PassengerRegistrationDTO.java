package com.app.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder(toBuilder = true)
public class PassengerRegistrationDTO {

  @NotEmpty(message = "Username cannot be empty")
  private String username;

  @NotEmpty(message = "Password cannot be empty")
  private String password;

  @Email(message = "Email is not valid")
  @NotEmpty(message = "Email cannot be empty")
  private String email;

  @NotEmpty(message = "First name cannot be empty")
  private String firstName;

  @NotEmpty(message = "Last name cannot be empty")
  private String lastName;

  @NotEmpty(message = "Phone number cannot be empty")
  private String phoneNumber;
}
