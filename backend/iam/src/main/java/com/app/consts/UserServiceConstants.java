package com.app.consts;

public class UserServiceConstants {

  public final static String MAIL_SENDER_NAME = "Uber";
  public final static String MAIL_SUBJECT = "Please verify your registration";
  public final static String SITE_URL = "http://localhost:8082/iam-service/api/uber/auth/verify?code=";
  public final static String MAIL_CONTENT =
      "Dear %s %s,<br>Please click the link below to verify you registration:<br> <h3><a href=%s%s target=\"_self\">VERIFY</a></h3>"
          + "Thank you,<br>The Uber team.";
  public final static String VERIFICATION_SUCCESS = "Registration successfully verified";
  public final static String VERIFICATION_FAILED = "Failed to verify user.";
}
