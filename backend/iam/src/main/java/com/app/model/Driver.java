package com.app.model;

import com.app.enums.DriverStatus;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@SuperBuilder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@DiscriminatorValue("driver")
public class Driver extends User {

  @Column
  private String vehicleNumber;

  @Column
  private Long addressId;

  @Column
  private int yearsExperience;

  @Enumerated(EnumType.STRING)
  private DriverStatus driverStatus;

  @Column
  private Long companyId;

  //private Set<Long> tripIds;
}

