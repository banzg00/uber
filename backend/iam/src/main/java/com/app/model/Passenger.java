package com.app.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder(toBuilder = true)
@AllArgsConstructor
//@NoArgsConstructor
@Entity
@DiscriminatorValue("passenger")
public class Passenger extends User {

  //private Set<Long> tripIds;
}

