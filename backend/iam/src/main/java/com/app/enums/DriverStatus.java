package com.app.enums;

public enum DriverStatus {
  AVAILABLE, INACTIVE, DRIVING
}
