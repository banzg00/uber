package com.app.security;

import com.app.cache.LoggedOutJwtTokenCache;
import com.app.event.OnUserLogoutSuccessEvent;
import com.app.model.User;
import com.app.service.impl.UserService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

@Component
public class JwtTokenUtil {

  private static final long EXPIRE_DURATION = 24 * 60 * 60 * 1000; // 24 hour
  private static final String APP_NAME = "uber";
  @Value("Authorization")
  private String AUTH_HEADER;
  @Value("abcdefghijklmnOPQRSTUVWXYZ")
  private String SECRET_KEY;

  @Autowired
  private LoggedOutJwtTokenCache loggedOutJwtTokenCache;

  public String generateToken(User user) {
    return Jwts.builder()
        .setIssuer(APP_NAME)
        .setIssuedAt(new Date())
        .setExpiration(generateExpirationDate())
        .claim("username", user.getUsername())
        .claim("id", user.getId())
        .claim("role", user.getUserRole().getName())
        .signWith(SignatureAlgorithm.HS512, SECRET_KEY)
        .compact();
  }

  public String getToken(HttpServletRequest request) {
    String authHeader = getAuthHeaderFromHeader(request);
    if (authHeader != null && authHeader.startsWith("Bearer ")) {
      return authHeader.substring(7);
    }
    return null;
  }

  public String getAuthHeaderFromHeader(HttpServletRequest request) {
    return request.getHeader(AUTH_HEADER);
  }

  private Date generateExpirationDate() {
    return new Date(new Date().getTime() + EXPIRE_DURATION);
  }

  public boolean validateToken(String token, UserDetails userDetails) {
    final String username = getUsernameFromToken(token);
    return (username != null && username.equals(userDetails.getUsername()) && !isTokenExpired(token)
        && validateTokenIsNotOnBlackList(token));
  }

  private boolean validateTokenIsNotOnBlackList(String authToken) {
    OnUserLogoutSuccessEvent previouslyLoggedOutEvent = loggedOutJwtTokenCache.getLogoutEventForToken(
        authToken);
    return previouslyLoggedOutEvent == null;
  }

  private Claims getAllClaimsFromToken(String token) {
    try {
      return Jwts.parser()
          .setSigningKey(SECRET_KEY)
          .parseClaimsJws(token)
          .getBody();
    } catch (Exception e) {
      return null;
    }
  }

  private boolean isTokenExpired(String token) {
    try {
      final Claims claims = this.getAllClaimsFromToken(token);
      Date expiration = claims.getExpiration();
      return expiration.before(new Date());
    } catch (Exception e) {
      return true;
    }
  }

  public String getUsernameFromToken(String token) {
    try {
      final Claims claims = this.getAllClaimsFromToken(token);
      return (String) claims.get("username");
    } catch (Exception e) {
      return null;
    }
  }

  public String getRoleFromToken(String token) {
    try {
      final Claims claims = this.getAllClaimsFromToken(token);
      return (String) claims.get("role");
    } catch (Exception e) {
      return null;
    }
  }

  public Date getExpirationFromToken(String token) {
    try {
      final Claims claims = this.getAllClaimsFromToken(token);
      return claims.getExpiration();
    } catch (Exception e) {
      return null;
    }
  }

  public boolean validateJWT(String authHeader, UserService userService, String userRole) {
    if (authHeader == null || !authHeader.startsWith("Bearer ")) {
      return false;
    }

    String jwt = authHeader.substring(7);

    if (!getRoleFromToken(jwt).equals(userRole)) {
      return false;
    }

    UserDetails userDetails = userService.loadUserByUsername(getUsernameFromToken(jwt));
    return validateToken(jwt, userDetails);
  }
}
