package com.app.controller.internal;

import static com.app.consts.LoggerConstant.log;

import com.app.security.JwtTokenUtil;
import com.app.service.impl.DriverService;
import com.app.service.impl.UserService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
@RequestMapping(value = "internal/auth")
public class AuthenticationControllerInternal {

  private final UserService userService;
  private final JwtTokenUtil jwtTokenUtil;
  private final DriverService driverService;

  @SneakyThrows
  @Operation(summary = "Validate jwt internal")
  @PostMapping(value = "/validate")
  public boolean validateJWT(@RequestHeader(HttpHeaders.AUTHORIZATION) String authHeader,
      @RequestBody String userRole) {

    log.info("Validating jwt internal");
    return jwtTokenUtil.validateJWT(authHeader, userService, userRole);
  }

  @SneakyThrows
  @Operation(summary = "Getting payment account internal")
  @GetMapping(value = "/paymentAccount/{username}")
  public Long getPaymentAccount(@PathVariable String username) {
    log.info(String.format("Getting payment account internal for user with username: %s",
        username));
    return userService.getPaymentAccount(username);
  }

  @SneakyThrows
  @Operation(summary = "Check if driver with given username exists")
  @GetMapping(value = "/check/driver/{username}")
  public boolean driverExist(@PathVariable String username) {
    return driverService.driverExist(username);
  }
}
