package com.app.controller;

import static com.app.consts.LoggerConstant.log;

import com.app.dto.DriverDTO;
import com.app.dto.PassengerDTO;
import com.app.model.Driver;
import com.app.service.IDriverService;
import com.app.service.IPassengerService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@AllArgsConstructor
@RestController
@RequestMapping(value = "api/uber/user")
public class UserController {

  private final IDriverService driverService;

  private final IPassengerService passengerService;

  @SneakyThrows
  @Operation(summary = "Get driver profile")
  @GetMapping(value = "/driver/get/{username}")
  public DriverDTO getDriverProfile(@PathVariable String username) {
    Driver driver = driverService.getByUsername(username);
    log.info(String.format("Successfully got driver profile with username: %s", username));
    return new DriverDTO(driver);
  }

  @SneakyThrows
  @Operation(summary = "Get passenger profile")
  @GetMapping(value = "/passenger/get/{username}")
  public PassengerDTO getPassengerProfile(@PathVariable String username) {
    PassengerDTO passenger = passengerService.getPassengerDTO(username);
    log.info(String.format("Successfully got passenger profile with username: %s", username));
    return passenger;
  }

  @SneakyThrows
  @Operation(summary = "Update passenger profile")
  @PutMapping(value = "/passenger/update")
  public ResponseEntity<?> updatePassengerProfile(@RequestBody PassengerDTO dto) {
    passengerService.updatePassenger(dto);
    log.info(String.format("Successfully updated passenger profile with username: %s",
        dto.getUsername()));
    return new ResponseEntity<>(HttpStatus.OK);
  }
}
