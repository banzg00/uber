package com.app.controller;

import static com.app.consts.LoggerConstant.log;

import com.app.dto.DriverRegistrationDTO;
import com.app.dto.LogInDTO;
import com.app.dto.LogInResponseDTO;
import com.app.dto.LogOutRequest;
import com.app.dto.PassengerRegistrationDTO;
import com.app.dto.RegistrationResponseDTO;
import com.app.event.OnUserLogoutSuccessEvent;
import com.app.model.User;
import com.app.security.JwtTokenUtil;
import com.app.service.CurrentUser;
import com.app.service.impl.UserService;
import io.swagger.v3.oas.annotations.Operation;
import javax.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
@RequestMapping(value = "api/uber/auth")
public class AuthenticationController {

  private final UserService userService;

  private final JwtTokenUtil jwtTokenUtil;

  @Autowired
  private AuthenticationManager authenticationManager;

  @Autowired
  private ApplicationEventPublisher applicationEventPublisher;

  @SneakyThrows
  @Operation(summary = "User login")
  @PostMapping(value = "/login")
  public LogInResponseDTO logIn(@Valid @RequestBody LogInDTO logInDTO) {
    Authentication authentication = authenticationManager
        .authenticate(new UsernamePasswordAuthenticationToken(logInDTO.getUsername(),
            logInDTO.getPassword()));

    SecurityContextHolder.getContext().setAuthentication(authentication);
    User user = (User) authentication.getPrincipal();

    String jwt = jwtTokenUtil.generateToken(user);
    LogInResponseDTO logInResponseDTO = LogInResponseDTO.builder().id(user.getId())
        .username(user.getUsername()).jwtToken(jwt).build();

    log.info(String.format("User with username: %s successfully logged in.", user.getUsername()));
    return logInResponseDTO;
  }

  @SneakyThrows
  @Operation(summary = "Driver registration")
  @PostMapping(value = "/register/driver")
  public RegistrationResponseDTO registerDriver(
      @Valid @RequestBody DriverRegistrationDTO driverRegistrationDTO) {

    User user = userService.registerDriver(driverRegistrationDTO);
    log.info(String.format("Driver with username: %s successfully registered.",
        user.getUsername()));
    return new RegistrationResponseDTO(user);
  }

  @SneakyThrows
  @Operation(summary = "Passenger registration")
  @PostMapping(value = "/register/passenger")
  public RegistrationResponseDTO registerPassenger(
      @Valid @RequestBody PassengerRegistrationDTO passengerRegistrationDTO) {

    User user = userService.registerPassenger(passengerRegistrationDTO);
    log.info(String.format("Passenger with username: %s successfully registered.",
        user.getUsername()));
    return new RegistrationResponseDTO(user);
  }

  @SneakyThrows
  @Operation(summary = "Verify user registration")
  @GetMapping(value = "/verify")
  public String verifyRegistration(@RequestParam("code") String code) {
    return userService.verifyRegistration(code);
  }

  @SneakyThrows
  @Operation(summary = "Log user out")
  @PutMapping("/logout")
  public ResponseEntity<?> logoutUser(@CurrentUser User currentUser,
      @Valid @RequestBody LogOutRequest logOutRequest) {

    OnUserLogoutSuccessEvent logoutSuccessEvent = new OnUserLogoutSuccessEvent(
        currentUser.getEmail(), logOutRequest.getToken(), logOutRequest);
    applicationEventPublisher.publishEvent(logoutSuccessEvent);
    return new ResponseEntity<>(HttpStatus.OK);
  }
}
