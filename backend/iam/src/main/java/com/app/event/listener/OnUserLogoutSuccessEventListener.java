package com.app.event.listener;

import static com.app.consts.LoggerConstant.log;

import com.app.cache.LoggedOutJwtTokenCache;
import com.app.event.OnUserLogoutSuccessEvent;
import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class OnUserLogoutSuccessEventListener implements
    ApplicationListener<OnUserLogoutSuccessEvent> {

  private final LoggedOutJwtTokenCache tokenCache;

  public void onApplicationEvent(OnUserLogoutSuccessEvent event) {
    log.info(String.format("Log out success event received for user [%s]", event.getUserEmail()));
    tokenCache.markLogoutEventForToken(event);
  }
}
