package com.app.cache;

import static com.app.consts.LoggerConstant.log;

import com.app.event.OnUserLogoutSuccessEvent;
import com.app.security.JwtTokenUtil;
import java.time.Instant;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import net.jodah.expiringmap.ExpiringMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LoggedOutJwtTokenCache {

  private final ExpiringMap<String, OnUserLogoutSuccessEvent> tokenEventMap;
  private JwtTokenUtil tokenProvider;

  public LoggedOutJwtTokenCache() {
    this.tokenEventMap = ExpiringMap.builder()
        .variableExpiration()
        .maxSize(1000)
        .build();
  }

  @Autowired
  public void setTokenProvider(JwtTokenUtil tokenProvider) {
    this.tokenProvider = tokenProvider;
  }

  public void markLogoutEventForToken(OnUserLogoutSuccessEvent event) {
    String token = event.getToken();
    if (tokenEventMap.containsKey(token)) {
      log.info(String.format("Log out token for user [%s] is already present in the cache",
          event.getUserEmail()));
    } else {
      Date tokenExpiryDate = tokenProvider.getExpirationFromToken(token);
      long ttlForToken = getTTLForToken(tokenExpiryDate);
      log.info(String.format(
          "Logout token cache set for [%s] with a TTL of [%s] seconds. Token is due expiry at [%s]",
          event.getUserEmail(), ttlForToken, tokenExpiryDate));
      tokenEventMap.put(token, event, ttlForToken, TimeUnit.SECONDS);
    }
  }

  public OnUserLogoutSuccessEvent getLogoutEventForToken(String token) {
    return tokenEventMap.get(token);
  }

  private long getTTLForToken(Date date) {
    long secondAtExpiry = date.toInstant().getEpochSecond();
    long secondAtLogout = Instant.now().getEpochSecond();
    return Math.max(0, secondAtExpiry - secondAtLogout);
  }
}
