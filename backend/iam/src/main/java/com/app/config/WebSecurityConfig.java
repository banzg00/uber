package com.app.config;

import com.app.security.JwtTokenFilter;
import com.app.security.JwtTokenUtil;
import com.app.security.RestAuthenticationEntryPoint;
import com.app.service.impl.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityCustomizer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig {

  @Autowired
  private JwtTokenUtil jwtTokenUtil;

  @Autowired
  private RestAuthenticationEntryPoint restAuthenticationEntryPoint;

  @Autowired
  private UserService userService;

  @Autowired
  public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
    auth.userDetailsService(userService);
  }

  @Bean
  public AuthenticationManager authenticationManager(HttpSecurity http,
      BCryptPasswordEncoder bCryptPasswordEncoder, UserService userDetailService)
      throws Exception {
    return http.getSharedObject(AuthenticationManagerBuilder.class)
        .userDetailsService(userService)
        .passwordEncoder(bCryptPasswordEncoder)
        .and()
        .build();
  }

  @Bean
  public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
    http
        .csrf().disable()
        .cors().and()
        .exceptionHandling().authenticationEntryPoint(restAuthenticationEntryPoint).and()
        .authorizeHttpRequests().antMatchers("api/uber/auth/login").permitAll()
        .antMatchers("api/uber/auth/register/**").permitAll()
        .antMatchers("api/uber/auth/verify").permitAll()
        .antMatchers("internal/auth/paymentAccount/**").permitAll()
        .antMatchers("internal/auth/check/driver/**").permitAll()
        .antMatchers("**/ws/**").permitAll()
        .antMatchers("actuator/prometheus").permitAll()
        .antMatchers("actuator/health").permitAll()
        .antMatchers("swagger-ui/**", "swagger-ui**", "/v3/api-docs/**", "/v3/api-docs**")
        .permitAll()
        .anyRequest().authenticated().and()
        .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
        .addFilterBefore(new JwtTokenFilter(jwtTokenUtil, userService),
            BasicAuthenticationFilter.class);
    return http.build();
  }

  @Bean
  public WebSecurityCustomizer webSecurityCustomizer() {
    return (web) -> web.ignoring()
        .antMatchers(HttpMethod.POST, "/api/uber/auth/**")
        .antMatchers(HttpMethod.GET, "/api/uber/auth/**")
        .antMatchers(HttpMethod.GET, "/internal/auth/paymentAccount/**")
        .antMatchers(HttpMethod.GET, "/internal/auth/check/driver/**")
        .antMatchers(HttpMethod.GET, "**/ws/**")
        .antMatchers(HttpMethod.POST, "**/ws/**")
        .antMatchers(HttpMethod.GET, "/actuator/prometheus")
        .antMatchers(HttpMethod.GET, "/actuator/health")
        .antMatchers("/", "/webjars/**", "/*.html", "/favicon.ico", "/**/*.html",
            "/**/*.css", "/**/*.js", "/*.js");
  }
}
