package com.app.repository;

import com.app.model.UserRole;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IUserRoleRepository extends JpaRepository<UserRole, Long> {

  Optional<UserRole> findByName(String name);
}
