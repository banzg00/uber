package com.app.repository;

import com.app.model.Passenger;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.NonNull;

public interface IPassengerRepository extends JpaRepository<Passenger, Long> {

  Optional<Passenger> findByUsername(String username);

  Passenger save(@NonNull Passenger passenger);
}
