package com.app.repository;

import com.app.model.Driver;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IDriverRepository extends JpaRepository<Driver, Long> {

  Optional<Driver> findByUsername(String username);
}
