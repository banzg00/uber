package com.app.exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.exception.ConstraintViolationException;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FieldValidationException extends Exception {

  private ConstraintViolationException exception;

  @Override
  public String getMessage() {
    return exception.getSQLException().getMessage();
  }
}
