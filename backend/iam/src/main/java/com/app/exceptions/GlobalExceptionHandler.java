package com.app.exceptions;

import static com.app.consts.LoggerConstant.log;

import java.util.Date;
import javax.mail.internet.AddressException;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.MailAuthenticationException;
import org.springframework.mail.MailSendException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;


@ControllerAdvice
public class GlobalExceptionHandler {

  @ExceptionHandler(EntityNotFoundException.class)
  public ResponseEntity<ErrorMessage> handleEntityNotFoundExceptionHandler(
      EntityNotFoundException exception, WebRequest request) {

    ErrorMessage errorMessage = new ErrorMessage(HttpStatus.NOT_FOUND.value(), new Date(),
        exception.getMessage(), request.getDescription(false));
    log.error(exception.getMessage());
    return new ResponseEntity<>(errorMessage, HttpStatus.NOT_FOUND);
  }

  @ExceptionHandler(UserNotFoundException.class)
  public ResponseEntity<ErrorMessage> handleUserNotFoundExceptionHandler(
      UserNotFoundException exception, WebRequest request) {

    ErrorMessage errorMessage = new ErrorMessage(HttpStatus.NOT_FOUND.value(), new Date(),
        exception.getMessage(), request.getDescription(false));
    log.error(exception.getMessage());
    return new ResponseEntity<>(errorMessage, HttpStatus.NOT_FOUND);
  }

  @ExceptionHandler(BadCredentialsException.class)
  public ResponseEntity<ErrorMessage> handleBadCredentialsExceptionHandler(
      BadCredentialsException exception, WebRequest request) {

    ErrorMessage errorMessage = new ErrorMessage(HttpStatus.BAD_REQUEST.value(), new Date(),
        "Incorrect password", request.getDescription(false));
    log.error("Incorrect password");
    return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(MethodArgumentNotValidException.class)
  public ResponseEntity<ErrorMessage> handleMethodArgumentNotValidExceptionHandler(
      MethodArgumentNotValidException exception, WebRequest request) {

    String message = exception.getBindingResult().getFieldErrors().stream()
        .map(DefaultMessageSourceResolvable::getDefaultMessage).findFirst().get();
    ErrorMessage errorMessage = new ErrorMessage(HttpStatus.BAD_REQUEST.value(), new Date(),
        message, request.getDescription(false));
    log.error(message);
    return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(DisabledException.class)
  public ResponseEntity<ErrorMessage> handleUserDisabledExceptionHandler(
      DisabledException exception, WebRequest request) {

    ErrorMessage errorMessage = new ErrorMessage(HttpStatus.BAD_REQUEST.value(), new Date(),
        exception.getMessage(), request.getDescription(false));
    log.error(exception.getMessage());
    return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(AddressException.class)
  public ResponseEntity<ErrorMessage> handleAddressExceptionHandler(AddressException exception,
      WebRequest request) {

    ErrorMessage errorMessage = new ErrorMessage(HttpStatus.BAD_REQUEST.value(), new Date(),
        "Invalid email address: " + exception.getMessage(), request.getDescription(false));
    log.error(String.format("Invalid email address: %s", exception.getMessage()));
    return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(MailSendException.class)
  public ResponseEntity<ErrorMessage> handleMailSendExceptionHandler(MailSendException exception,
      WebRequest request) {

    String message = exception.getFailedMessages().values().stream().map(Throwable::getMessage)
        .findFirst().get();
    ErrorMessage errorMessage = new ErrorMessage(HttpStatus.BAD_REQUEST.value(), new Date(),
        message, request.getDescription(false));
    log.error(message);
    return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
  }
  @ExceptionHandler(MailAuthenticationException.class)
  public ResponseEntity<ErrorMessage> handleMailAuthenticationExceptionHandler(
      MailAuthenticationException exception, WebRequest request) {
    ErrorMessage errorMessage = new ErrorMessage(HttpStatus.BAD_REQUEST.value(), new Date(),
        exception.getMessage(), request.getDescription(false));
    log.error(exception.getMessage());
    return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(ConstraintViolationException.class)
  public ResponseEntity<ErrorMessage> handleConstraintViolationExceptionHandler(
      ConstraintViolationException exception, WebRequest request) {

    ErrorMessage errorMessage = new ErrorMessage(HttpStatus.BAD_REQUEST.value(), new Date(),
        exception.getSQLException().getMessage(), request.getDescription(false));
    log.error(exception.getSQLException().getMessage());
    return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(UserVerificationException.class)
  public ResponseEntity<ErrorMessage> handleUserVerificationExceptionHandler(
      UserVerificationException exception, WebRequest request) {

    ErrorMessage errorMessage = new ErrorMessage(HttpStatus.BAD_REQUEST.value(), new Date(),
        exception.getMessage(), request.getDescription(false));
    log.error(exception.getMessage());
    return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(TripServiceException.class)
  public ResponseEntity<ErrorMessage> handleTripServiceExceptionHandler(
      TripServiceException exception, WebRequest request) {

    ErrorMessage errorMessage = new ErrorMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
        new Date(),
        exception.getMessage(), request.getDescription(false));
    log.error(exception.getMessage());
    return new ResponseEntity<>(errorMessage, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler(PaymentServiceException.class)
  public ResponseEntity<ErrorMessage> handlePaymentServiceExceptionHandler(
      PaymentServiceException exception, WebRequest request) {

    ErrorMessage errorMessage = new ErrorMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
        new Date(),
        exception.getMessage(), request.getDescription(false));
    log.error(exception.getMessage());
    return new ResponseEntity<>(errorMessage, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler(FieldValidationException.class)
  public ResponseEntity<ErrorMessage> handleDataIntegrityViolationExceptionHandler(
      FieldValidationException exception, WebRequest request) {

    ErrorMessage errorMessage = new ErrorMessage(HttpStatus.BAD_REQUEST.value(), new Date(),
        exception.getMessage(), request.getDescription(false));
    log.error(exception.getMessage());
    return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
  }
}
