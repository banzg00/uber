package com.app.exceptions;

import lombok.Getter;
import lombok.Setter;
import static com.app.consts.UserServiceConstants.VERIFICATION_FAILED;

@Getter
@Setter
public class UserVerificationException extends Exception{

  @Override
  public String getMessage() {
    return VERIFICATION_FAILED;
  }
}
