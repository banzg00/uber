package com.app.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name = "payment-service", url = "${spring.feign.payment-service-url}")
public interface PaymentServiceClient {

  @PostMapping(value = "createPaymentAccount")
  Long createPaymentAccount();

  @GetMapping("get/money/{paymentAccountId}")
  Double getPassengerMoney(@PathVariable Long paymentAccountId);
}
