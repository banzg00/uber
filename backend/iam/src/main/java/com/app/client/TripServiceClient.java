package com.app.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "trip-service", url = "${spring.feign.trip-service-url}")
public interface TripServiceClient {

  @GetMapping(value = "getCompany")
  Long getCompany();
}
