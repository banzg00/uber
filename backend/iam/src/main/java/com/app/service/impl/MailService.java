package com.app.service.impl;

import static com.app.consts.UserServiceConstants.MAIL_CONTENT;
import static com.app.consts.UserServiceConstants.MAIL_SENDER_NAME;
import static com.app.consts.UserServiceConstants.MAIL_SUBJECT;
import static com.app.consts.UserServiceConstants.SITE_URL;

import com.app.config.MailConfig;
import com.app.model.User;
import com.app.service.IMailService;
import javax.mail.internet.MimeMessage;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import static com.app.consts.LoggerConstant.log;

@AllArgsConstructor
@Service
public class MailService implements IMailService {

  @Autowired
  private final JavaMailSender mailSender;
  @Autowired
  private final MailConfig mailConfig;

  @Async
  @Override
  @SneakyThrows
  public void sendVerificationEmail(User user) {
    log.debug(String.format("Sending email to user with username: %s to email: %s",
        user.getUsername(), user.getEmail()));

    String toAddress = user.getEmail();
    String fromAddress = mailConfig.getUsername();
    String content = String.format(MAIL_CONTENT, user.getFirstName(), user.getLastName(),
        SITE_URL, user.getVerificationCode());

    MimeMessage message = mailSender.createMimeMessage();
    MimeMessageHelper helper = new MimeMessageHelper(message);
    helper.setFrom(fromAddress, MAIL_SENDER_NAME);
    helper.setTo(toAddress);
    helper.setSubject(MAIL_SUBJECT);
    helper.setText(content, true);

    mailSender.send(message);
  }
}
