package com.app.service;

import com.app.model.UserRole;

public interface IUserRoleService {

  UserRole findByName(String name);
}
