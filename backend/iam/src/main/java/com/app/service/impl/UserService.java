package com.app.service.impl;

import static com.app.consts.LoggerConstant.log;
import static com.app.consts.UserServiceConstants.VERIFICATION_SUCCESS;

import com.app.client.PaymentServiceClient;
import com.app.client.TripServiceClient;
import com.app.dto.DriverRegistrationDTO;
import com.app.dto.PassengerRegistrationDTO;
import com.app.enums.DriverStatus;
import com.app.exceptions.EntityNotFoundException;
import com.app.exceptions.FieldValidationException;
import com.app.exceptions.PaymentServiceException;
import com.app.exceptions.TripServiceException;
import com.app.exceptions.UserNotFoundException;
import com.app.exceptions.UserVerificationException;
import com.app.model.Driver;
import com.app.model.Passenger;
import com.app.model.User;
import com.app.repository.IUserRepository;
import com.app.service.IMailService;
import com.app.service.IUserRoleService;
import feign.FeignException.InternalServerError;
import feign.RetryableException;
import io.github.resilience4j.retry.annotation.Retry;

import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import javax.mail.internet.AddressException;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.mail.MailAuthenticationException;
import org.springframework.mail.MailSendException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@AllArgsConstructor
@Service
public class UserService implements UserDetailsService {

  private final IUserRepository userRepository;
  private final IUserRoleService userRoleService;
  private final IMailService mailService;
  private final PaymentServiceClient paymentServiceClient;
  private final TripServiceClient tripServiceClient;
  private final PasswordEncoder passwordEncoder;

  @Override
  @SneakyThrows
  public UserDetails loadUserByUsername(String username) {
    return userRepository.findByUsername(username)
        .orElseThrow(() -> new UserNotFoundException(username));
  }

  @Transactional(rollbackFor = {Exception.class})
  @Retry(name = "${spring.application.name}", fallbackMethod = "registerDriverFallback")
  public User registerDriver(DriverRegistrationDTO driverRegistrationDTO)
      throws DataIntegrityViolationException, FieldValidationException, PaymentServiceException,
      MailSendException, AddressException, TripServiceException, MailAuthenticationException {

    Long companyId;
    try {
      companyId = tripServiceClient.getCompany();
    } catch (InternalServerError | RetryableException e) {
      throw new TripServiceException();
    }
    User user = userRepository.save(
        Driver.builder().username(driverRegistrationDTO.getUsername())
            .password(passwordEncoder.encode(driverRegistrationDTO.getPassword()))
            .firstName(driverRegistrationDTO.getFirstName())
            .lastName(driverRegistrationDTO.getLastName())
            .email(driverRegistrationDTO.getEmail())
            .phoneNumber(driverRegistrationDTO.getPhoneNumber())
            .userRole(userRoleService.findByName("ROLE_DRIVER"))
            .vehicleNumber(driverRegistrationDTO.getVehicleNumber())
            .yearsExperience(driverRegistrationDTO.getYearsExperience())
            .driverStatus(DriverStatus.INACTIVE).enabled(false)
            .verificationCode(UUID.randomUUID().toString())
            .companyId(companyId).build());

    try {
      Long paymentAccountId = paymentServiceClient.createPaymentAccount();
      user.setPaymentAccountId(paymentAccountId);
    } catch (InternalServerError | RetryableException e) {
      throw new PaymentServiceException();
    }
    return saveNewUser(user);
  }

  public User registerDriverFallback(DriverRegistrationDTO driverRegistrationDTO,
      Exception e)
      throws DataIntegrityViolationException, FieldValidationException, AddressException,
      PaymentServiceException, MailSendException, TripServiceException, MailAuthenticationException {

    if (e.getCause() instanceof ConstraintViolationException) {
      throw new FieldValidationException((ConstraintViolationException) e.getCause());
    } else if (e instanceof MailSendException) {
      throw new MailSendException(e.getMessage());
    } else if (e instanceof AddressException) {
      throw new AddressException();
    } else if (e instanceof TripServiceException) {
      throw new TripServiceException("There was an error occurred with trip service"
          + " and driver is not registered.");
    } else if (e instanceof MailAuthenticationException) {
      throw new MailAuthenticationException(e.getMessage());
    }
    throw new PaymentServiceException("There was an error occurred with payment service"
        + " and driver is not registered.");
  }

  @Transactional(rollbackFor = {Exception.class})
  @Retry(name = "${spring.application.name}", fallbackMethod = "registerPassengerFallback")
  public User registerPassenger(PassengerRegistrationDTO passengerRegistrationDTO)
      throws DataIntegrityViolationException, FieldValidationException, PaymentServiceException,
      MailSendException, AddressException, MailAuthenticationException {

    User user = userRepository.save(
        Passenger.builder().username(passengerRegistrationDTO.getUsername())
            .password(passwordEncoder.encode(passengerRegistrationDTO.getPassword()))
            .firstName(passengerRegistrationDTO.getFirstName())
            .lastName(passengerRegistrationDTO.getLastName())
            .email(passengerRegistrationDTO.getEmail())
            .phoneNumber(passengerRegistrationDTO.getPhoneNumber())
            .userRole(userRoleService.findByName("ROLE_PASSENGER")).enabled(false)
            .verificationCode(UUID.randomUUID().toString()).build());

    try {
      Long paymentAccountId = paymentServiceClient.createPaymentAccount();
      user.setPaymentAccountId(paymentAccountId);
    } catch (InternalServerError | RetryableException e) {
      throw new PaymentServiceException();
    }
    return saveNewUser(user);
  }

  public User registerPassengerFallback(PassengerRegistrationDTO passengerRegistrationDTO,
      Exception e) throws DataIntegrityViolationException, FieldValidationException,
      PaymentServiceException, MailSendException, AddressException, MailAuthenticationException {

    if (e.getCause() instanceof ConstraintViolationException) {
      throw new FieldValidationException((ConstraintViolationException) e.getCause());
    } else if (e instanceof MailSendException) {
      throw new MailSendException(Objects.requireNonNull(e.getMessage()));
    } else if (e instanceof AddressException) {
      throw new AddressException();
    } else if (e instanceof MailAuthenticationException) {
      throw new MailAuthenticationException(Objects.requireNonNull(e.getMessage()));
    }
    throw new PaymentServiceException("There was an error occurred with payment service"
        + " and passenger is not registered.");
  }

  @SneakyThrows
  @Transactional
  private User saveNewUser(User user) {
    User newUser = userRepository.save(user);
    mailService.sendVerificationEmail(newUser);
    log.debug(String.format("Email successfully sent to user with username: %s",
        newUser.getUsername()));
    return newUser;
  }

  @SneakyThrows
  public String verifyRegistration(String verificationCode) {
    Optional<User> user = userRepository.findByVerificationCode(verificationCode);
    if (user.isEmpty() || user.get().isEnabled()) {
      log.error("Failed to verify user");
      throw new UserVerificationException();
    } else {
      User foundUser = user.get();
      foundUser.setEnabled(true);
      userRepository.save(foundUser);
      log.debug(String.format("Successfully verified user with username: %s",
          foundUser.getUsername()));
      return VERIFICATION_SUCCESS;
    }
  }

  @SneakyThrows
  public User getById(Long id) {
    return userRepository.findById(id)
        .orElseThrow(() -> new EntityNotFoundException("User", id));
  }

  @SneakyThrows
  public Long getPaymentAccount(String username) {
    User user = userRepository.findByUsername(username)
        .orElseThrow(() -> new UserNotFoundException(username));
    return user.getPaymentAccountId();
  }
}
