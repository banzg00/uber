package com.app.service.impl;

import com.app.dto.DriverDTO;
import com.app.exceptions.UserNotFoundException;
import com.app.model.Driver;
import com.app.repository.IDriverRepository;
import com.app.service.IDriverService;
import java.util.List;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import static com.app.consts.LoggerConstant.log;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
public class DriverService implements IDriverService {

  private IDriverRepository driverRepository;

  @Override
  public List<Driver> getAll() {
    return driverRepository.findAll();
  }

  @Override
  public Driver getByUsername(String username) throws UserNotFoundException {
    return driverRepository.findByUsername(username)
        .orElseThrow(() -> new UserNotFoundException(username));
  }

  @Override
  public List<DriverDTO> getAllDTO() {
    return convertToDTO(driverRepository.findAll());
  }

  @Override
  public boolean driverExist(String username) {
    try {
      getByUsername(username);
      log.debug(String.format("Driver with username: [%s] found", username));
    } catch (UserNotFoundException e) {
      log.debug(String.format("Driver with username: [%s] not found", username));
      return false;
    }
    return true;
  }

  private List<DriverDTO> convertToDTO(List<Driver> drivers) {
    return drivers.stream().map(DriverDTO::new).collect(Collectors.toList());
  }
}
