package com.app.service;

import com.app.dto.PassengerDTO;
import com.app.model.Passenger;
import java.util.List;

public interface IPassengerService {

  Passenger getByUsername(String username);

  List<Passenger> getAll();

  PassengerDTO getPassengerDTO(String username);

  void updatePassenger(PassengerDTO dto);

  //boolean checkEnoughMoney(RouteDTO dto, String username);
}
