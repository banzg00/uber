package com.app.service;

import com.app.model.User;

public interface IMailService {
  void sendVerificationEmail(User user);
}
