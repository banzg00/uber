package com.app.service.impl;

import com.app.exceptions.EntityNotFoundException;
import com.app.model.UserRole;
import com.app.repository.IUserRoleRepository;
import com.app.service.IUserRoleService;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

import static com.app.consts.LoggerConstant.log;

@AllArgsConstructor
@Service
public class UserRoleService implements IUserRoleService {

  private IUserRoleRepository userRoleRepository;

  @Override
  @SneakyThrows
  public UserRole findByName(String name) {
    log.debug(String.format("Getting user role with name: %s", name));
    return userRoleRepository.findByName(name)
        .orElseThrow(() -> new EntityNotFoundException("UserRole"));
  }
}
