package com.app.service;

import com.app.dto.DriverDTO;
import com.app.exceptions.UserNotFoundException;
import com.app.model.Driver;
import java.util.List;

public interface IDriverService {

  List<Driver> getAll();

  Driver getByUsername(String username) throws UserNotFoundException;

  List<DriverDTO> getAllDTO();

  boolean driverExist(String username);
}
