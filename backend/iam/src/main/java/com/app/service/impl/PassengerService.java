package com.app.service.impl;

import com.app.client.PaymentServiceClient;
import com.app.dto.PassengerDTO;
import com.app.exceptions.UserNotFoundException;
import com.app.model.Passenger;
import com.app.repository.IPassengerRepository;
import com.app.service.IPassengerService;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
public class PassengerService implements IPassengerService {

  private IPassengerRepository passengerRepository;

  private PaymentServiceClient paymentServiceClient;

  private UserService userService;

  @Override
  public List<Passenger> getAll() {
    return passengerRepository.findAll();
  }

  @Override
  @SneakyThrows
  public Passenger getByUsername(String username) {
    return passengerRepository.findByUsername(username)
        .orElseThrow(() -> new UserNotFoundException(username));
  }

  @Override
  public PassengerDTO getPassengerDTO(String username) {
    Passenger p = getByUsername(username);
    Long paymentAccountId = userService.getPaymentAccount(username);
    double credits = paymentServiceClient.getPassengerMoney(paymentAccountId);
    return PassengerDTO.builder().email(p.getEmail()).firstName(p.getFirstName())
        .lastName(p.getLastName()).username(username)
        .phoneNumber(p.getPhoneNumber()).credits(credits).build();
  }

  @Override
  public void updatePassenger(PassengerDTO dto) {
    Passenger passenger = getByUsername(dto.getUsername());
    passenger.setFirstName(dto.getFirstName());
    passenger.setLastName(dto.getLastName());
    passenger.setPhoneNumber(dto.getPhoneNumber());
    passengerRepository.save(passenger);
  }


}
