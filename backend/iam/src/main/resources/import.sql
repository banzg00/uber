insert into user_roles(name) values ('ROLE_PASSENGER');
insert into user_roles(name) values ('ROLE_DRIVER');

insert into users(type, username, password, email, first_name, last_name, phone_number, user_role_id, payment_account_id, enabled) values ('passenger', 'marko', '$2y$10$MdFDdo2Q4v4SVs.2nV8VruZ9bIUpikroFCvRzMPcK//gs2wpQ3BL.', 'marko@example.com', 'Marko', 'Markovic', '+381695230', 1, 1, 'true'); -- pass123
insert into users(type, username, password, email, first_name, last_name, phone_number, user_role_id, payment_account_id, vehicle_number, address_id, years_experience, driver_status, company_id, enabled) values ('driver', 'petar', '$2y$10$bCKHNGl1Cab9I6vPJ4wnhujmgdnLYtUwu4DNJPyzV2VPSs8ZvUjBO', 'petar@example.com', 'Petar', 'Petrovic', '+381967592', 2, 2, 'KP65166', 1, 1, 'AVAILABLE', 1, 'true'); --pass999
