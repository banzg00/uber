  ___    _    __  __
 |_ _|  / \  |  \/  |
  | |  / _ \ | |\/| |
  | | / ___ \| |  | |
 |___/_/   \_\_|  |_|

${application.title} ${application.version}
Powered by Spring Boot ${spring-boot.version}