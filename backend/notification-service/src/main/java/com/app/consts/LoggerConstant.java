package com.app.consts;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoggerConstant {

  public static final Logger log = LoggerFactory.getLogger("NotificationLogger");
}