package com.app.service.impl;

import static com.app.consts.LoggerConstant.log;

import com.app.dto.NotificationDTO;
import com.app.producer.RabbitMQProducer;
import com.app.service.INotificationService;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class DriverNotificationService implements INotificationService {

  private final RabbitMQProducer producer;

  @Autowired
  public DriverNotificationService(@Qualifier("driverProducer") RabbitMQProducer producer) {
    this.producer = producer;
  }

  @Override
  @SneakyThrows
  public void sendNotification(NotificationDTO notification) {
    producer.send(notification);
    log.debug(String.format("Notification for trip: [%d] sent to driver(s): [%s]",
        notification.getTripId(),
        notification.getUsername() == null ? "all" : notification.getUsername()));
  }

}
