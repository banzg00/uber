package com.app.service.impl;

import static com.app.consts.LoggerConstant.log;

import com.app.dto.NotificationDTO;
import com.app.producer.RabbitMQProducer;
import com.app.service.INotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class PassengerNotificationService implements INotificationService {

  private final RabbitMQProducer producer;

  @Autowired
  public PassengerNotificationService(@Qualifier("passengerProducer") RabbitMQProducer producer) {
    this.producer = producer;
  }

  @Override
  public void sendNotification(NotificationDTO notification) {
    producer.send(notification);
    log.debug(String.format("Notification for trip: [%d] sent to passenger: [%s]",
        notification.getTripId(), notification.getUsername()));
  }
}
