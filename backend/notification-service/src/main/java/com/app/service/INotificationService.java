package com.app.service;

import com.app.dto.NotificationDTO;

public interface INotificationService {

  void sendNotification(NotificationDTO notification);

}
