package com.app.config;

import lombok.Getter;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Getter
@Configuration
public class RabbitMQConfig {

  @Value("${spring.rabbitmq.exchange.name}")
  private String exchangeName;

  @Value("${spring.rabbitmq.queue.passenger.name}")
  private String passengerQueue;

  @Value("${spring.rabbitmq.routing.passenger.key}")
  private String passengerRoutingKey;

  @Value("${spring.rabbitmq.queue.driver.name}")
  private String driverQueue;

  @Value("${spring.rabbitmq.routing.driver.key}")
  private String driverRoutingKey;

  @Bean
  public Queue driversQueue() {
    return new Queue(driverQueue);
  }

  @Bean
  public Binding driversBinding() {
    return BindingBuilder
        .bind(driversQueue())
        .to(exchange())
        .with(driverRoutingKey);
  }

  @Bean
  public Queue passengersQueue() {
    return new Queue(passengerQueue);
  }

  @Bean
  public Binding passengersBinding() {
    return BindingBuilder
        .bind(passengersQueue())
        .to(exchange())
        .with(passengerRoutingKey);
  }

  @Bean
  public TopicExchange exchange() {
    return new TopicExchange(exchangeName);
  }

  @Bean
  public MessageConverter converter() {
    return new Jackson2JsonMessageConverter();
  }

  @Bean
  public AmqpTemplate amqpTemplate(ConnectionFactory connectionFactory) {
    RabbitTemplate template = new RabbitTemplate(connectionFactory);
    template.setMessageConverter(converter());
    return template;
  }

}
