package com.app.consumer;

import static com.app.consts.LoggerConstant.log;

import com.app.dto.NotificationDTO;
import lombok.AllArgsConstructor;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class RabbitMQConsumer {

  private final SimpMessagingTemplate messagingTemplate;

  @RabbitListener(queues = {"${spring.rabbitmq.queue.passenger.name}"})
  public void consumePassengerMessage(NotificationDTO notification, Message rabbitMessage) {
    String routingKey = rabbitMessage.getMessageProperties().getReceivedRoutingKey();
    String username = routingKey.substring(routingKey.indexOf(".") + 1);
    log.debug(String.format(
        "[PASSENGERS] Received message -> %s \twith routing key specific to passenger with username: [%s]",
        notification.toString(), username));

    messagingTemplate.convertAndSend(String.format("/topic/messages/passengers/%s", username),
        notification);
  }

  @RabbitListener(queues = {"${spring.rabbitmq.queue.driver.name}"})
  public void consumeDriverMessage(NotificationDTO notification, Message rabbitMessage) {
    String routingKey = rabbitMessage.getMessageProperties().getReceivedRoutingKey();
    String username = routingKey.substring(routingKey.indexOf(".") + 1);
    log.debug(String.format(
        "[DRIVERS] Received message -> %s \twith routing key specific to driver with username: [%s]",
        notification.toString(), username));

    messagingTemplate.convertAndSend(String.format("/topic/messages/drivers/%s", username),
        notification);
  }

}
