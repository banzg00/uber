package com.app.controller.internal;

import static com.app.consts.LoggerConstant.log;

import com.app.dto.NotificationDTO;
import com.app.service.INotificationService;
import com.app.service.impl.DriverNotificationService;
import com.app.service.impl.PassengerNotificationService;
import io.swagger.v3.oas.annotations.Operation;
import java.util.List;
import javax.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@RequestMapping(value = "internal")
public class NotificationControllerInternal {

  private final List<INotificationService> notificationServices;

  @SneakyThrows
  @PostMapping("send/driver")
  @Operation(summary = "Send notification to a specific driver or to all drivers")
  public ResponseEntity<String> sendDriverNotification(@Valid @RequestBody NotificationDTO dto) {
    for (var service : notificationServices) {
      if (service instanceof DriverNotificationService) {
        service.sendNotification(dto);
        String response = String.format("Notification for trip: [%d] sent to driver(s): [%s]",
            dto.getTripId(),
            dto.getUsername() == null ? "all" : dto.getUsername());
        log.info(response);
        return new ResponseEntity<>(response, HttpStatus.OK);
      }
    }
    return new ResponseEntity<>("There was an error occurred.", HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @SneakyThrows
  @PostMapping("send/passenger")
  @Operation(summary = "Send notification to a specific passenger")
  public ResponseEntity<String> sendPassengerNotification(@Valid @RequestBody NotificationDTO dto) {
    for (var service : notificationServices) {
      if (service instanceof PassengerNotificationService) {
        service.sendNotification(dto);
        String response = String.format("Notification for trip: [%d] sent to passenger: [%s]",
            dto.getTripId(), dto.getUsername());
        log.info(response);
        return new ResponseEntity<>(response, HttpStatus.OK);
      }
    }
    return new ResponseEntity<>("There was an error occurred.", HttpStatus.INTERNAL_SERVER_ERROR);
  }

}
