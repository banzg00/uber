package com.app.producer;

import static com.app.consts.LoggerConstant.log;

import com.app.config.RabbitMQConfig;
import com.app.dto.NotificationDTO;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
@Qualifier("passengerProducer")
public class RabbitMQPassengerProducer extends RabbitMQProducer {

  @Autowired
  public RabbitMQPassengerProducer(RabbitMQConfig rabbitMQConfig, RabbitTemplate template) {
    super(rabbitMQConfig, template);
  }

  @Override
  public void send(NotificationDTO notification) {
    log.debug(String.format("Message sent -> %s", notification.toString()));
    String routingKey = String.format("passengers.%s", notification.getUsername());
    template.convertAndSend(rabbitMQConfig.getExchangeName(), routingKey, notification);
  }
}
