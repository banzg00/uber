package com.app.producer;

import com.app.config.RabbitMQConfig;
import com.app.dto.NotificationDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

@Data
@AllArgsConstructor
public abstract class RabbitMQProducer {

  protected final RabbitMQConfig rabbitMQConfig;

  protected final RabbitTemplate template;

  public abstract void send(NotificationDTO notification);

}
