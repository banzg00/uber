package com.app.controller;

import static com.app.constants.Constants.CONTENT;
import static com.app.constants.Constants.DRIVER_USERNAME;
import static com.app.constants.Constants.PASSENGER_USERNAME;
import static com.app.constants.Constants.TRIP_ID1;
import static com.app.constants.URLs.BASE_URL;
import static com.app.constants.URLs.DRIVER_NOTIFICATION_URL;
import static com.app.constants.URLs.PASSENGER_NOTIFICATION_URL;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import com.app.dto.NotificationDTO;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class NotificationControllerIntegrationTests {

  @Autowired
  private TestRestTemplate restTemplate;
  private NotificationDTO driverMessage;
  private NotificationDTO passengerMessage;
  private NotificationDTO invalidMessage;

  @Before
  public void init() {
    driverMessage = NotificationDTO.builder().tripId(TRIP_ID1).username(DRIVER_USERNAME)
        .content(CONTENT).build();
    passengerMessage = NotificationDTO.builder().tripId(TRIP_ID1).username(PASSENGER_USERNAME)
        .content(CONTENT).build();
    invalidMessage = NotificationDTO.builder().tripId(TRIP_ID1).username(null)
        .content("").build();
  }

  @Test
  public void testSendPassengerNotification() {
    HttpEntity<NotificationDTO> request = new HttpEntity<>(passengerMessage);
    ResponseEntity<String> response = restTemplate.exchange(
        BASE_URL + PASSENGER_NOTIFICATION_URL,
        HttpMethod.POST,
        request,
        String.class
    );

    String resBody = response.getBody();
    assertEquals(HttpStatus.OK, response.getStatusCode());
    assertNotNull(resBody);
  }

  @Test
  public void testSendPassengerNotification_invalid() {
    HttpEntity<NotificationDTO> request = new HttpEntity<>(invalidMessage);
    ResponseEntity<String> response = restTemplate.exchange(
        BASE_URL + PASSENGER_NOTIFICATION_URL,
        HttpMethod.POST,
        request,
        String.class
    );

    String resBody = response.getBody();
    assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    assertNotNull(resBody);
  }

  @Test
  public void testSendDriverNotification() {
    HttpEntity<NotificationDTO> request = new HttpEntity<>(driverMessage);
    ResponseEntity<String> response = restTemplate.exchange(
        BASE_URL + DRIVER_NOTIFICATION_URL,
        HttpMethod.POST,
        request,
        String.class
    );

    String resBody = response.getBody();
    assertEquals(HttpStatus.OK, response.getStatusCode());
    assertNotNull(resBody);
  }

  @Test
  public void testSendDriverNotification_invalid() {
    HttpEntity<NotificationDTO> request = new HttpEntity<>(invalidMessage);
    ResponseEntity<String> response = restTemplate.exchange(
        BASE_URL + DRIVER_NOTIFICATION_URL,
        HttpMethod.POST,
        request,
        String.class
    );

    String resBody = response.getBody();
    assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    assertNotNull(resBody);
  }

}
