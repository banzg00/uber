package com.app.constants;

public class URLs {

  public static final String BASE_URL = "/internal/";
  public static final String PASSENGER_NOTIFICATION_URL = "sendPassengerNotification";
  public static final String DRIVER_NOTIFICATION_URL = "sendDriverNotification";

}
