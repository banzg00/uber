package com.app.service;

import static com.app.constants.Constants.CONTENT;
import static com.app.constants.Constants.PASSENGER_USERNAME;
import static com.app.constants.Constants.TRIP_ID1;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.app.dto.NotificationDTO;
import com.app.producer.RabbitMQPassengerProducer;
import com.app.service.impl.PassengerNotificationService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class PassengerNotificationServiceTests {

  @Mock
  private RabbitMQPassengerProducer producer;

  @InjectMocks
  private PassengerNotificationService notificationService;

  private NotificationDTO notification;

  @BeforeEach
  public void init() {
    notification = NotificationDTO.builder().tripId(TRIP_ID1).username(PASSENGER_USERNAME)
        .content(CONTENT).build();
  }

  @Test
  public void sendPassengerNotification() {
    notificationService.sendNotification(notification);
    verify(producer, times(1)).send(notification);
  }

}
