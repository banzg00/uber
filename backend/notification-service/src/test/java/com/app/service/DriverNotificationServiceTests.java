package com.app.service;

import static com.app.constants.Constants.CONTENT;
import static com.app.constants.Constants.DRIVER_USERNAME;
import static com.app.constants.Constants.TRIP_ID1;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.app.dto.NotificationDTO;
import com.app.producer.RabbitMQDriverProducer;
import com.app.service.impl.DriverNotificationService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class DriverNotificationServiceTests {

  @Mock
  private RabbitMQDriverProducer producer;

  @InjectMocks
  private DriverNotificationService notificationService;

  private NotificationDTO singleDriverNotification;

  private NotificationDTO allDriversNotification;

  @BeforeEach
  public void init() {
    singleDriverNotification = NotificationDTO.builder().tripId(TRIP_ID1).username(DRIVER_USERNAME)
        .content(CONTENT).build();
    allDriversNotification = NotificationDTO.builder().tripId(TRIP_ID1).username(null)
        .content(CONTENT).build();
  }

  @Test
  public void sendSingleDriverNotification() {
    notificationService.sendNotification(singleDriverNotification);
    verify(producer, times(1)).send(singleDriverNotification);
  }

  @Test
  public void sendAllDriverNotification() {
    notificationService.sendNotification(allDriversNotification);
    verify(producer, times(1)).send(allDriversNotification);
  }

}
