package com.app.filter;

import static com.app.util.Constants.BEARER;
import static com.app.util.Constants.INTERNAL;

import com.app.config.PathConfig;
import com.app.service.JwtService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@Component
public class AuthenticationFilter implements GatewayFilter {

  @Autowired
  private PathConfig pathConfig;

  @Autowired
  private JwtService jwtService;

  @Override
  public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
    String path = exchange.getRequest().getPath().value();

    if (pathConfig.getUnprotected().stream().anyMatch(path::contains))
      return chain.filter(exchange);

    if (path.contains(INTERNAL)) {
      exchange.getResponse().setStatusCode(HttpStatus.FORBIDDEN);
      return exchange.getResponse().setComplete();
    }

    String authorizationHeader = exchange.getRequest().getHeaders()
        .getFirst(HttpHeaders.AUTHORIZATION);

    if (authorizationHeader == null || !authorizationHeader.startsWith(BEARER)) {
      exchange.getResponse().setStatusCode(HttpStatus.BAD_REQUEST);
      return exchange.getResponse().setComplete();
    }

    if (pathConfig.getPermissible().stream().anyMatch(path::contains)) {
      return jwtService.validateJWT(authorizationHeader).flatMap(valid -> {
        if (valid) {
          return chain.filter(exchange);
        }
        exchange.getResponse().setStatusCode(HttpStatus.UNAUTHORIZED);
        return exchange.getResponse().setComplete();
      });
    }

    exchange.getResponse().setStatusCode(HttpStatus.BAD_REQUEST);
    return exchange.getResponse().setComplete();
  }
}
