package com.app.service;

import static com.app.util.Constants.*;
import static com.app.util.LoggerConstant.log;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service
public class JwtService {

  @Autowired
  private WebClient.Builder webClientBuilder;

  @Value("${iam-url}")
  private String iamUrl;

  @SneakyThrows
  public Mono<Boolean> validateJWT(String authorizationHeader) {
    log.info("Validating jwt token");
    return webClientBuilder.build()
        .post()
        .uri(iamUrl + IAM_JWT_VALIDATION_URL)
        .headers(httpHeaders -> {
          httpHeaders.add(HttpHeaders.AUTHORIZATION, authorizationHeader);
          httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        })
        .body(Mono.just(ROLE_DRIVER), String.class)
        .retrieve()
        .bodyToMono(Boolean.class)
        .onErrorResume(throwable -> Mono.just(false));
  }
}
