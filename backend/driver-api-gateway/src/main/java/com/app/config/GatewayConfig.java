package com.app.config;

import com.app.filter.AuthenticationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class GatewayConfig {

  @Autowired
  private AuthenticationFilter filter;

  @Bean
  @LoadBalanced
  public WebClient.Builder loadBalancedWebClientBuilder() {
    return WebClient.builder();
  }

  @Bean
  public RouteLocator routes(RouteLocatorBuilder builder) {
    return builder.routes()
        .route("iam-service",
            r -> r.path("/iam-service/**").filters(f -> f.filter(filter)).uri("lb://iam-service"))
        .route("payment-service",
            r -> r.path("/payment-service/**").filters(f -> f.filter(filter)).uri("lb://payment-service"))
        .route("trip-service",
            r -> r.path("/trip-service/**").filters(f -> f.filter(filter)).uri("lb://trip-service"))
        .build();
  }
}
