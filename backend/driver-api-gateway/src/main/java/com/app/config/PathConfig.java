package com.app.config;

import java.util.List;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
@ConfigurationProperties("allowed-path")
public class PathConfig {
  
  private List<String> unprotected;
  private List<String> permissible;
}
