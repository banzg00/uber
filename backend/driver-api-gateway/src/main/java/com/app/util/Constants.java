package com.app.util;

public class Constants {
  public static final String INTERNAL = "/internal/";
  public static final String BEARER = "Bearer ";
  public static final String ROLE_DRIVER = "ROLE_DRIVER";
  public static final String IAM_JWT_VALIDATION_URL = "/auth/validate";
}
