package com.app.service;

import static com.app.constants.TransactionConstants.COMPANY_PENALTY_COEFFICIENT;
import static com.app.constants.TransactionConstants.HUGE_TRANSACTION_AMOUNT;
import static com.app.constants.TransactionConstants.PAYMENT_ACCOUNT_AMOUNT_1;
import static com.app.constants.TransactionConstants.PAYMENT_ACCOUNT_AMOUNT_2;
import static com.app.constants.TransactionConstants.PAYMENT_ACCOUNT_AMOUNT_NOT_ENOUGH;
import static com.app.constants.TransactionConstants.PAYMENT_ACCOUNT_ID_1;
import static com.app.constants.TransactionConstants.PAYMENT_ACCOUNT_ID_2;
import static com.app.constants.TransactionConstants.PAY_IN_TRANSACTION_TYPE;
import static com.app.constants.TransactionConstants.PAY_OUT_TRANSACTION_TYPE;
import static com.app.constants.TransactionConstants.TRANSACTION_AMOUNT;
import static com.app.constants.TransactionConstants.TRANSACTION_AMOUNT_PENALTY;
import static com.app.constants.TransactionConstants.TRANSACTION_DATE;
import static com.app.constants.TransactionConstants.TRANSACTION_ID_1;
import static com.app.constants.TransactionConstants.TRANSACTION_ID_2;
import static com.app.constants.TransactionConstants.TRANSACTION_ID_3;
import static com.app.constants.TransactionConstants.TRANSACTION_ID_4;
import static com.app.constants.TransactionConstants.TRANSACTION_TYPE;
import static com.app.constants.TransactionConstants.TRIP_ID_1;
import static com.app.constants.TransactionConstants.TRIP_ID_2;
import static com.app.constants.TransactionConstants.TRIP_PRICE_1;
import static com.app.constants.TransactionConstants.TRIP_PRICE_2;
import static com.app.constants.TransactionConstants.TRIP_STATUS_1;
import static com.app.constants.TransactionConstants.TRIP_STATUS_2;
import static com.app.constants.UserConstants.USER_DRIVER_USERNAME;
import static com.app.constants.UserConstants.USER_PASSENGER_USERNAME;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.app.client.IAMServiceClient;
import com.app.dto.PayInCreditsDTO;
import com.app.dto.PayOutCreditsDTO;
import com.app.dto.TransactionDTO;
import com.app.dto.TripPaymentDTO;
import com.app.exceptions.CreditPayOutException;
import com.app.exceptions.IAMServiceException;
import com.app.exceptions.PaymentException;
import com.app.model.PaymentAccount;
import com.app.model.Transaction;
import com.app.repository.ITransactionRepository;
import com.app.service.impl.TransactionService;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class TransactionServiceUnitTests {

  @Mock
  private ITransactionRepository transactionRepository;

  @Mock
  private IPaymentAccountService paymentAccountService;

  @Mock
  private IAMServiceClient iAMServiceClient;

  @InjectMocks
  private TransactionService transactionService;

  TripPaymentDTO TRIP1;
  TripPaymentDTO TRIP2;
  Transaction TRANSACTION1;
  Transaction TRANSACTION2;
  Transaction TRANSACTION3;
  Transaction TRANSACTION4;
  PaymentAccount PAYMENT_ACCOUNT1;
  PaymentAccount PAYMENT_ACCOUNT2;

  @BeforeEach
  public void init() {
    PAYMENT_ACCOUNT1 = PaymentAccount.builder().id(PAYMENT_ACCOUNT_ID_1)
        .amount(PAYMENT_ACCOUNT_AMOUNT_1).build();
    PAYMENT_ACCOUNT2 = PaymentAccount.builder().id(PAYMENT_ACCOUNT_ID_2)
        .amount(PAYMENT_ACCOUNT_AMOUNT_2).build();
    TRIP1 = TripPaymentDTO.builder().tripId(TRIP_ID_1).tripStatus(TRIP_STATUS_1)
        .passengerUsername(USER_PASSENGER_USERNAME).price(TRIP_PRICE_1)
        .penaltyCoefficient(COMPANY_PENALTY_COEFFICIENT)
        .driverUsername(USER_DRIVER_USERNAME).build();
    TRIP2 = TripPaymentDTO.builder().tripId(TRIP_ID_2).tripStatus(TRIP_STATUS_2)
        .passengerUsername(USER_PASSENGER_USERNAME).price(TRIP_PRICE_2)
        .penaltyCoefficient(COMPANY_PENALTY_COEFFICIENT)
        .driverUsername(USER_DRIVER_USERNAME).build();
    TRANSACTION1 = Transaction.builder().date(TRANSACTION_DATE).amount(TRANSACTION_AMOUNT)
        .transactionType(TRANSACTION_TYPE).id(TRANSACTION_ID_1)
        .paymentAccountFrom(PAYMENT_ACCOUNT1).paymentAccountTo(PAYMENT_ACCOUNT2).build();
    TRANSACTION2 = Transaction.builder().date(TRANSACTION_DATE).amount(TRANSACTION_AMOUNT_PENALTY)
        .transactionType(TRANSACTION_TYPE).id(TRANSACTION_ID_2)
        .paymentAccountFrom(PAYMENT_ACCOUNT1).paymentAccountTo(PAYMENT_ACCOUNT2).build();
    TRANSACTION3 = Transaction.builder().id(TRANSACTION_ID_3).date(TRANSACTION_DATE).amount(TRANSACTION_AMOUNT)
        .transactionType(PAY_IN_TRANSACTION_TYPE).paymentAccountTo(PAYMENT_ACCOUNT1).build();
    TRANSACTION4 = Transaction.builder().id(TRANSACTION_ID_4).date(TRANSACTION_DATE).amount(TRANSACTION_AMOUNT)
        .transactionType(PAY_OUT_TRANSACTION_TYPE).paymentAccountTo(PAYMENT_ACCOUNT2).build();
  }

  @SneakyThrows
  @Test
  public void testChargeTrip() {
    when(transactionRepository.save(any(Transaction.class))).thenReturn(TRANSACTION1);
    when(paymentAccountService.getById(PAYMENT_ACCOUNT_ID_1)).thenReturn(PAYMENT_ACCOUNT1);
    when(paymentAccountService.getById(PAYMENT_ACCOUNT_ID_2)).thenReturn(PAYMENT_ACCOUNT2);
    when(iAMServiceClient.getPaymentAccount(USER_PASSENGER_USERNAME)).thenReturn(
        PAYMENT_ACCOUNT_ID_1);
    when(iAMServiceClient.getPaymentAccount(USER_DRIVER_USERNAME)).thenReturn(
        PAYMENT_ACCOUNT_ID_2);

    Long transactionId = transactionService.chargeTrip(TRIP1);

    assertThat(transactionId).isNotNull();

    verify(transactionRepository, times(1)).save(any(Transaction.class));
    verify(paymentAccountService, times(1)).getById(PAYMENT_ACCOUNT_ID_1);
    verify(paymentAccountService, times(1)).getById(PAYMENT_ACCOUNT_ID_2);
    verify(iAMServiceClient, times(1)).getPaymentAccount(USER_PASSENGER_USERNAME);
    verify(iAMServiceClient, times(1)).getPaymentAccount(USER_DRIVER_USERNAME);
  }

  @SneakyThrows
  @Test
  public void testChargeTripNotEnoughCredit() {
    PAYMENT_ACCOUNT1.setAmount(PAYMENT_ACCOUNT_AMOUNT_NOT_ENOUGH);

    when(paymentAccountService.getById(PAYMENT_ACCOUNT_ID_1)).thenReturn(PAYMENT_ACCOUNT1);
    when(paymentAccountService.getById(PAYMENT_ACCOUNT_ID_2)).thenReturn(PAYMENT_ACCOUNT2);
    when(iAMServiceClient.getPaymentAccount(USER_PASSENGER_USERNAME)).thenReturn(
        PAYMENT_ACCOUNT_ID_1);
    when(iAMServiceClient.getPaymentAccount(USER_DRIVER_USERNAME)).thenReturn(
        PAYMENT_ACCOUNT_ID_2);

    assertThrows(PaymentException.class, () -> {
      transactionService.chargeTrip(TRIP1);
    });
  }

  @SneakyThrows
  @Test
  public void testChargeTripPenalty() {
    when(transactionRepository.save(any(Transaction.class))).thenReturn(TRANSACTION2);
    when(paymentAccountService.getById(PAYMENT_ACCOUNT_ID_1)).thenReturn(PAYMENT_ACCOUNT1);
    when(paymentAccountService.getById(PAYMENT_ACCOUNT_ID_2)).thenReturn(PAYMENT_ACCOUNT2);
    when(iAMServiceClient.getPaymentAccount(USER_PASSENGER_USERNAME)).thenReturn(
        PAYMENT_ACCOUNT_ID_1);
    when(iAMServiceClient.getPaymentAccount(USER_DRIVER_USERNAME)).thenReturn(
        PAYMENT_ACCOUNT_ID_2);

    Long transactionId = transactionService.chargeTripPenalty(TRIP2);

    assertThat(transactionId).isNotNull();

    verify(transactionRepository, times(1)).save(any(Transaction.class));
    verify(paymentAccountService, times(1)).getById(PAYMENT_ACCOUNT_ID_1);
    verify(paymentAccountService, times(1)).getById(PAYMENT_ACCOUNT_ID_2);
    verify(iAMServiceClient, times(1)).getPaymentAccount(USER_PASSENGER_USERNAME);
    verify(iAMServiceClient, times(1)).getPaymentAccount(USER_DRIVER_USERNAME);
  }

  @SneakyThrows
  @Test
  public void testPayTripPenaltyNotEnoughCredit() {
    PAYMENT_ACCOUNT1.setAmount(PAYMENT_ACCOUNT_AMOUNT_NOT_ENOUGH);

    when(paymentAccountService.getById(PAYMENT_ACCOUNT_ID_1)).thenReturn(PAYMENT_ACCOUNT1);
    when(paymentAccountService.getById(PAYMENT_ACCOUNT_ID_2)).thenReturn(PAYMENT_ACCOUNT2);
    when(iAMServiceClient.getPaymentAccount(USER_PASSENGER_USERNAME)).thenReturn(
        PAYMENT_ACCOUNT_ID_1);
    when(iAMServiceClient.getPaymentAccount(USER_DRIVER_USERNAME)).thenReturn(
        PAYMENT_ACCOUNT_ID_2);

    assertThrows(PaymentException.class, () -> {
      transactionService.chargeTrip(TRIP2);
    });
  }

  @Test
  public void testPayInCredits() {
    when(iAMServiceClient.getPaymentAccount(USER_PASSENGER_USERNAME)).thenReturn(
        PAYMENT_ACCOUNT_ID_1);
    when(transactionRepository.save(any(Transaction.class))).thenReturn(TRANSACTION3);
    when(paymentAccountService.getById(PAYMENT_ACCOUNT_ID_1)).thenReturn(PAYMENT_ACCOUNT1);

    PayInCreditsDTO payInCreditsDTO = PayInCreditsDTO.builder().username(USER_PASSENGER_USERNAME)
        .amount(TRANSACTION_AMOUNT).build();

    TransactionDTO transaction = transactionService.payInCredits(payInCreditsDTO);

    assertThat(transaction).isNotNull();

    verify(transactionRepository, times(1)).save(any(Transaction.class));
    verify(iAMServiceClient, times(1)).getPaymentAccount(USER_PASSENGER_USERNAME);
    verify(paymentAccountService, times(1)).getById(PAYMENT_ACCOUNT_ID_1);
  }

  @Test
  public void testPayOutCredits() throws IAMServiceException, CreditPayOutException {
    when(iAMServiceClient.getPaymentAccount(USER_DRIVER_USERNAME)).thenReturn(
        PAYMENT_ACCOUNT_ID_2);
    when(transactionRepository.save(any(Transaction.class))).thenReturn(TRANSACTION4);
    when(paymentAccountService.getById(PAYMENT_ACCOUNT_ID_2)).thenReturn(PAYMENT_ACCOUNT2);

    PayOutCreditsDTO payOutCreditsDTO = PayOutCreditsDTO.builder().username(USER_DRIVER_USERNAME)
        .amount(TRANSACTION_AMOUNT).build();

    Transaction transaction = transactionService.payOutCredits(payOutCreditsDTO);

    assertThat(transaction).isNotNull();
    assertThat(transaction.getAmount()).isEqualTo(TRANSACTION_AMOUNT);

    verify(transactionRepository, times(1)).save(any(Transaction.class));
    verify(iAMServiceClient, times(1)).getPaymentAccount(USER_DRIVER_USERNAME);
    verify(paymentAccountService, times(1)).getById(PAYMENT_ACCOUNT_ID_2);
  }

  @Test
  public void testPayOutCreditsInvalidAmount() {
    when(iAMServiceClient.getPaymentAccount(USER_DRIVER_USERNAME)).thenReturn(
        PAYMENT_ACCOUNT_ID_2);
    when(paymentAccountService.getById(PAYMENT_ACCOUNT_ID_2)).thenReturn(PAYMENT_ACCOUNT2);

    PayOutCreditsDTO payOutCreditsDTO = PayOutCreditsDTO.builder().username(USER_DRIVER_USERNAME)
        .amount(HUGE_TRANSACTION_AMOUNT).build();

    assertThrows(CreditPayOutException.class, () -> {
      transactionService.payOutCredits(payOutCreditsDTO);
    });
  }
}
