package com.app.constants;

import com.app.enums.TransactionType;

import java.time.LocalDateTime;

public class TransactionConstants {

  public final static Long TRIP_ID_1 = 1L;
  public final static String TRIP_STATUS_1 = "ENDED";
  public final static double TRIP_PRICE_1 = 500;
  public final static Long TRIP_ID_2 = 2L;
  public final static String TRIP_STATUS_2 = "CANCELED";
  public final static double TRIP_PRICE_2 = 400;
  public final static double TRANSACTION_AMOUNT = 500;
  public final static LocalDateTime TRANSACTION_DATE = LocalDateTime.now();
  public final static TransactionType TRANSACTION_TYPE = TransactionType.PAYMENT;
  public final static Long PAYMENT_ACCOUNT_ID_1 = 1L;
  public final static Long PAYMENT_ACCOUNT_ID_2 = 2L;
  public final static double PAYMENT_ACCOUNT_AMOUNT_1 = 1000;
  public final static double PAYMENT_ACCOUNT_AMOUNT_2 = 2000;
  public final static double PAYMENT_ACCOUNT_AMOUNT_NOT_ENOUGH = 0;
  public final static double COMPANY_PENALTY_COEFFICIENT = 0.2;
  public final static double TRANSACTION_AMOUNT_PENALTY =
      COMPANY_PENALTY_COEFFICIENT * TRIP_PRICE_2;
  public final static TransactionType PAY_IN_TRANSACTION_TYPE = TransactionType.PAY_IN;
  public final static TransactionType PAY_OUT_TRANSACTION_TYPE = TransactionType.PAY_OUT;
  public final static double HUGE_TRANSACTION_AMOUNT = 50000;
  public static final String TRANSACTION_BASE_URL = "/api/uber/transaction/";
  public static final String PAY_IN_CREDITS_URL = "credit/payIn";
  public static final String PAY_OUT_CREDITS_URL = "credit/payOut";
  public static final Long TRANSACTION_ID_1 = 1L;
  public static final Long TRANSACTION_ID_2 = 2L;
  public static final Long TRANSACTION_ID_3 = 3L;
  public static final Long TRANSACTION_ID_4 = 4L;
}
