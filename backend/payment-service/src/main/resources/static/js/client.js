var stripe = Stripe(stripePublicKey);

var purchase = {
    email: email,
    amount: amount,
    featureRequest: featureRequest
};

fetch("/payment-service/api/uber/stripe/createPaymentIntent", {
    method: "POST",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify(purchase)
})
    .then(function(result) {
        return result.json();
    })
    .then(function(data) {
        var elements = stripe.elements();

        var style = {
            base: {
                color: "#32325d",
                fontFamily: 'Arial, sans-serif',
                fontSmoothing: "antialiased",
                fontSize: "16px",
                "::placeholder": {
                    color: "#32325d"
                }
            },
            invalid: {
                fontFamily: 'Arial, sans-serif',
                color: "#fa755a",
                iconColor: "#fa755a"
            }
        };

        var card = elements.create("card", { style: style });
        card.mount("#card-element");

        card.on("change", function (event) {
            document.querySelector("button").disabled = event.empty;
            document.querySelector("#card-error").textContent = event.error ? event.error.message : "";
        });

        var form = document.getElementById("payment-form");
        form.addEventListener("submit", function(event) {
            event.preventDefault();
            payWithCard(stripe, card, data.clientSecret);
        });
    });

var payWithCard = function(stripe, card, clientSecret) {
    stripe
        .confirmCardPayment(clientSecret, {
            receipt_email: email,
            payment_method: {
                card: card,
                billing_details: {
                    email: email
                }
            }
        })
        .then(function(result) {
            if (result.error) {
                alert("Payment failed.");
            } else {
                fetch('/payment-service/api/uber/transaction/credit/payIn', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({ "passengerId": 1, "amount": purchase.amount })
                })
                  .then(function(result) {
                    if(result.error) {
                      alert("Payment failed.");
                    } else {
                      alert("Payment succeeded.");
                    }
                  })
                }
        });
};

