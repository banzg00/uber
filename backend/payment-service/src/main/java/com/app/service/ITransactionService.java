package com.app.service;

import com.app.dto.PayInCreditsDTO;
import com.app.dto.PayOutCreditsDTO;
import com.app.dto.TransactionDTO;
import com.app.dto.TripPaymentDTO;
import com.app.exceptions.CreditPayOutException;
import com.app.exceptions.IAMServiceException;
import com.app.exceptions.PaymentException;
import com.app.model.Transaction;

public interface ITransactionService {

  Long chargeTrip(TripPaymentDTO tripPaymentDTO)
      throws PaymentException, IAMServiceException;

  Long chargeTripPenalty(TripPaymentDTO tripPaymentDTO)
      throws PaymentException, IAMServiceException;

  TransactionDTO payInCredits(PayInCreditsDTO buyCreditsDTO);

  Transaction payOutCredits(PayOutCreditsDTO payOutCreditsDTO)
      throws CreditPayOutException, IAMServiceException;
}
