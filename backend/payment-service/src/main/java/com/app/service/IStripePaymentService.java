package com.app.service;

import com.app.dto.CreateStripePaymentDTO;
import com.app.dto.CreateStripePaymentResponseDTO;

public interface IStripePaymentService {

  CreateStripePaymentResponseDTO createStripePaymentIntent(
      CreateStripePaymentDTO createStripePaymentDTO);
}
