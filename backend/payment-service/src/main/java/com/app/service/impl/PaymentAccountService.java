package com.app.service.impl;

import static com.app.util.LoggerConstant.log;

import com.app.client.IAMServiceClient;
import com.app.exceptions.EntityNotFoundException;
import com.app.model.PaymentAccount;
import com.app.repository.IPaymentAccountRepository;
import com.app.service.IPaymentAccountService;
import java.util.Optional;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
public class PaymentAccountService implements IPaymentAccountService {

  private final IPaymentAccountRepository paymentAccountRepository;
  @Autowired
  private final IAMServiceClient IAMServiceClient;

  @Override
  public void updatePaymentAccountAmount(PaymentAccount paymentAccount, double amount) {
    log.debug(String.format("Updating payment account with id: %d", paymentAccount.getId()));
    paymentAccount.setAmount(amount);
    paymentAccountRepository.save(paymentAccount);
  }

  @SneakyThrows
  @Override
  public PaymentAccount getById(Long id) {
    Optional<PaymentAccount> trip = paymentAccountRepository.findById(id);
    if (trip.isPresent()) {
      return trip.get();
    } else {
      throw new EntityNotFoundException("Payment account", id);
    }
  }

  @Override
  public boolean checkPaymentAccountBalance(double price, String username) {
    Long paymentAccountId = IAMServiceClient.getPaymentAccount(username);
    PaymentAccount paymentAccount = getById(paymentAccountId);
    return paymentAccount.getAmount() >= price;
  }

  @SneakyThrows
  @Override
  public Long createPaymentAccount() {
    PaymentAccount paymentAccount = paymentAccountRepository.save(new PaymentAccount());
    return paymentAccount.getId();
  }

  @Override
  public Double getPassengerMoney(Long paymentAccountId) {
    PaymentAccount paymentAccount = getById(paymentAccountId);
    return paymentAccount.getAmount();
  }
}
