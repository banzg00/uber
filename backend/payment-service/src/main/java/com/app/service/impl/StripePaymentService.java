package com.app.service.impl;

import static com.app.util.LoggerConstant.log;
import static com.app.util.StripePaymentConstants.CENTS;
import static com.app.util.StripePaymentConstants.CURRENCY;
import static com.app.util.StripePaymentConstants.FEATURE_REQUEST;

import com.app.dto.CreateStripePaymentDTO;
import com.app.dto.CreateStripePaymentResponseDTO;
import com.app.service.IStripePaymentService;
import com.stripe.model.PaymentIntent;
import com.stripe.param.PaymentIntentCreateParams;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

@Service
public class StripePaymentService implements IStripePaymentService {

  @SneakyThrows
  @Override
  public CreateStripePaymentResponseDTO createStripePaymentIntent(
      CreateStripePaymentDTO createStripePaymentDTO) {
    log.debug(String.format("Creating payment intent for user with email: %s",
        createStripePaymentDTO.getEmail()));
    PaymentIntentCreateParams createParams = new
        PaymentIntentCreateParams.Builder()
        .setCurrency(CURRENCY)
        .putMetadata(FEATURE_REQUEST, createStripePaymentDTO.getFeatureRequest())
        .setAmount(createStripePaymentDTO.getAmount() * CENTS)
        .build();

    PaymentIntent intent = PaymentIntent.create(createParams);
    CreateStripePaymentResponseDTO createStripePaymentResponseDTO =
        CreateStripePaymentResponseDTO.builder().clientSecret(intent.getClientSecret()).build();
    return createStripePaymentResponseDTO;
  }
}
