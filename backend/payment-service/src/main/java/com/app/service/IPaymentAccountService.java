package com.app.service;

import com.app.model.PaymentAccount;

public interface IPaymentAccountService {

  void updatePaymentAccountAmount(PaymentAccount paymentAccount, double amount);

  PaymentAccount getById(Long id);

  boolean checkPaymentAccountBalance(double price, String username);

  Long createPaymentAccount();

  Double getPassengerMoney(Long paymentAccountId);
}
