package com.app.service.impl;

import static com.app.util.LoggerConstant.log;

import com.app.client.IAMServiceClient;
import com.app.dto.PayInCreditsDTO;
import com.app.dto.PayOutCreditsDTO;
import com.app.dto.TransactionDTO;
import com.app.dto.TripPaymentDTO;
import com.app.enums.TransactionType;
import com.app.exceptions.CreditPayOutException;
import com.app.exceptions.IAMServiceException;
import com.app.exceptions.PaymentException;
import com.app.model.PaymentAccount;
import com.app.model.Transaction;
import com.app.repository.ITransactionRepository;
import com.app.service.IPaymentAccountService;
import com.app.service.ITransactionService;
import io.github.resilience4j.retry.annotation.Retry;
import java.time.LocalDateTime;
import javax.transaction.Transactional;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
public class TransactionService implements ITransactionService {

  private final ITransactionRepository transactionRepository;
  private final IPaymentAccountService paymentAccountService;
  @Autowired
  private final IAMServiceClient iAMServiceClient;

  @Override
  @Transactional
  @Retry(name = "${spring.application.name}", fallbackMethod = "chargeTripFallback")
  public Long chargeTrip(TripPaymentDTO tripPaymentDTO)
      throws PaymentException, IAMServiceException {
    return transferCredit(tripPaymentDTO);
  }

  public Long chargeTripFallback(TripPaymentDTO tripPaymentDTO, Exception e)
      throws PaymentException, IAMServiceException {
    if (e instanceof PaymentException) {
      throw new PaymentException("Passenger does not have enough credit.");
    }
    throw new IAMServiceException("There was an error occurred with IAM service,"
        + " so trip is not charged and new transaction is not created.");
  }

  @SneakyThrows
  private Long transferCredit(TripPaymentDTO tripPaymentDTO) {
    double amount =
        tripPaymentDTO.getTripStatus().equals("ACCEPTED") ? calculateTripPenalty(tripPaymentDTO)
            : tripPaymentDTO.getPrice();

    log.debug("Getting payment accounts");
    Long paymentAccountFromId = iAMServiceClient.getPaymentAccount(
        tripPaymentDTO.getPassengerUsername());
    PaymentAccount paymentAccountFrom = paymentAccountService.getById(paymentAccountFromId);

    Long paymentAccountToId = iAMServiceClient.getPaymentAccount(
        tripPaymentDTO.getDriverUsername());
    PaymentAccount paymentAccountTo = paymentAccountService.getById(paymentAccountToId);

    if (amount > paymentAccountFrom.getAmount()) {
      throw new PaymentException("Passenger does not have enough credit.");
    }

    paymentAccountService.updatePaymentAccountAmount(paymentAccountFrom,
        paymentAccountFrom.getAmount() - amount);
    paymentAccountService.updatePaymentAccountAmount(paymentAccountTo,
        paymentAccountTo.getAmount() + amount);

    log.debug(String.format("Creating transaction for trip with id: %d",
        tripPaymentDTO.getTripId()));
    Transaction newTransaction = Transaction.builder().date(LocalDateTime.now()).amount(amount)
        .transactionType(TransactionType.PAYMENT)
        .paymentAccountFrom(paymentAccountFrom).paymentAccountTo(paymentAccountTo).build();
    Transaction transaction = transactionRepository.save(newTransaction);
    return transaction.getId();
  }

  @Override
  @Transactional
  @Retry(name = "${spring.application.name}", fallbackMethod = "chargeTripPenaltyFallback")
  public Long chargeTripPenalty(TripPaymentDTO tripPaymentDTO) {
    return transferCredit(tripPaymentDTO);
  }

  public Long chargeTripPenaltyFallback(TripPaymentDTO tripPaymentDTO, Exception e)
      throws PaymentException, IAMServiceException {
    if (e instanceof PaymentException) {
      throw new PaymentException("Passenger does not have enough credit.");
    }
    throw new IAMServiceException("There was an error occurred with IAM service,"
        + " so trip penalty is not charged and new transaction is not created.");
  }

  @SneakyThrows
  private double calculateTripPenalty(TripPaymentDTO tripPaymentDTO) {
    log.debug("Calculating trip penalty.");
    double penaltyCoefficient = tripPaymentDTO.getPenaltyCoefficient();
    return tripPaymentDTO.getPrice() * penaltyCoefficient;
  }

  @Override
  @Transactional
  @Retry(name = "${spring.application.name}", fallbackMethod = "payInCreditsFallback")
  public TransactionDTO payInCredits(PayInCreditsDTO buyCreditsDTO) {
    log.debug(String.format("Getting user with username: %s", buyCreditsDTO.getUsername()));
    Long paymentAccountId = iAMServiceClient.getPaymentAccount(buyCreditsDTO.getUsername());
    PaymentAccount paymentAccount = paymentAccountService.getById(paymentAccountId);

    paymentAccountService.updatePaymentAccountAmount(paymentAccount,
        paymentAccount.getAmount() + buyCreditsDTO.getAmount());

    log.debug(String.format("Creating transaction for payment account with id: %d",
        paymentAccountId));
    Transaction transaction = Transaction.builder().date(LocalDateTime.now())
        .amount(buyCreditsDTO.getAmount()).transactionType(TransactionType.PAY_IN)
        .paymentAccountTo(paymentAccount).build();
    return transactionToDTO(transactionRepository.save(transaction));
  }

  @SneakyThrows
  public TransactionDTO payInCreditsFallback(PayInCreditsDTO buyCreditsDTO, Exception e) {
    throw new IAMServiceException("There was an error occurred with IAM service,"
        + " so credits are not paid in and new transaction is not created.");
  }

  @Override
  @Transactional
  @Retry(name = "${spring.application.name}", fallbackMethod = "payOutCreditsFallback")
  public Transaction payOutCredits(PayOutCreditsDTO payOutCreditsDTO)
      throws CreditPayOutException, IAMServiceException {
    log.debug(String.format("Getting user with id: %s", payOutCreditsDTO.getUsername()));
    Long paymentAccountId = iAMServiceClient.getPaymentAccount(payOutCreditsDTO.getUsername());
    PaymentAccount paymentAccount = paymentAccountService.getById(paymentAccountId);

    if (payOutCreditsDTO.getAmount() > paymentAccount.getAmount()) {
      throw CreditPayOutException.builder()
          .description("User does not have that amount of credits.").build();
    }

    paymentAccountService.updatePaymentAccountAmount(paymentAccount,
        paymentAccount.getAmount() - payOutCreditsDTO.getAmount());

    log.debug(String.format("Creating transaction for payment account with id: %d",
        paymentAccount.getId()));
    Transaction transaction = Transaction.builder().date(LocalDateTime.now())
        .amount(payOutCreditsDTO.getAmount()).transactionType(TransactionType.PAY_OUT)
        .paymentAccountFrom(paymentAccount).build();
    return transactionRepository.save(transaction);
  }

  public Transaction payOutCreditsFallback(PayOutCreditsDTO payOutCreditsDTO, Exception e)
      throws CreditPayOutException, IAMServiceException {
    if (e instanceof CreditPayOutException) {
      throw CreditPayOutException.builder()
          .description("User does not have that amount of credits.").build();
    }
    throw new IAMServiceException("There was an error occurred with IAM service,"
        + " so credits are not paid out and new transaction is not created.");
  }

  private TransactionDTO transactionToDTO(Transaction transaction) {
    return TransactionDTO.builder().balance(transaction.getPaymentAccountTo().getAmount()).build();
  }
}
