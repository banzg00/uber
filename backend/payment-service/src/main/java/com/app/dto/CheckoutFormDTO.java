package com.app.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder(toBuilder = true)
public class CheckoutFormDTO {

  @NotNull(message = "Amount cannot be null.")
  private double amount;

  @NotNull(message = "Feature request cannot be null.")
  private String featureRequest;

  @Email
  private String email;
}
