package com.app.dto;

import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder(toBuilder = true)
public class PayOutCreditsDTO {

  @NotNull(message = "Username cannot be null.")
  private String username;
  @NotNull(message = "Amount cannot be null.")
  private double amount;
}
