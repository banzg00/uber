package com.app.dto;

import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder(toBuilder = true)
public class CreateStripePaymentDTO {

  @NotNull(message = "Amount cannot be null.")
  private Long amount;

  @NotNull(message = "Feature request cannot be null.")
  private String featureRequest;

  @NotNull(message = "Email cannot be null.")
  private String email;
}
