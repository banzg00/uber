package com.app.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder(toBuilder = true)
public class TripPaymentDTO {

  private Long tripId;
  private String driverUsername;
  private String passengerUsername;
  private double price;
  private String tripStatus;
  private double penaltyCoefficient;
}
