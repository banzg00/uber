package com.app.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Getter
@Setter
@ConfigurationProperties("notion")
public class NotionConfig {

  private String stripeSecretKey;

  private String stripePublicKey;

}
