package com.app.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "iam-service", url = "${spring.feign.iam-service-url}")
public interface IAMServiceClient {

  @GetMapping("paymentAccount/{username}")
  Long getPaymentAccount(@PathVariable String username);
}
