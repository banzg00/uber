package com.app.exceptions;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class CreditPayOutException extends Exception {

  private String description;

  @Override
  public String getMessage() {
    return String.format("Cannot pay out credits: %s", description);
  }
}
