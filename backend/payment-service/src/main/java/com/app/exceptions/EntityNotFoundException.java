package com.app.exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EntityNotFoundException extends Exception {

  private String type;
  private Long id;

  public EntityNotFoundException(String entity) {
    this.type = entity;
    this.id = 0L;
  }

  @Override
  public String getMessage() {
    if (id == 0) {
      return String.format("%s not found.", type);
    } else {
      return String.format("%s with id=%d not found", type, id);
    }
  }

}
