package com.app.exceptions;

import static com.app.util.LoggerConstant.log;

import com.stripe.exception.StripeException;
import java.util.Date;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class GlobalExceptionHandler {

  @ExceptionHandler(EntityNotFoundException.class)
  public ResponseEntity<ErrorMessage> handleEntityNotFoundExceptionHandler(
      EntityNotFoundException exception, WebRequest request) {
    ErrorMessage errorMessage = new ErrorMessage(HttpStatus.NOT_FOUND.value(), new Date(),
        exception.getMessage(), request.getDescription(false));
    log.error(exception.getMessage());
    return new ResponseEntity<>(errorMessage, HttpStatus.NOT_FOUND);
  }

  @ExceptionHandler(MethodArgumentNotValidException.class)
  public ResponseEntity<ErrorMessage> handleMethodArgumentNotValidExceptionHandler(
      MethodArgumentNotValidException exception, WebRequest request) {
    String message = exception.getBindingResult().getFieldErrors().stream()
        .map(err -> err.getDefaultMessage()).findFirst().get().toString();
    ErrorMessage errorMessage = new ErrorMessage(HttpStatus.BAD_REQUEST.value(), new Date(),
        message, request.getDescription(false));
    log.error(message);
    return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(PaymentException.class)
  public ResponseEntity<ErrorMessage> handlePaymentExceptionHandler(PaymentException exception,
      WebRequest request) {
    ErrorMessage errorMessage = new ErrorMessage(HttpStatus.BAD_REQUEST.value(), new Date(),
        exception.getMessage(), request.getDescription(false));
    log.error(exception.getMessage());
    return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(StripeException.class)
  public ResponseEntity<ErrorMessage> handleStripeExceptionHandler(StripeException exception,
      WebRequest request) {
    ErrorMessage errorMessage = new ErrorMessage(HttpStatus.BAD_REQUEST.value(), new Date(),
        exception.getMessage(), request.getDescription(false));
    log.error(exception.getMessage());
    return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(CreditPayOutException.class)
  public ResponseEntity<ErrorMessage> handleCreditPayOutExceptionHandler(
      CreditPayOutException exception, WebRequest request) {
    ErrorMessage errorMessage = new ErrorMessage(HttpStatus.BAD_REQUEST.value(), new Date(),
        exception.getMessage(), request.getDescription(false));
    log.error(exception.getMessage());
    return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(IAMServiceException.class)
  public ResponseEntity<ErrorMessage> handleIAMServiceExceptionHandler(
      IAMServiceException exception, WebRequest request) {
    ErrorMessage errorMessage = new ErrorMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), new Date(),
        exception.getMessage(), request.getDescription(false));
    log.error(exception.getMessage());
    return new ResponseEntity<>(errorMessage, HttpStatus.INTERNAL_SERVER_ERROR);
  }
}
