package com.app.controller;

import static com.app.util.LoggerConstant.log;

import com.app.dto.PayInCreditsDTO;
import com.app.dto.PayOutCreditsDTO;
import com.app.dto.TransactionDTO;
import com.app.model.Transaction;
import com.app.service.ITransactionService;
import io.swagger.v3.oas.annotations.Operation;
import javax.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
@RequestMapping(value = "api/uber/transaction")
public class TransactionController {

  private final ITransactionService transactionService;

  @Operation(summary = "Pay in credits")
  @SneakyThrows
  @PostMapping(value = "/credit/payIn")
  public ResponseEntity<TransactionDTO> payInCredits(
      @Valid @RequestBody PayInCreditsDTO buyCreditsDTO) {
    TransactionDTO transaction = transactionService.payInCredits(buyCreditsDTO);
    log.info(String.format("Credit successfully paid in to payment account with username: %s",
        buyCreditsDTO.getUsername()));
    return new ResponseEntity<>(transaction, HttpStatus.OK);
  }

  @Operation(summary = "Pay out credits")
  @SneakyThrows
  @PostMapping(value = "/credit/payOut")
  public ResponseEntity<Transaction> payOutCredits(
      @Valid @RequestBody PayOutCreditsDTO payOutCreditsDTO) {
    Transaction transaction = transactionService.payOutCredits(payOutCreditsDTO);
    log.info(String.format("Credit successfully paid out from payment account with id: %d",
        transaction.getPaymentAccountFrom().getId()));
    return new ResponseEntity<>(transaction, HttpStatus.OK);
  }
}
