package com.app.controller;

import static com.app.util.LoggerConstant.log;

import com.app.config.NotionConfig;
import com.app.dto.CreateStripePaymentDTO;
import com.app.dto.CreateStripePaymentResponseDTO;
import com.app.service.IStripePaymentService;
import com.stripe.Stripe;
import io.swagger.v3.oas.annotations.Operation;
import javax.annotation.PostConstruct;
import javax.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
@RequestMapping(value = "api/uber/stripe")
public class StripePaymentController {

  private final IStripePaymentService stripePaymentService;

  @Autowired
  private NotionConfig notionConfig;

  @PostConstruct
  public void init() {
    Stripe.apiKey = notionConfig.getStripeSecretKey();
  }

  @Operation(summary = "Create stripe payment intent")
  @SneakyThrows
  @PostMapping("/createPaymentIntent")
  public ResponseEntity<CreateStripePaymentResponseDTO> createStripePaymentIntent(
      @RequestBody @Valid CreateStripePaymentDTO createStripePaymentDTOPayment) {
    CreateStripePaymentResponseDTO createStripePaymentResponseDTO =
        stripePaymentService.createStripePaymentIntent(createStripePaymentDTOPayment);
    log.info(String.format("Created stripe payment intent for user with email: %s",
        createStripePaymentDTOPayment.getEmail()));
    return new ResponseEntity<>(createStripePaymentResponseDTO, HttpStatus.OK);
  }
}
