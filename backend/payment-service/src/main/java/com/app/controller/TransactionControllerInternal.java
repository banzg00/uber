package com.app.controller;

import static com.app.util.LoggerConstant.log;

import com.app.dto.TripPaymentDTO;
import com.app.service.IPaymentAccountService;
import com.app.service.ITransactionService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
@RequestMapping(value = "internal")
public class TransactionControllerInternal {

  private final ITransactionService transactionService;
  private final IPaymentAccountService paymentAccountService;

  @SneakyThrows
  @Operation(summary = "Charge trip")
  @PostMapping(value = "/charge/trip")
  public Long chargeTrip(@RequestBody TripPaymentDTO tripPaymentDTO) {
    log.info(String.format("Charging trip with id: %d", tripPaymentDTO.getTripId()));
    return transactionService.chargeTrip(tripPaymentDTO);
  }

  @SneakyThrows
  @Operation(summary = "Charge trip penalty")
  @PostMapping(value = "/charge/penalty")
  public Long chargeTripPenalty(@RequestBody TripPaymentDTO tripPaymentDTO) {
    log.info(String.format("Charging trip penalty with id: %d", tripPaymentDTO.getTripId()));
    return transactionService.chargeTripPenalty(tripPaymentDTO);
  }

  @SneakyThrows
  @Operation(summary = "Check payment account balance")
  @GetMapping("/check/money/{price}/{username}")
  public boolean checkPaymentAccountBalance(@PathVariable double price,
      @PathVariable String username) {
    log.info(String.format("Checking payment account balance for user with username: %s",
        username));
    return paymentAccountService.checkPaymentAccountBalance(price, username);
  }

  @SneakyThrows
  @Operation(summary = "Create payment account")
  @PostMapping(value = "/createPaymentAccount")
  public Long createPaymentAccount() {
    log.info("Creating payment account");
    return paymentAccountService.createPaymentAccount();
  }

  @SneakyThrows
  @Operation(summary = "Get passenger money")
  @GetMapping(value = "get/money/{paymentAccountId}")
  public Double getPassengerMoney(@PathVariable Long paymentAccountId) {
    log.info(String.format("Getting credits for payment account: [%d]", paymentAccountId));
    return paymentAccountService.getPassengerMoney(paymentAccountId);
  }
  
}
