package com.app.controller;

import static com.app.util.StripePaymentConstants.AMOUNT_ATTRIBUTE;
import static com.app.util.StripePaymentConstants.CHECKOUT_FORM_ATTRIBUTE;
import static com.app.util.StripePaymentConstants.CHECKOUT_HTML;
import static com.app.util.StripePaymentConstants.EMAIL_ATTRIBUTE;
import static com.app.util.StripePaymentConstants.FEATURE_REQUEST;
import static com.app.util.StripePaymentConstants.INDEX_HTML;
import static com.app.util.StripePaymentConstants.STRIPE_PUBLIC_KEY_ATTRIBUTE;

import com.app.config.NotionConfig;
import com.app.dto.CheckoutFormDTO;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/uber/stripe/checkout")
public class StripeWebCheckoutController {

  @Autowired
  private NotionConfig notionConfig;

  @GetMapping
  public String home(Model model) {
    model.addAttribute(CHECKOUT_FORM_ATTRIBUTE, CheckoutFormDTO.builder().build());
    return INDEX_HTML;
  }

  @PostMapping
  public String checkout(@ModelAttribute @Valid CheckoutFormDTO checkoutForm,
      BindingResult bindingResult, Model model) {
    if (bindingResult.hasErrors()) {
      return INDEX_HTML;
    }

    model.addAttribute(STRIPE_PUBLIC_KEY_ATTRIBUTE, notionConfig.getStripePublicKey());
    model.addAttribute(AMOUNT_ATTRIBUTE, checkoutForm.getAmount());
    model.addAttribute(EMAIL_ATTRIBUTE, checkoutForm.getEmail());
    model.addAttribute(FEATURE_REQUEST, checkoutForm.getFeatureRequest());
    return CHECKOUT_HTML;
  }
}
