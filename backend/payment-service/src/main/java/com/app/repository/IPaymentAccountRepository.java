package com.app.repository;

import com.app.model.PaymentAccount;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IPaymentAccountRepository extends JpaRepository<PaymentAccount, Long> {

}
