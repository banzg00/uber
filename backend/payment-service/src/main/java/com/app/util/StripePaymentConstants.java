package com.app.util;

public class StripePaymentConstants {

  public final static String CURRENCY = "usd";
  public final static String FEATURE_REQUEST = "featureRequest";
  public final static Long CENTS = 100L;
  public final static String CHECKOUT_FORM_ATTRIBUTE = "checkoutForm";
  public final static String STRIPE_PUBLIC_KEY_ATTRIBUTE = "stripePublicKey";
  public final static String AMOUNT_ATTRIBUTE = "amount";
  public final static String EMAIL_ATTRIBUTE = "email";
  public final static String INDEX_HTML = "index";
  public final static String CHECKOUT_HTML = "checkout";
}
