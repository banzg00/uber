package com.app.enums;

public enum TransactionType {
  PAYMENT, PAY_IN, PAY_OUT
}
