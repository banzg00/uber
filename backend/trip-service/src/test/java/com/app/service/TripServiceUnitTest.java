package com.app.service;

import static com.app.constants.DriverConstants.DRIVER_USERNAME;
import static com.app.constants.TripConstants.COMPANY_ID;
import static com.app.constants.TripConstants.ETA_10;
import static com.app.constants.TripConstants.FROM;
import static com.app.constants.TripConstants.LAT;
import static com.app.constants.TripConstants.LNG;
import static com.app.constants.TripConstants.PASSENGER_USERNAME;
import static com.app.constants.TripConstants.PENALTY;
import static com.app.constants.TripConstants.PRICE_100;
import static com.app.constants.TripConstants.PRICE_ENOUGH;
import static com.app.constants.TripConstants.ROUTE_API_RESPONSE;
import static com.app.constants.TripConstants.TO;
import static com.app.constants.TripConstants.TOTAL_DISTANCE;
import static com.app.constants.TripConstants.TOTAL_TIME;
import static com.app.constants.TripConstants.TRIP_CANCELLATION_DESCRIPTION;
import static com.app.constants.TripConstants.TRIP_CANCELLATION_ID_1;
import static com.app.constants.TripConstants.TRIP_CANCELLATION_ID_2;
import static com.app.constants.TripConstants.TRIP_DRIVER_CANCELLATION_DESCRIPTION;
import static com.app.constants.TripConstants.TRIP_ID;
import static com.app.constants.TripConstants.TRIP_ID_1;
import static com.app.constants.TripConstants.TRIP_ID_2;
import static com.app.constants.TripConstants.TRIP_ID_3;
import static com.app.constants.TripConstants.TRIP_ID_7;
import static com.app.constants.TripConstants.TRIP_STATUS_1;
import static com.app.constants.TripConstants.TRIP_STATUS_2;
import static com.app.constants.TripConstants.TRIP_STATUS_3;
import static com.app.constants.TripConstants.TRIP_STATUS_4;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.app.dto.AcceptTripDTO;
import com.app.dto.CancelTripByDriverDTO;
import com.app.dto.MapPointDTO;
import com.app.dto.NotificationDTO;
import com.app.dto.RouteDTO;
import com.app.dto.TripDTO;
import com.app.enums.TripCancellationType;
import com.app.enums.TripStatus;
import com.app.exceptions.NoRoutesFoundException;
import com.app.exceptions.NotificationServiceException;
import com.app.exceptions.TripStatusException;
import com.app.model.Company;
import com.app.model.Trip;
import com.app.model.TripCancellation;
import com.app.repository.ITripRepository;
import com.app.service.client.IAMServiceClient;
import com.app.service.client.NotificationServiceClient;
import com.app.service.client.PaymentServiceClient;
import com.app.service.impl.TripService;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

@SpringBootTest
public class TripServiceUnitTest {

  Trip trip;
  RouteDTO route;
  Trip TRIP1;
  Trip TRIP2;
  Trip TRIP3;
  TripCancellation TRIP_CANCELLATION_1;
  TripCancellation TRIP_CANCELLATION_2;
  Trip TRIP7;
  Company company;
  @Mock
  private ITripRepository tripRepository;
  @Mock
  private NotificationServiceClient notificationServiceClient;
  @Mock
  private PaymentServiceClient paymentServiceClient;
  @Mock
  private RestTemplate restTemplate;
  @Mock
  private ICompanyService companyService;
  @Mock
  private ITripCancellationService tripCancellationService;
  @Mock
  private IAMServiceClient iamServiceClient;
  @InjectMocks
  private TripService tripService;

  @BeforeEach
  public void init() {
    MapPointDTO mp1 = MapPointDTO.builder().lat(LAT).lng(LNG).build();
    MapPointDTO mp2 = MapPointDTO.builder().lat(LAT).lng(LNG).build();
    List<MapPointDTO> mapPoints = new ArrayList<>();
    mapPoints.add(mp1);
    mapPoints.add(mp2);
    route = RouteDTO.builder().coordinates(mapPoints).totalTime(TOTAL_TIME)
        .passengerUsername(PASSENGER_USERNAME)
        .totalDistance(TOTAL_DISTANCE).price(PRICE_ENOUGH).build();
    trip = Trip.builder().id(TRIP_ID).tripStatus(TripStatus.PENDING)
        .passengerUsername(PASSENGER_USERNAME)
        .price(PRICE_ENOUGH)
        .build();
    trip.setId(TRIP_ID);
    TRIP1 = Trip.builder().id(TRIP_ID_1).tripStatus(TRIP_STATUS_1)
        .passengerUsername(PASSENGER_USERNAME)
        .driverUsername(DRIVER_USERNAME).build();
    TRIP2 = Trip.builder().id(TRIP_ID_2).tripStatus(TRIP_STATUS_2)
        .passengerUsername(PASSENGER_USERNAME)
        .driverUsername(DRIVER_USERNAME).build();
    TRIP3 = Trip.builder().id(TRIP_ID_3).tripStatus(TRIP_STATUS_3)
        .passengerUsername(PASSENGER_USERNAME)
        .driverUsername(DRIVER_USERNAME).price(PRICE_100).build();
    TRIP_CANCELLATION_1 = TripCancellation.builder().id(TRIP_CANCELLATION_ID_1)
        .description(TRIP_DRIVER_CANCELLATION_DESCRIPTION)
        .tripCancellationType(
            TripCancellationType.DRIVER_CANCELED_DRIVER_ISSUES).build();
    TRIP_CANCELLATION_2 = TripCancellation.builder().id(TRIP_CANCELLATION_ID_2)
        .description(TRIP_CANCELLATION_DESCRIPTION).tripCancellationType(
            TripCancellationType.PASSENGER_CANCELED).build();
    TRIP7 = Trip.builder().id(TRIP_ID_7).tripStatus(TripStatus.PENDING).price(PRICE_100)
        .passengerUsername(PASSENGER_USERNAME).build();
    company = Company.builder().penaltyCoefficient(PENALTY).build();
  }

  @Test
  @SneakyThrows
  public void testEndTripInvalidStatus() {
    when(tripRepository.save(any(Trip.class))).thenReturn(TRIP1);
    when(tripRepository.findById(TRIP_ID_1)).thenReturn(Optional.of(TRIP1));

    assertThrows(TripStatusException.class, () -> tripService.endTrip(TRIP_ID_1));
  }

  @Test
  @SneakyThrows
  public void testStartTripInvalidStatus() {
    when(tripRepository.save(any(Trip.class))).thenReturn(TRIP1);
    when(tripRepository.findById(TRIP_ID_1)).thenReturn(Optional.of(TRIP1));

    assertThrows(TripStatusException.class, () -> tripService.startTrip(TRIP_ID_1));
  }

  @Test
  @SneakyThrows
  public void testCancelTripByPassengerInvalidStatus() {
    when(tripRepository.save(any(Trip.class))).thenReturn(TRIP1);
    when(tripRepository.findById(TRIP_ID_1)).thenReturn(Optional.of(TRIP1));

    assertThrows(TripStatusException.class,
        () -> tripService.cancelTripByPassenger(TRIP_ID_1, TRIP_CANCELLATION_DESCRIPTION));
  }

  @Test
  @SneakyThrows
  public void testCancelTripByDriverInvalidStatus() {
    when(tripRepository.save(any(Trip.class))).thenReturn(TRIP1);
    when(tripRepository.findById(TRIP_ID_1)).thenReturn(Optional.of(TRIP1));

    CancelTripByDriverDTO cancelTripByDriverDTO = CancelTripByDriverDTO.builder().tripId(TRIP_ID_1)
        .cancellationDescription(TRIP_DRIVER_CANCELLATION_DESCRIPTION)
        .tripCancellationType(TripCancellationType.DRIVER_CANCELED_DRIVER_ISSUES).build();

    assertThrows(TripStatusException.class,
        () -> tripService.cancelTripByDriver(cancelTripByDriverDTO));
  }

  @Test
  public void testSendDriverArrivedMessage()
      throws TripStatusException, NotificationServiceException {
    when(tripRepository.findById(TRIP_ID_3)).thenReturn(Optional.of(TRIP3));

    tripService.sendDriverArrivedMessage(TRIP_ID_3);
    verify(notificationServiceClient, times(1)).sendPassengerNotification(any());
  }

  @Test
  public void testSendDriverArrivedMessage_invalidTripStatus() {
    when(tripRepository.findById(TRIP_ID_2)).thenReturn(Optional.of(TRIP2));

    assertThrows(TripStatusException.class,
        () -> tripService.sendDriverArrivedMessage(TRIP_ID_2));
  }

  @Test
  public void testAcceptTrip_alreadyAccepted() {
    when(tripRepository.save(any(Trip.class))).thenReturn(TRIP3);
    when(tripRepository.findById(TRIP_ID_3)).thenReturn(Optional.of(TRIP3));
    AcceptTripDTO dto = AcceptTripDTO.builder().tripId(TRIP_ID_3).eta(ETA_10)
        .driverUsername(DRIVER_USERNAME).build();

    assertThrows(TripStatusException.class, () -> tripService.acceptTrip(dto));
  }
}
