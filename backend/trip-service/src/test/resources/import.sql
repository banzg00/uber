insert into routes(trip_duration, distance) values (10, 10);

insert into addresses (latitude, longitude, name, route_id) values (45.264110, 19.830390, 'Bulevar oslobodjenja 1', 1);
insert into addresses (latitude, longitude, name, route_id) values (45.247850, 19.850720, 'Bulevar cara Lazara 1', 1);
insert into addresses (latitude, longitude, name, route_id) values (45.248900, 19.825250, 'Cara Dusana 1', 1);

insert into companies(name, tariff, penalty_coefficient, address_id) values ('Uber', 100, 0.2, 1);

insert into trips(start_date, end_date, trip_status, passenger_username, driver_username, price, route_id, version, created) values ('2022-08-22 08:09', '2022-08-22 08:19', 'ENDED', 'marko', 'petar', 500, 1, 1, '2022-08-22 08:09');
insert into trips(start_date, end_date, trip_status, passenger_username, driver_username, price, route_id, version, created) values ('2022-10-11 10:09', '2022-10-11 10:19', 'STARTED', 'marko', 'petar', 480, 1, 1, '2022-08-22 08:09');
insert into trips(start_date, end_date, trip_status, passenger_username, driver_username, price, route_id, version, created) values ('2022-12-05 14:20', '2022-12-05 14:30', 'ENDED', 'marko', 'petar', 499, 1, 1, '2022-08-22 08:09');
insert into trips(start_date, end_date, trip_status, passenger_username, driver_username, price, route_id, version, created) values ('2023-01-26 20:10', '2022-01-26 20:20', 'ENDED', 'marko', 'petar', 505, 1, 1, '2022-08-22 08:09');
insert into trips(trip_status, passenger_username, driver_username, price, route_id, version, created) values ('ACCEPTED', 'marko', 'petar', 390, 1, 1, '2022-08-22 08:09');
insert into trips(trip_status, passenger_username, price, route_id, version, created) values ('PENDING', 'marko', 385, 1, 1, '2022-08-22 08:09');
insert into trips(start_date, end_date, trip_status, passenger_username, driver_username, price, route_id, version, created) values ('2022-10-11 10:09', '2022-10-11 10:19', 'STARTED', 'marko', 'petar', 480, 1, 1, '2022-08-22 08:09');
insert into trips(trip_status, passenger_username, driver_username, price, route_id, version, created) values ('ACCEPTED', 'marko', 'petar', 350, 1, 1, '2022-08-22 08:09');
insert into trips(trip_status, passenger_username, price, route_id, version, created) values ('PENDING', 'marko', 420, 1, 1, '2022-08-22 08:09');
insert into trips(trip_status, passenger_username, price, route_id, version, created) values ('PENDING', 'marko', 460, 1, 1, '2022-08-22 08:09');

insert into rates(driver_username, passenger_username, trip_id, rate_number) values ('petar', 'marko', 3, 5);
insert into rates(driver_username, passenger_username, trip_id, rate_number) values ('petar', 'marko', 4, 4);