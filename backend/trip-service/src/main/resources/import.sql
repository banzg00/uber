insert into routes(trip_duration, distance) values (10, 10);

insert into addresses (latitude, longitude, name, route_id) values (45.264110, 19.830390, 'Arse Teodorovica 2, Rotkvarija, Centar, Novi Sad, Srbija', 1);
insert into addresses (latitude, longitude, name, route_id) values (45.247850, 19.850720, 'Bulevar cara Lazara 1', 1);
insert into addresses (latitude, longitude, name, route_id) values (45.248900, 19.825250, 'Cara Dusana 1', 1);

insert into companies(name, tariff, penalty_coefficient, address_id) values ('Uber', 0.15, 0.2, 1);

-- insert into trips(trip_status, passenger_username, price, route_id, version) values ('PENDING', 'marko', 385, 1, 1);
-- insert into trips(trip_status, passenger_username, price, route_id, version) values ('STARTED', 'marko', 385, 1, 1);
-- insert into trips(trip_status, passenger_username, driver_username, price, route_id, version) values ('ACCEPTED', 'marko', 'petar', 385, 1, 1);