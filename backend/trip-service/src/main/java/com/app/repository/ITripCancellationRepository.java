package com.app.repository;

import com.app.model.TripCancellation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ITripCancellationRepository extends JpaRepository<TripCancellation, Long> {

}
