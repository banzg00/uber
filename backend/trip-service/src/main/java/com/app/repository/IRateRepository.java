package com.app.repository;

import com.app.model.Rate;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IRateRepository extends JpaRepository<Rate, Long> {

  Rate findByTripId(Long id);

  List<Rate> findAllByDriverUsername(String username);
}
