package com.app.repository;

import com.app.model.Trip;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ITripRepository extends JpaRepository<Trip, Long> {

  List<Trip> findAllByPassengerUsername(String username);

}
