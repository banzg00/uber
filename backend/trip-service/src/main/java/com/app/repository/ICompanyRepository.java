package com.app.repository;

import com.app.model.Company;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.NonNull;

public interface ICompanyRepository extends JpaRepository<Company, Long> {

  Optional<Company> findById(@NonNull Long id);
}
