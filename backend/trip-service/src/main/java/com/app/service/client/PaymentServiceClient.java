package com.app.service.client;

import com.app.dto.TripPaymentDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "payment-service", url = "${spring.feign.payment-service-url}")
public interface PaymentServiceClient {

  @PostMapping("charge/trip")
  void chargeTrip(@RequestBody TripPaymentDTO trip);

  @PostMapping("charge/penalty")
  void chargeTripPenalty(@RequestBody TripPaymentDTO trip);

  @GetMapping("check/money/{price}/{username}")
  Boolean checkEnoughMoney(@PathVariable double price, @PathVariable String username);
}
