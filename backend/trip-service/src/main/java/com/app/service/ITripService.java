package com.app.service;

import com.app.dto.AcceptTripDTO;
import com.app.dto.AddressDTO;
import com.app.dto.CancelTripByDriverDTO;
import com.app.dto.RouteDTO;
import com.app.dto.TripDTO;
import com.app.exceptions.IAMServiceException;
import com.app.exceptions.NotificationServiceException;
import com.app.exceptions.PaymentServiceException;
import com.app.exceptions.TripAlreadyAcceptedException;
import com.app.exceptions.TripStatusException;
import com.app.exceptions.UserNotFoundException;
import com.app.model.Trip;
import java.util.List;
import org.springframework.orm.ObjectOptimisticLockingFailureException;

public interface ITripService {

  Trip getById(Long id);

  TripDTO acceptTrip(AcceptTripDTO dto)
      throws TripStatusException, ObjectOptimisticLockingFailureException,
      NotificationServiceException, TripAlreadyAcceptedException, UserNotFoundException, IAMServiceException;

  TripDTO endTrip(Long id) throws TripStatusException, PaymentServiceException;

  TripDTO startTrip(Long id);

  TripDTO cancelTripByPassenger(Long id, String description)
      throws TripStatusException, PaymentServiceException, NotificationServiceException;

  TripDTO cancelTripByDriver(CancelTripByDriverDTO cancelTripByDriverDTO)
      throws TripStatusException, PaymentServiceException, NotificationServiceException;

  TripDTO createTrip(RouteDTO route);

  List<RouteDTO> getRoutes(String from, String to);

  void sendDriverArrivedMessage(Long tripId)
      throws TripStatusException, NotificationServiceException;

  boolean checkEnoughMoney(double price, String username);

  List<AddressDTO> getAddresses(String address);

  List<TripDTO> getActiveTrips(String username);
}
