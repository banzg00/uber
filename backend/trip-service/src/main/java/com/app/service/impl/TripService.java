package com.app.service.impl;

import static com.app.util.LoggerConstant.log;
import static com.app.util.TripServiceConstants.*;

import com.app.dto.AcceptTripDTO;
import com.app.dto.AddressDTO;
import com.app.dto.CancelTripByDriverDTO;
import com.app.dto.MapPointDTO;
import com.app.dto.NotificationDTO;
import com.app.dto.RouteDTO;
import com.app.dto.TripDTO;
import com.app.dto.TripPaymentDTO;
import com.app.enums.TripCancellationType;
import com.app.enums.TripStatus;
import com.app.exceptions.EntityNotFoundException;
import com.app.exceptions.IAMServiceException;
import com.app.exceptions.NotificationServiceException;
import com.app.exceptions.PaymentServiceException;
import com.app.exceptions.TripAlreadyAcceptedException;
import com.app.exceptions.TripStatusException;
import com.app.exceptions.UserNotFoundException;
import com.app.model.Address;
import com.app.model.Company;
import com.app.model.Route;
import com.app.model.Trip;
import com.app.model.TripCancellation;
import com.app.repository.IAddressRepository;
import com.app.repository.IRouteRepository;
import com.app.repository.ITripRepository;
import com.app.service.ICompanyService;
import com.app.service.ITripCancellationService;
import com.app.service.ITripService;
import com.app.service.client.IAMServiceClient;
import com.app.service.client.NotificationServiceClient;
import com.app.service.client.PaymentServiceClient;
import com.app.util.Messages;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import feign.FeignException.InternalServerError;
import feign.RetryableException;
import io.github.resilience4j.retry.annotation.Retry;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.apache.commons.httpclient.util.URIUtil;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

@Service
@AllArgsConstructor
public class TripService implements ITripService {

  private final ITripRepository tripRepository;
  private final NotificationServiceClient notificationServiceClient;
  private final ICompanyService companyService;
  private final PaymentServiceClient paymentServiceClient;
  private final ITripCancellationService tripCancellationService;
  private final IAMServiceClient iamServiceClient;
  private final IRouteRepository routeRepository;
  private final IAddressRepository addressRepository;

  @Override
  @SneakyThrows
  public Trip getById(Long id) {
    Optional<Trip> trip = tripRepository.findById(id);
    if (trip.isPresent()) {
      return trip.get();
    } else {
      throw new EntityNotFoundException("Trip", id);
    }
  }

  @Override
  @Transactional
  @Retry(name = "${spring.application.name}", fallbackMethod = "createTripFallback")
  public TripDTO createTrip(RouteDTO route) {
    Trip trip = Trip.builder()
        .tripStatus(TripStatus.PENDING)
        .price(route.getPrice())
        .passengerUsername(route.getPassengerUsername())
        .route(this.createRoute(route))
        .created(LocalDateTime.now())
        .build();
    trip = tripRepository.save(trip);
    notificationServiceClient.sendDriverNotification(
        createDriverNotificationDTO(trip, Messages.NEW_TRIP_DRIVER_MESSAGE));
    log.debug(String.format("New trip with ID: [%d] is created.", trip.getId()));
    return convertTripToDTO(trip);
  }

  @SneakyThrows
  public TripDTO createTripFallback(RouteDTO route, Exception e) {
    throw new NotificationServiceException("There was an error occurred with notification service,"
        + " so notification is not sent and new trip is not created.");
  }

  @Override
  @SneakyThrows
  public List<AddressDTO> getAddresses(String address) {
    RestTemplate restTemplate = new RestTemplate();
    ObjectMapper objectMapper = new ObjectMapper();

    ResponseEntity<String> responseEntity = restTemplate.exchange(
        String.format(ADDRESS_API_URL, address),
        HttpMethod.GET, null, String.class);

    if (responseEntity.getStatusCode() == HttpStatus.OK) {
      List<AddressDTO> addresses = objectMapper.readValue(responseEntity.getBody(),
          new TypeReference<List<AddressDTO>>() {
          });
      return filterAddressNames(addresses);
    }
    return new ArrayList<>();
  }

  private List<AddressDTO> filterAddressNames(List<AddressDTO> addresses) {
    return addresses.stream()
        .map(address -> {
          String name = "";
          if (address.getAddress().getRoad() != null && !address.getAddress().getRoad()
              .equals("")) {
            name += address.getAddress().getRoad() + ", ";
          }
          if (address.getAddress().getHouse_number() != null && !address.getAddress()
              .getHouse_number().equals("")) {
            name += address.getAddress().getHouse_number() + ", ";
          }
          if (address.getAddress().getQuarter() != null && !address.getAddress().getQuarter()
              .equals("")) {
            name += address.getAddress().getQuarter() + ", ";
          }
          if (address.getAddress().getCity_district() != null && !address.getAddress()
              .getCity_district().equals("")) {
            name += address.getAddress().getCity_district() + ", ";
          }
          if (address.getAddress().getState() != null && !address.getAddress().getState()
              .equals("")) {
            name += address.getAddress().getState() + ", ";
          }
          if (address.getAddress().getCountry() != null && !address.getAddress().getCountry()
              .equals("")) {
            name += address.getAddress().getCountry() + ", ";
          }
          address.setDisplay_name(name.substring(0, name.length() - 2));
          return address;
        }).collect(Collectors.toList());
  }

  @Override
  @SneakyThrows
  public List<RouteDTO> getRoutes(String from, String to) {
    String url = String.format(ROUTE_API_URL, from, to);
    String realURL = URIUtil.encodeQuery(url);
    RestTemplate restTemplate = new RestTemplate();
    ResponseEntity<String> responseEntity = restTemplate.exchange(realURL, HttpMethod.GET, null,
        String.class);

    JsonParser parser = new JsonParser();
    JsonElement element = parser.parse(Objects.requireNonNull(responseEntity.getBody()));

    String code = element.getAsJsonObject().get("code").getAsString();
    if (!code.equalsIgnoreCase("OK")) {
      log.debug(String.format("No routes found for source: [%s] and destination: [%s].", from, to));
      throw new RuntimeException();
    }

    JsonArray routesElement = element.getAsJsonObject().get("routes").getAsJsonArray();
    return getRoutes(routesElement);
  }

  @SneakyThrows
  private List<RouteDTO> getRoutes(JsonArray routesElement) {
    List<RouteDTO> routes = new ArrayList<>();
    for (var route : routesElement) {
      List<MapPointDTO> coordinates = new ArrayList<>();
      JsonArray steps = route.getAsJsonObject().get("legs").getAsJsonArray().get(0)
          .getAsJsonObject().get("steps").getAsJsonArray();
      double time = route.getAsJsonObject().get("duration").getAsDouble();
      double distance = route.getAsJsonObject().get("distance").getAsDouble();
      for (var step : steps) {
        JsonArray intersections = step.getAsJsonObject().get("intersections").getAsJsonArray();
        for (var inter : intersections) {
          JsonArray location = inter.getAsJsonObject().get("location").getAsJsonArray();
          coordinates.add(MapPointDTO.builder()
              .lng(location.get(0).getAsDouble())
              .lat(location.get(1).getAsDouble())
              .name(step.getAsJsonObject().get("name").getAsString())
              .build());
        }
      }
      routes.add(RouteDTO.builder().coordinates(coordinates).totalDistance(distance / 1000).totalTime(time)
          .price(calculatePrice(distance)).build());
    }
    return routes;
  }

  @SneakyThrows
  private double calculatePrice(double distance) {
    Company company = companyService.getById(1L);
    return distance * company.getTariff();
  }

  private Route createRoute(RouteDTO dto) {
    Route route = Route.builder()
            .distance(dto.getTotalDistance())
            .tripDuration(dto.getTotalTime())
            .build();
    Route routeWithID = routeRepository.save(route);
    List<Address> addresses = new ArrayList<>();
    dto.getCoordinates().forEach((c) -> {
      Address a = Address.builder()
              .route(routeWithID)
              .latitude(c.getLat())
              .longitude(c.getLng())
              .name(c.getName())
              .build();
      addresses.add(addressRepository.save(a));
    });
    routeWithID.setAddresses(addresses);
    return routeWithID;
  }

  @Override
  @Transactional
  @Retry(name = "${spring.application.name}", fallbackMethod = "endTripFallback")
  public TripDTO endTrip(Long id) throws TripStatusException, PaymentServiceException {
    Trip trip = getById(id);
    if (!trip.getTripStatus().equals(TripStatus.STARTED)) {
      throw new TripStatusException(trip.getTripStatus(), TripStatus.STARTED);
    }

    log.debug(String.format("Updating trip with id: %d", id));
    trip.setTripStatus(TripStatus.ENDED);
    trip.setEndDate(LocalDateTime.now());
    tripRepository.save(trip);

    log.debug(String.format("Starting trip payment with id: %d", id));
    paymentServiceClient.chargeTrip(convertTripToPaymentDTO(trip));
    return convertTripToDTO(trip);
  }

  public TripDTO endTripFallback(Long id, Exception e)
      throws TripStatusException, PaymentServiceException {
    if (e instanceof TripStatusException) {
      Trip trip = getById(id);
      throw new TripStatusException(trip.getTripStatus(), TripStatus.STARTED);
    }
    throw new PaymentServiceException(String.format("There was an error occurred with payment"
        + " service and payment process for trip: [%d] is canceled.", id));
  }

  @Override
  @SneakyThrows
  public TripDTO startTrip(Long id) {
    Trip trip = getById(id);
    if (!trip.getTripStatus().equals(TripStatus.ACCEPTED)) {
      throw new TripStatusException(trip.getTripStatus(), TripStatus.ACCEPTED);
    }
    log.debug(String.format("Updating trip with id: %d", id));
    trip.setTripStatus(TripStatus.STARTED);
    trip.setStartDate(LocalDateTime.now());
    tripRepository.save(trip);
    return convertTripToDTO(trip);
  }

  @Override
  @Transactional
  @Retry(name = "${spring.application.name}", fallbackMethod = "cancelTripByPassengerFallback")
  public TripDTO cancelTripByPassenger(Long id, String description)
      throws TripStatusException, PaymentServiceException, NotificationServiceException {
    Trip trip = getById(id);
    if (!(trip.getTripStatus().equals(TripStatus.ACCEPTED) || trip.getTripStatus()
        .equals(TripStatus.PENDING))) {
      throw new TripStatusException(trip.getTripStatus());
    }

    if (trip.getTripStatus().equals(TripStatus.ACCEPTED)) {
      log.debug(String.format("Starting trip penalty payment with id: %d", id));
      try {
        paymentServiceClient.chargeTripPenalty(convertTripToPaymentDTO(trip));
      } catch (InternalServerError | RetryableException e) {
        throw new PaymentServiceException();
      }
    }
    try {
      notificationServiceClient.sendDriverNotification(createDriverNotificationDTO(trip,
          String.format(Messages.CANCEL_TRIP_BY_PASSENGER, trip.getId())));
    } catch (InternalServerError | RetryableException e) {
      throw new NotificationServiceException();
    }
    return saveCanceledTrip(trip, description, TripCancellationType.PASSENGER_CANCELED);
  }

  public TripDTO cancelTripByPassengerFallback(Long id, String description, Exception e)
      throws TripStatusException, PaymentServiceException, NotificationServiceException {
    if (e instanceof TripStatusException) {
      Trip trip = getById(id);
      throw new TripStatusException(trip.getTripStatus());
    } else if (e instanceof PaymentServiceException) {
      throw new PaymentServiceException(
          "There was an error occurred with payment service so trip cannot be canceled.");
    }
    throw new NotificationServiceException(
        "There was an error occurred with notification service so trip cannot be canceled.");
  }

  @Override
  @Transactional
  @Retry(name = "${spring.application.name}", fallbackMethod = "cancelTripByDriverFallback")
  public TripDTO cancelTripByDriver(CancelTripByDriverDTO dto)
          throws TripStatusException, PaymentServiceException, NotificationServiceException {
    Trip trip = getById(dto.getTripId());
    TripStatus tripStatus = trip.getTripStatus();

    validateDriverCancellationTripStatus(tripStatus);
    if (dto.getTripCancellationType() == TripCancellationType.DRIVER_CANCELED_PASSENGER_ISSUES) {
      handleDriverCanceledTrip(trip);
    }
    if (tripStatus == TripStatus.ACCEPTED) {
      sendDriverCancellationPassengerNotification(trip);
    }

    return saveCanceledTrip(trip, dto.getCancellationDescription(), dto.getTripCancellationType());
  }

  private void validateDriverCancellationTripStatus(TripStatus tripStatus) throws TripStatusException {
    if (!(tripStatus == TripStatus.ACCEPTED || tripStatus == TripStatus.STARTED)) {
      throw new TripStatusException(tripStatus);
    }
  }

  private void handleDriverCanceledTrip(Trip trip)
          throws PaymentServiceException {
    if (trip.getTripStatus() == TripStatus.STARTED) {
      log.debug(String.format("Starting trip penalty payment with trip id: %d",
              trip.getId()));
      try {
        paymentServiceClient.chargeTripPenalty(convertTripToPaymentDTO(trip));
      } catch (InternalServerError | RetryableException e) {
        throw new PaymentServiceException();
      }
    }
  }

  private void sendDriverCancellationPassengerNotification(Trip trip)
          throws NotificationServiceException {
    try {
      notificationServiceClient.sendPassengerNotification(createPassengerNotificationDTO(trip,
              Messages.CANCEL_TRIP_BY_DRIVER));
    } catch (InternalServerError | RetryableException e) {
      throw new NotificationServiceException();
    }
  }


  public TripDTO cancelTripByDriverFallback(CancelTripByDriverDTO dto, Exception e)
      throws TripStatusException, PaymentServiceException, NotificationServiceException {
    if (e instanceof TripStatusException) {
      Trip trip = getById(dto.getTripId());
      throw new TripStatusException(trip.getTripStatus());
    } else if (e instanceof PaymentServiceException) {
      throw new PaymentServiceException(
          "There was an error occurred with payment service so trip cannot be canceled.");
    }
    throw new NotificationServiceException(
        "There was an error occurred with notification service so trip cannot be canceled.");
  }

  private TripDTO saveCanceledTrip(Trip trip, String cancellationDescription,
      TripCancellationType tripCancellationType) {
    log.debug(String.format("Saving trip cancellation for trip with id: %d", trip.getId()));
    TripCancellation tripCancellation = tripCancellationService.save(
        TripCancellation.builder().description(cancellationDescription)
            .tripCancellationType(tripCancellationType).build());

    log.debug(String.format("Updating trip with id: %d", trip.getId()));
    trip.setTripStatus(TripStatus.CANCELED);
    trip.setTripCancellation(tripCancellation);
    tripRepository.save(trip);
    return convertTripToDTO(trip);
  }

  @Override
  @Transactional
  @Retry(name = "${spring.application.name}", fallbackMethod = "sendDriverArrivedMessageFallback")
  public void sendDriverArrivedMessage(Long tripId)
      throws TripStatusException, NotificationServiceException {
    Trip trip = getById(tripId);
    if (trip.getTripStatus() != TripStatus.ACCEPTED) {
      throw new TripStatusException(trip.getTripStatus(), TripStatus.ACCEPTED);
    }
    notificationServiceClient.sendPassengerNotification(
        createPassengerNotificationDTO(trip, Messages.DRIVER_ARRIVED));
    log.debug(
        String.format(
            "Sending notification about driver arrival to passenger with username: [%s]...",
            trip.getPassengerUsername()));
  }

  public void sendDriverArrivedMessageFallback(Long tripId, Exception e)
      throws TripStatusException, NotificationServiceException {
    Trip trip = getById(tripId);
    if (e instanceof TripStatusException) {
      throw new TripStatusException(trip.getTripStatus(), TripStatus.ACCEPTED);
    }
    throw new NotificationServiceException(
        String.format("There was an error occurred with notification service and"
                + " notification about driver arrival is not sent to user with username: [%s].",
            trip.getPassengerUsername()));
  }

  @Override
  @Retry(name = "${spring.application.name}", fallbackMethod = "checkEnoughMoneyFallback")
  public boolean checkEnoughMoney(double price, String username) {
    return paymentServiceClient.checkEnoughMoney(price, username);
  }

  @SneakyThrows
  public boolean checkEnoughMoneyFallback(double price, String username, Exception e) {
    throw new PaymentServiceException(String.format("There was an error occurred with payment"
        + " service and money could not be checked for user with username: [%s].", username));
  }

  @Override
  @Transactional(rollbackFor = {Exception.class})
  @Retry(name = "${spring.application.name}", fallbackMethod = "acceptTripFallback")
  public TripDTO acceptTrip(AcceptTripDTO dto)
      throws TripStatusException, ObjectOptimisticLockingFailureException, IAMServiceException,
      NotificationServiceException, TripAlreadyAcceptedException, UserNotFoundException {
    Trip trip = getById(dto.getTripId());
    if (trip.getTripStatus() != TripStatus.PENDING) {
      throw new TripStatusException(trip.getTripStatus(), TripStatus.PENDING);
    }
    try {
      if (!iamServiceClient.driverExist(dto.getDriverUsername())) {
        throw new UserNotFoundException(dto.getDriverUsername());
      }
    } catch (InternalServerError | RetryableException e) {
      throw new IAMServiceException();
    }
    trip.setTripStatus(TripStatus.ACCEPTED);
    trip.setDriverUsername(dto.getDriverUsername());
    tripRepository.save(trip);
    try {
      notificationServiceClient.sendPassengerNotification(createPassengerNotificationDTO(trip,
          String.format(Messages.TRIP_ACCEPTED_PASSENGER_MESSAGE, dto.getEta())));
    } catch (InternalServerError | RetryableException e) {
      throw new NotificationServiceException();
    }
    log.debug(
        String.format("Status of selected trip with id: [%d] successfully changed to ACCEPTED.",
            trip.getId()));
    return convertTripToDTO(trip);
  }

  public TripDTO acceptTripFallback(AcceptTripDTO dto, Exception e)
      throws TripStatusException, ObjectOptimisticLockingFailureException, IAMServiceException,
      NotificationServiceException, TripAlreadyAcceptedException, UserNotFoundException {
    System.out.println(e.getClass());
    if (e instanceof TripStatusException) {
      Trip trip = getById(dto.getTripId());
      throw new TripStatusException(trip.getTripStatus(), TripStatus.PENDING);
    } else if (e instanceof ObjectOptimisticLockingFailureException) {
      throw new TripAlreadyAcceptedException();
    } else if (e instanceof UserNotFoundException) {
      throw new UserNotFoundException(dto.getDriverUsername());
    } else if (e instanceof IAMServiceException) {
      throw new IAMServiceException("There was an error with iam service...");
    }
    throw new NotificationServiceException("There was an error occurred with notification service"
        + " and notification is not sent to passenger.");
  }

  @Override
  public List<TripDTO> getActiveTrips(String username) {
    List<TripDTO> dto = new ArrayList<>();
    List<Trip> trips = tripRepository.findAllByPassengerUsername(username).stream()
            .filter(t -> t.getTripStatus() == TripStatus.PENDING ||
                    t.getTripStatus() == TripStatus.ACCEPTED ||
                    t.getTripStatus() == TripStatus.STARTED)
            .collect(Collectors.toList());
    trips.forEach(t -> dto.add(convertTripToDTO(t)));
    log.debug(String.format("Got active trips for user with username: %s.", username));
    return dto;
  }

  @Scheduled(fixedRate = 1, timeUnit = TimeUnit.MINUTES)
  public void checkUnacceptedTrips() throws NotificationServiceException {
    log.debug("Checking for PENDING trips...");
    List<Trip> trips = tripRepository.findAll().stream()
            .filter(t -> t.getTripStatus() == TripStatus.PENDING)
            .collect(Collectors.toList());
    for (var trip : trips) {
      long passedPeriod = Duration.between(trip.getCreated(), LocalDateTime.now()).toMinutes();
      if (passedPeriod >= NO_DRIVER_ACCEPTED_PERIOD) {
        trip.setTripStatus(TripStatus.NOT_ACCEPTED);
        tripRepository.save(trip);
        try {
          notificationServiceClient.sendPassengerNotification(
                  createPassengerNotificationDTO(trip, Messages.NO_DRIVER_ACCEPTED));
        } catch (InternalServerError | RetryableException e) {
          throw new NotificationServiceException();
        }
      }
    }
  }

  private TripDTO convertTripToDTO(Trip trip) {
    List<Address> addresses = trip.getRoute().getAddresses();
    return TripDTO.builder().id(trip.getId()).startDate(trip.getStartDate())
        .endDate(trip.getEndDate())
        .routeId(trip.getRoute() == null ? null : trip.getRoute().getId())
        .tripStatus(trip.getTripStatus())
        .passengerUsername(trip.getPassengerUsername() == null ? null : trip.getPassengerUsername())
        .driverUsername(trip.getDriverUsername() == null ? null : trip.getDriverUsername())
        .price(trip.getPrice())
        .tripCancellationId(
            trip.getTripCancellation() == null ? null : trip.getTripCancellation().getId())
        .distance(trip.getRoute().getDistance())
        .source(addresses.get(0).getName())
        .destination(addresses.get(addresses.size() - 1).getName()).build();
  }

  private TripPaymentDTO convertTripToPaymentDTO(Trip trip) {
    Company company = companyService.getById(1L);
    return TripPaymentDTO.builder().tripId(trip.getId()).tripStatus(trip.getTripStatus().toString())
        .driverUsername(trip.getDriverUsername())
        .passengerUsername(trip.getPassengerUsername())
        .price(trip.getPrice()).penaltyCoefficient(company.getPenaltyCoefficient()).build();
  }

  private NotificationDTO createDriverNotificationDTO(Trip trip, String message) {
    return NotificationDTO.builder().tripId(trip.getId())
        .username(trip.getDriverUsername())
        .content(message).build();
  }

  private NotificationDTO createPassengerNotificationDTO(Trip trip, String message) {
    return NotificationDTO.builder().tripId(trip.getId())
        .username(trip.getPassengerUsername())
        .content(message).build();
  }

}
