package com.app.service.impl;

import com.app.exceptions.EntityNotFoundException;
import com.app.model.Company;
import com.app.repository.ICompanyRepository;
import com.app.service.ICompanyService;
import java.util.Optional;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CompanyService implements ICompanyService {

  private final ICompanyRepository companyRepository;

  @Override
  @SneakyThrows
  public Company getById(Long id) {
    Optional<Company> company = companyRepository.findById(1L);
    if (company.isEmpty()) {
      throw new EntityNotFoundException("company", 1L);
    }
    return company.get();
  }

  @Override
  @SneakyThrows
  public Long getCompanyId() {
    return getById(1L).getId();
  }
}
