package com.app.service.impl;

import com.app.model.TripCancellation;
import com.app.repository.ITripCancellationRepository;
import com.app.service.ITripCancellationService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class TripCancellationService implements ITripCancellationService {

  private final ITripCancellationRepository tripCancellationRepository;

  @Override
  public TripCancellation save(TripCancellation tripCancellation) {
    return tripCancellationRepository.save(tripCancellation);
  }
}
