package com.app.service;

import com.app.model.TripCancellation;

public interface ITripCancellationService {

  TripCancellation save(TripCancellation tripCancellation);

}
