package com.app.service.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "iam-service", url = "${spring.feign.iam-service-url}")
public interface IAMServiceClient {

  @GetMapping("auth/check/driver/{username}")
  boolean driverExist(@PathVariable String username);

}
