package com.app.service;

import com.app.model.Company;

public interface ICompanyService {

  Company getById(Long id);
  Long getCompanyId();
}
