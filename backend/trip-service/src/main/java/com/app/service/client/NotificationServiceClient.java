package com.app.service.client;

import com.app.dto.NotificationDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "notification-service", url = "${spring.feign.notification-service-url}")
public interface NotificationServiceClient {

  @PostMapping("send/driver")
  void sendDriverNotification(@RequestBody NotificationDTO notification);

  @PostMapping("send/passenger")
  void sendPassengerNotification(@RequestBody NotificationDTO notification);

}
