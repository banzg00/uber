package com.app.dto;

import com.app.enums.TripCancellationType;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder(toBuilder = true)
public class CancelTripByDriverDTO {

  @NotNull(message = "Trip cannot be null.")
  private Long tripId;

  @NotNull(message = "Trip cancellation description cannot be null")
  private String cancellationDescription;

  @NotNull(message = "Trip cancellation type cannot be null")
  private TripCancellationType tripCancellationType;
}
