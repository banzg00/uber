package com.app.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class NotificationDTO {

  @NotNull(message = "Trip ID cannot be null")
  private Long tripId;

  private String username;

  @NotEmpty(message = "Content cannot be empty")
  private String content;

}
