package com.app.dto;

import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder(toBuilder = true)
public class CancelTripByPassengerDTO {

  @NotNull(message = "Trip cannot be null.")
  private Long tripId;

  @NotNull(message = "Trip cancellation description cannot be null.")
  private String cancellationDescription;
}
