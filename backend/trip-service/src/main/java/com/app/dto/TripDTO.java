package com.app.dto;

import com.app.enums.TripStatus;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class TripDTO {

  private Long id;

  private LocalDateTime startDate;

  private LocalDateTime endDate;

  private Long routeId;

  private TripStatus tripStatus;

  private String passengerUsername;

  private String driverUsername;

  private double price;

  private Long tripCancellationId;

  private double distance;

  private String source;

  private String destination;
}
