package com.app.dto;

import java.util.List;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
public class RouteDTO {

  @NotNull(message = "Coordinates cannot be null.")
  private List<MapPointDTO> coordinates;

  @NotNull(message = "Total time cannot be null.")
  private Double totalTime;

  @NotNull(message = "Total distance cannot be null.")
  private Double totalDistance;

  @NotNull(message = "Price cannot be null.")
  private Double price;

  @NotEmpty(message = "Passenger username cannot be empty.")
  private String passengerUsername;

}
