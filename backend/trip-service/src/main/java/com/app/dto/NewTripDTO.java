package com.app.dto;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.util.Pair;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class NewTripDTO {

  private String startDest;
  private String endDest;
  private double price;
  private List<Pair<Double, Double>> latLng;

}
