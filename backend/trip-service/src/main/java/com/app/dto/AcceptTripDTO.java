package com.app.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class AcceptTripDTO {

  @NotNull(message = "Trip id cannot be null.")
  private Long tripId;

  @NotNull(message = "ETA cannot be null.")
  private Integer eta;

  @NotEmpty(message = "Driver username cannot be empty.")
  private String driverUsername;

}
