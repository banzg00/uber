package com.app.controller;

import static com.app.util.LoggerConstant.log;

import com.app.service.ICompanyService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
@RequestMapping(value = "internal")
public class TripControllerInternal {

  private final ICompanyService companyService;

  @SneakyThrows
  @GetMapping("/getCompany")
  @Operation(summary = "Get company internal")
  public Long getCompany() {
    log.info("Getting company id");
    return companyService.getCompanyId();
  }
}
