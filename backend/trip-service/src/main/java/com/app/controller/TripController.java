package com.app.controller;

import static com.app.util.LoggerConstant.log;

import com.app.dto.AcceptTripDTO;
import com.app.dto.AddressDTO;
import com.app.dto.CancelTripByDriverDTO;
import com.app.dto.CancelTripByPassengerDTO;
import com.app.dto.RouteDTO;
import com.app.dto.TripDTO;
import com.app.service.ITripService;
import io.swagger.v3.oas.annotations.Operation;
import java.util.List;
import javax.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@EnableTransactionManagement
public class TripController {

  private final ITripService tripService;

  @SneakyThrows
  @PostMapping(value = "create")
  @Operation(summary = "Creates new trip with status PENDING")
  public ResponseEntity<TripDTO> createTrip(@RequestBody RouteDTO route) {
    if (!tripService.checkEnoughMoney(route.getPrice(), route.getPassengerUsername())) {
      log.info(
          String.format("User: %s does not have enough credits.", route.getPassengerUsername()));
      return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }
    TripDTO tripDTO = tripService.createTrip(route);
    log.info(
        String.format("Successfully ordered trip with ID: [%d] for user: [%s].", tripDTO.getId(),
        route.getPassengerUsername()));
    return new ResponseEntity<>(tripDTO, HttpStatus.CREATED);
  }

  @SneakyThrows
  @GetMapping(value = "get/address")
  @Operation(summary = "Returns latitude and longitude for chosen address")
  public ResponseEntity<?> getAddress(@RequestParam String address) {
    List<AddressDTO> routes = tripService.getAddresses(address);
    log.info(String.format("Address [%s] from API successfully found.", address));
    return new ResponseEntity<>(routes, HttpStatus.OK);
  }

  @SneakyThrows
  @GetMapping(value = "get/routes")
  @Operation(summary = "Returns one or two routes for selected source and destination")
  public ResponseEntity<List<RouteDTO>> getRoutes(@RequestParam String from,
      @RequestParam String to) {
    List<RouteDTO> routes = tripService.getRoutes(from, to);
    log.info(
        String.format("Routes with source: [%s] and destination: [%s] successfully found.", from,
            to));
    return new ResponseEntity<>(routes, HttpStatus.OK);
  }

  @SneakyThrows
  @PutMapping(value = "end")
  @Operation(summary = "End trip")
  public ResponseEntity<TripDTO> endTrip(@Valid @RequestBody Long tripId) {
    TripDTO tripDTO = tripService.endTrip(tripId);
    log.info(String.format("Trip with id: %d is successfully ended.", tripId));
    return new ResponseEntity<>(tripDTO, HttpStatus.OK);
  }

  @SneakyThrows
  @PutMapping(value = "start")
  @Operation(summary = "Start trip")
  public ResponseEntity<TripDTO> startTrip(@Valid @RequestBody Long tripId) {
    TripDTO tripDTO = tripService.startTrip(tripId);
    log.info(String.format("Trip with id: %d is successfully started.", tripId));
    return new ResponseEntity<>(tripDTO, HttpStatus.OK);
  }

  @SneakyThrows
  @PutMapping(value = "/cancel/driver")
  @Operation(summary = "Cancel trip by driver")
  public ResponseEntity<TripDTO> cancelTripByDriver(
      @Valid @RequestBody CancelTripByDriverDTO cancelTripByDriverDTODTO) {
    TripDTO tripDTO = tripService.cancelTripByDriver(cancelTripByDriverDTODTO);
    log.info(
        String.format("Trip with id: %d is successfully canceled.",
            cancelTripByDriverDTODTO.getTripId()));
    return new ResponseEntity<>(tripDTO, HttpStatus.OK);
  }

  @SneakyThrows
  @GetMapping(value = "arrived/{tripId}")
  @Operation(summary = "Inform customer about vehicle arrival")
  public ResponseEntity<String> driverArrived(@PathVariable Long tripId) {
    tripService.sendDriverArrivedMessage(tripId);
    log.info("Notification about driver arrival sent to passenger.");
    return new ResponseEntity<>("Notification is sent.", HttpStatus.OK);
  }

  @SneakyThrows
  @PutMapping(value = "accept")
  @Operation(summary = "Accept trip")
  public ResponseEntity<TripDTO> acceptTrip(@Valid @RequestBody AcceptTripDTO req) {
    TripDTO tripDTO = tripService.acceptTrip(req);
    log.info(String.format("Trip with id: %d is successfully accepted.", req.getTripId()));
    return new ResponseEntity<>(tripDTO, HttpStatus.ACCEPTED);
  }

  @SneakyThrows
  @PutMapping(value = "/cancel/passenger")
  @Operation(summary = "Cancel trip by passenger")
  public ResponseEntity<TripDTO> cancelTripByPassenger(
      @Valid @RequestBody CancelTripByPassengerDTO cancelTripDTO) {
    TripDTO tripDTO = tripService.cancelTripByPassenger(cancelTripDTO.getTripId(),
        cancelTripDTO.getCancellationDescription());
    log.info(
        String.format("Trip with id: %d is successfully canceled.", cancelTripDTO.getTripId()));
    return new ResponseEntity<>(tripDTO, HttpStatus.OK);
  }

  @SneakyThrows
  @GetMapping(value = "/trip/active/get/{username}")
  @Operation(summary = "Get active trips")
  public ResponseEntity<List<TripDTO>> getActiveTrips(@PathVariable String username) {
    List<TripDTO> activeTrips = tripService.getActiveTrips(username);
    log.info(String.format("Got active trips for user with username: %s.", username));
    return new ResponseEntity<>(activeTrips, HttpStatus.OK);
  }  
}
