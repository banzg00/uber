package com.app.exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserNotFoundException extends Exception {

  private String username;

  @Override
  public String getMessage() {
    return String.format("User with username %s not found.", username);
  }
}
