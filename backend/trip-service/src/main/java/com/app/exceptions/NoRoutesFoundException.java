package com.app.exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class NoRoutesFoundException extends Exception {

  private String source;

  private String destination;

  @Override
  public String getMessage() {
    return String.format("Routes with selected source: %s and destination: %s not found.", source,
        destination);
  }

}
