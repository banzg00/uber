package com.app.exceptions;

import com.app.enums.TripStatus;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TripStatusException extends Exception {

  private TripStatus tripStatus;
  private TripStatus expectedTripStatus;

  public TripStatusException(TripStatus tripStatus) {
    this.tripStatus = tripStatus;
    this.expectedTripStatus = null;
  }

  @Override
  public String getMessage() {
    return expectedTripStatus == null ? String.format("Trip status can't be %s", tripStatus)
        : String.format("Trip status %s but should be %s", tripStatus, expectedTripStatus);
  }
}
