package com.app.exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FieldAlreadyExistsException extends Exception {

  private String field;

  private String value;

  @Override
  public String getMessage() {
    return String.format("%s %s already exists.", field, value);
  }

}
