package com.app.exceptions;

import static com.app.util.LoggerConstant.log;

import java.util.Date;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.MailSendException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import com.fasterxml.jackson.core.JsonProcessingException;

@ControllerAdvice
public class GlobalExceptionHandler {

  @ExceptionHandler(EntityNotFoundException.class)
  public ResponseEntity<ErrorMessage> handleEntityNotFoundExceptionHandler(
      EntityNotFoundException exception, WebRequest request) {
    ErrorMessage errorMessage = new ErrorMessage(HttpStatus.NOT_FOUND.value(), new Date(),
        exception.getMessage(), request.getDescription(false));
    log.error(exception.getMessage());
    return new ResponseEntity<>(errorMessage, HttpStatus.NOT_FOUND);
  }

  @ExceptionHandler(UserNotFoundException.class)
  public ResponseEntity<ErrorMessage> handleUserNotFoundExceptionHandler(
      UserNotFoundException exception, WebRequest request) {
    ErrorMessage errorMessage = new ErrorMessage(HttpStatus.NOT_FOUND.value(), new Date(),
        exception.getMessage(), request.getDescription(false));
    log.error(exception.getMessage());
    return new ResponseEntity<>(errorMessage, HttpStatus.NOT_FOUND);
  }

  @ExceptionHandler(FieldAlreadyExistsException.class)
  public ResponseEntity<ErrorMessage> handleUsernameAlreadyExistsExceptionHandler(
      FieldAlreadyExistsException exception, WebRequest request) {
    ErrorMessage errorMessage = new ErrorMessage(HttpStatus.BAD_REQUEST.value(), new Date(),
        exception.getMessage(), request.getDescription(false));
    log.error(exception.getMessage());
    return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(MethodArgumentNotValidException.class)
  public ResponseEntity<ErrorMessage> handleMethodArgumentNotValidExceptionHandler(
      MethodArgumentNotValidException exception, WebRequest request) {
    String message = exception.getBindingResult().getFieldErrors().stream()
        .map(DefaultMessageSourceResolvable::getDefaultMessage).findFirst().get();
    ErrorMessage errorMessage = new ErrorMessage(HttpStatus.BAD_REQUEST.value(), new Date(),
        message, request.getDescription(false));
    log.error(message);
    return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(RateException.class)
  public ResponseEntity<ErrorMessage> handleRateExceptionHandler(RateException exception,
      WebRequest request) {
    ErrorMessage errorMessage = new ErrorMessage(HttpStatus.BAD_REQUEST.value(), new Date(),
        exception.getMessage(), request.getDescription(false));
    log.error(exception.getMessage());
    return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(MailSendException.class)
  public ResponseEntity<ErrorMessage> handleMailSendExceptionHandler(MailSendException exception,
      WebRequest request) {
    String message = exception.getFailedMessages().values().stream().map(Throwable::getMessage)
        .findFirst().get();
    ErrorMessage errorMessage = new ErrorMessage(HttpStatus.BAD_REQUEST.value(), new Date(),
        message, request.getDescription(false));
    log.error(message);
    return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(PaymentException.class)
  public ResponseEntity<ErrorMessage> handlePaymentExceptionHandler(PaymentException exception,
      WebRequest request) {
    ErrorMessage errorMessage = new ErrorMessage(HttpStatus.BAD_REQUEST.value(), new Date(),
        exception.getMessage(), request.getDescription(false));
    log.error(exception.getMessage());
    return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(TripStatusException.class)
  public ResponseEntity<ErrorMessage> handleTripStatusExceptionHandler(
      TripStatusException exception, WebRequest request) {
    ErrorMessage errorMessage = new ErrorMessage(HttpStatus.BAD_REQUEST.value(), new Date(),
        exception.getMessage(), request.getDescription(false));
    log.error(exception.getMessage());
    return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(NoRoutesFoundException.class)
  public ResponseEntity<ErrorMessage> handleNoRoutesFoundExceptionHandler(
      NoRoutesFoundException exception, WebRequest request) {
    ErrorMessage errorMessage = new ErrorMessage(HttpStatus.NOT_FOUND.value(), new Date(),
        exception.getMessage(), request.getDescription(false));
    log.error(exception.getMessage());
    return new ResponseEntity<>(errorMessage, HttpStatus.NOT_FOUND);
  }

  @ExceptionHandler(CreditPayOutException.class)
  public ResponseEntity<ErrorMessage> handleCreditPayOutExceptionHandler(
      CreditPayOutException exception, WebRequest request) {
    ErrorMessage errorMessage = new ErrorMessage(HttpStatus.BAD_REQUEST.value(), new Date(),
        exception.getMessage(), request.getDescription(false));
    log.error(exception.getMessage());
    return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(TripAlreadyAcceptedException.class)
  public ResponseEntity<ErrorMessage> handleTripAlreadyAcceptedExceptionHandler(
      TripAlreadyAcceptedException exception, WebRequest request) {
    ErrorMessage errorMessage = new ErrorMessage(HttpStatus.BAD_REQUEST.value(), new Date(),
        "Trip is already accepted by other driver.", request.getDescription(false));
    log.error("Trip is already accepted by other driver.");
    return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(NotificationServiceException.class)
  public ResponseEntity<ErrorMessage> handleNotificationServiceExceptionHandler(
      NotificationServiceException exception, WebRequest request) {
    ErrorMessage errorMessage = new ErrorMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
        new Date(), exception.getMessage(), request.getDescription(false));
    log.error(exception.getMessage());
    return new ResponseEntity<>(errorMessage, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler(PaymentServiceException.class)
  public ResponseEntity<ErrorMessage> handlePaymentServiceExceptionHandler(
      PaymentServiceException exception, WebRequest request) {
    ErrorMessage errorMessage = new ErrorMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
        new Date(), exception.getMessage(), request.getDescription(false));
    log.error(exception.getMessage());
    return new ResponseEntity<>(errorMessage, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler(JsonProcessingException.class)
  public ResponseEntity<ErrorMessage> handleJsonProcessingExceptionHandler(
    JsonProcessingException exception, WebRequest request) {
    ErrorMessage errorMessage = new ErrorMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(),
        new Date(), "There was an error with address service", request.getDescription(false));
    log.error(exception.getMessage());
    return new ResponseEntity<>(errorMessage, HttpStatus.INTERNAL_SERVER_ERROR);
  }

}
