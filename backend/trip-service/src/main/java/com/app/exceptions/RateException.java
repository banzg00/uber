package com.app.exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RateException extends Exception {

  private String description;

  @Override
  public String getMessage() {
    return String.format("Cannot add rate: %s", description);
  }
}
