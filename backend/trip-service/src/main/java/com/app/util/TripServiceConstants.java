package com.app.util;

public class TripServiceConstants {

  public static final String ROUTE_API_URL = "https://router.project-osrm.org/route/v1/driving/%s;%s?overview=false&alternatives=true&steps=true&hints=;";
  public static final String ADDRESS_API_URL = "https://nominatim.openstreetmap.org/?q=%s&format=json&addressdetails=1&accept-language=sr-Latn-RS";
  public static final long NO_DRIVER_ACCEPTED_PERIOD = 4L;
}
