package com.app.util;

public class Messages {

  public static final String NEW_TRIP_DRIVER_MESSAGE = "There is a new trip. Take it or just ignore the message.";

  public static final String TRIP_ACCEPTED_PASSENGER_MESSAGE = "One of drivers accepted your trip request and will come in %d minutes.";
  
  public static final String CANCEL_TRIP_BY_PASSENGER = "Passenger cancelled the trip with id: %d";
  
  public static final String CANCEL_TRIP_BY_DRIVER = "We are sorry, but our driver cancelled your trip.";

  public static final String DRIVER_ARRIVED = "Your vehicle has arrived.";

  public static final String NO_DRIVER_ACCEPTED = "There are no available drivers at the moment. Please try again later.";
}
