package com.app.model;

import com.app.enums.TripStatus;
import java.time.LocalDateTime;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "trips")
@Entity
public class Trip {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column
  private LocalDateTime startDate;

  @Column
  private LocalDateTime endDate;

  @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JoinColumn(name = "route_id", referencedColumnName = "id")
  private Route route;

  @Enumerated(EnumType.STRING)
  private TripStatus tripStatus;

  @Column
  private String passengerUsername;

  @Column
  private String driverUsername;

  @Column
  private double price;

  @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  @JoinColumn(name = "trip_cancellation_id", referencedColumnName = "id")
  private TripCancellation tripCancellation;

  @Column
  private Long transactionId;

  @Version
  @Column
  private long version;

  @Column
  @JsonIgnore
  private LocalDateTime created;
}
