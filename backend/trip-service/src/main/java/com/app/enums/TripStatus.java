package com.app.enums;

public enum TripStatus {
  PENDING, ACCEPTED, CANCELED, STARTED, ENDED, NOT_ACCEPTED
}
